package util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;

public interface TestStatementUnit
{
	static void assertStatementResultEquals(StatementUnitResult expected, StatementUnitResult actual)
	{
		TestSemanticUnit.assertSemanticUnitEquals(expected.statement(), actual.statement());
		TestSemanticUnit.assertSemanticUnitEquals(expected.subjectUnit(), actual.subjectUnit());
		assertEquals(expected.compounds().keySet().size(), actual.compounds().keySet().size());
		
		for(var entry : expected.compounds().entrySet())
		{
			TestCompoundUnit.assertCompoundResultEquals(entry.getValue(), actual.compounds().get(entry.getKey()));
		}
		
		assertEquals(expected.statements().size(), actual.statements().size());
		
		for(int x = 0; x < actual.statements().size(); x++)
		{
			TestStatementUnit.assertStatementResultEquals(expected.statements().get(x), actual.statements().get(x));
		}
	}
}
