package util;

import eu.tib.kgbbengine.adapter.output.rdf4j.Rdf4jRepositiory;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;

public interface Rdf4jSemanticUnitRepositorySupplier extends SemanticUnitRepositorySupplier
{
	@Override
	default SemanticUnitRepository newSemanticUnitRepository(LinearStorageModel genericSemanticUnitModel, LinearStorageModel genericResourceObjectModel)
	{
		return new Rdf4jRepositiory(genericSemanticUnitModel, genericResourceObjectModel);
	}
}
