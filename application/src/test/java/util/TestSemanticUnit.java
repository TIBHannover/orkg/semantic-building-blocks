package util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

import org.junit.jupiter.api.BeforeEach;

import eu.tib.kgbbengine.adapter.output.FakeInMemoryKGBBApplicationSpecificationRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryKGBBRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryObjectPositionRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryStorageModelRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryUserRepository;
import eu.tib.kgbbengine.adapter.output.FakeOntologyLookup;
import eu.tib.kgbbengine.domain.KGBBEngine;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.UriSupplier;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.spi.KGBBApplicationSpecificationRepository;
import eu.tib.kgbbengine.spi.KGBBRepository;
import eu.tib.kgbbengine.spi.ObjectPositionRepository;
import eu.tib.kgbbengine.spi.OntologyLookupService;
import eu.tib.kgbbengine.spi.StorageModelRepository;
import eu.tib.kgbbengine.spi.UserRepository;

public interface TestSemanticUnit extends SemanticUnitRepositorySupplier
{
	static final URI USER_ID = new URI("KGBBEngineUser");
	static final Delegate<KGBBEngine> ENGINE = new Delegate<KGBBEngine>();
	
	@BeforeEach
	default void setup() throws Exception
	{
		KGBBRepository kgbbRepo = new FakeInMemoryKGBBRepository();
		KGBBApplicationSpecificationRepository kgbbAppSpecRepo = new FakeInMemoryKGBBApplicationSpecificationRepository();
		ObjectPositionRepository positionRepo = new FakeInMemoryObjectPositionRepository();
		StorageModelRepository storageModelRepo = new FakeInMemoryStorageModelRepository();
		UserRepository userRepo = new FakeInMemoryUserRepository();
		OntologyLookupService ontoService = new FakeOntologyLookup();
		ENGINE.set(new KGBBEngine(kgbbRepo, kgbbAppSpecRepo, positionRepo, storageModelRepo, this::newSemanticUnitRepository, userRepo, ontoService, new UriSupplier()
		{
			private int index = 0;
			
			@Override
			public URI newURI()
			{
				return new URI(String.valueOf(this.index++));
			}
		}));
	}
	
	default KGBBEngine engine()
	{
		return ENGINE.get();
	}
	
	static void assertSemanticUnitEquals(SemanticUnit expected, SemanticUnit actual)
	{
		assertEquals(expected.getSemanticUnitsGraph(), actual.getSemanticUnitsGraph());
		assertEquals(expected.getUri(), actual.getUri());
//		assertEquals(expected.getStorageModel(), actual.getStorageModel());
		assertEquals(expected.getSubject(), actual.getSubject());
		assertEquals(expected.getLabel(), actual.getLabel());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertEquals(expected.getKgbb(), actual.getKgbb());
		assertEquals(expected.getKgbbi(), actual.getKgbbi());
		assertEquals(expected.getLinkedSemanticUnits(), actual.getLinkedSemanticUnits());
		assertSetEquals(expected.getResourceObjects(), actual.getResourceObjects(), (a, b) -> a.getUri().compareTo(b.getUri()), TestSemanticUnit::assertResourceObjectEquals);
		assertSetEquals(expected.getLiteralObjects(), actual.getLiteralObjects(), (a, b) -> a.getUri().compareTo(b.getUri()), TestSemanticUnit::assertLiteralObjectEquals);
		assertEquals(expected.getAssociatedSemanticUnits(), actual.getAssociatedSemanticUnits());
		assertEquals(expected.getLicense(), actual.getLicense());
		assertEquals(expected.getAccessRestrictedTo(), actual.getAccessRestrictedTo());
		assertEquals(expected.getCreator(), actual.getCreator());
//		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getCreatedWithApplication(), actual.getCreatedWithApplication());
		assertEquals(expected.getObjectDescribedBySemanticUnits(), actual.getObjectDescribedBySemanticUnits());
		assertEquals(expected.isCurrentVersion(), actual.isCurrentVersion());
		assertEquals(expected.getDeletedBy(), actual.getDeletedBy());
//		assertEquals(expected.getDeletionDate(), actual.getDeletionDate());
	}
	
	static void assertResourceObjectEquals(ResourceObjectNode expected, ResourceObjectNode actual)
	{
		assertEquals(expected.getUri(), actual.getUri());
		assertEquals(expected.getInputTypeLabel(), actual.getInputTypeLabel());
		assertEquals(expected.getObjectPositionClass(), actual.getObjectPositionClass());
		assertEquals(expected.getCreator(), actual.getCreator());
//		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getCreatedWithApplication(), actual.getCreatedWithApplication());
		assertEquals(expected.isCurrentVersion(), actual.isCurrentVersion());
		assertEquals(expected.getResourceURI(), actual.getResourceURI());
		assertEquals(expected.getResourceLabel(), actual.getResourceLabel());
		assertEquals(expected.getConstraint(), actual.getConstraint());
	}
	
	static void assertLiteralObjectEquals(LiteralObjectNode expected, LiteralObjectNode actual)
	{
		assertEquals(expected.getUri(), actual.getUri());
		assertEquals(expected.getInputTypeLabel(), actual.getInputTypeLabel());
		assertEquals(expected.getObjectPositionClass(), actual.getObjectPositionClass());
		assertEquals(expected.getCreator(), actual.getCreator());
//		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEquals(expected.getCreatedWithApplication(), actual.getCreatedWithApplication());
		assertEquals(expected.isCurrentVersion(), actual.isCurrentVersion());
		assertEquals(expected.getLiteral(), actual.getLiteral());
	}
	
	static <T> void assertSetEquals(Set<T> expected, Set<T> actual, Comparator<T> comparator, BiConsumer<T, T> validator)
	{
		assertEquals(expected.size(), actual.size());
		
		List<T> sortedExpected = new ArrayList<T>(expected);
		sortedExpected.sort(comparator);
		List<T> sortedActual = new ArrayList<T>(actual);
		sortedActual.sort(comparator);
		
		for(int x = 0; x < sortedExpected.size(); x++)
		{
			validator.accept(sortedExpected.get(x), sortedActual.get(x));
		}
	}
}
