package util;

import eu.tib.kgbbengine.adapter.output.FakeInMemorySemanticUnitRepository;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;

public interface FakeSemanticUnitRepositorySupplier extends SemanticUnitRepositorySupplier
{
	@Override
	default SemanticUnitRepository newSemanticUnitRepository(LinearStorageModel genericSemanticUnitModel, LinearStorageModel genericResourceObjectModel)
	{
		return new FakeInMemorySemanticUnitRepository();
	}
}
