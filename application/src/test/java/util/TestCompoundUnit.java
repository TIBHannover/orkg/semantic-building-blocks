package util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;

public interface TestCompoundUnit
{
	static void assertCompoundResultEquals(CompoundUnitResult actual, CompoundUnitResult expected)
	{
		TestSemanticUnit.assertSemanticUnitEquals(actual.compound(), expected.compound());
		TestSemanticUnit.assertSemanticUnitEquals(actual.niiu(), expected.niiu());
		assertEquals(actual.statements().size(), expected.statements().size());
		
		for(int x = 0; x < actual.statements().size(); x++)
		{
			TestStatementUnit.assertStatementResultEquals(actual.statements().get(x), expected.statements().get(x));
		}
	}
}
