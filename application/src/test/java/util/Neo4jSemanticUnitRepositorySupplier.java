package util;

import org.neo4j.cypherdsl.core.Cypher;
import org.neo4j.cypherdsl.core.Node;
import org.neo4j.cypherdsl.core.Statement;
import org.neo4j.cypherdsl.core.executables.ExecutableStatement;

import eu.tib.kgbbengine.adapter.output.neo4j.Neo4jRepository;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;

public interface Neo4jSemanticUnitRepositorySupplier extends SemanticUnitRepositorySupplier
{
	static final Delegate<Neo4jRepository> INSTANCE = new Delegate<Neo4jRepository>();
	
	@Override
	default SemanticUnitRepository newSemanticUnitRepository(LinearStorageModel genericSemanticUnitModel, LinearStorageModel genericResourceObjectModel)
	{
		if(INSTANCE.get() == null)
		{
			INSTANCE.set(new Neo4jRepository("bolt://localhost:7687", "neo4j", "123456", genericSemanticUnitModel, genericResourceObjectModel));
		}
		
		Node node = Cypher.anyNode();
		Statement query = Cypher.match(node).detachDelete(node).build();
		ExecutableStatement.of(query).executeWith(Neo4jSemanticUnitRepositorySupplier.INSTANCE.get().getNeo4jClient().getQueryRunner());
		return Neo4jSemanticUnitRepositorySupplier.INSTANCE.get();
	}
}
