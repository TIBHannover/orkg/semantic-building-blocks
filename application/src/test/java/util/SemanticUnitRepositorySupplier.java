package util;

import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;

public interface SemanticUnitRepositorySupplier
{
	SemanticUnitRepository newSemanticUnitRepository(LinearStorageModel genericSemanticUnitModel, LinearStorageModel genericResourceObjectModel);
}
