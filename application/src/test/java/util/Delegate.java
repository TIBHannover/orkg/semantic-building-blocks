package util;

public class Delegate<T>
{
	private T value;
	
	public void set(T value)
	{
		this.value = value;
	}
	
	public T get()
	{
		return this.value;
	}
}
