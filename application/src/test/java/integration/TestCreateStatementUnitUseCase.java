package integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.kgbb.AssociationException;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.spi.OntologyServiceException;
import util.TestSemanticUnit;
import util.TestStatementUnit;

public interface TestCreateStatementUnitUseCase extends TestSemanticUnit
{
	static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	static final URI STATEMENT_KGBBI = new URI("HasPartStatementKGBBInstance");
	static final URI STATEMENT_RESOURCE_POSITION = new URI("HasPartStatementUnitResourceObjectPosition");
	
	@BeforeEach
	default void setup() throws Exception
	{
		TestSemanticUnit.super.setup();
		this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
	}
	
	@Test
	default void testCreateStatementUnitClassInput() throws Exception
	{
		SemanticUnit subjectUnit = this.engine().findSemanticUnit(new URI("5"));
		StatementUnitResult actual = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), new URI("5"));
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("14"));
		expectedItem.setSemanticUnitsGraph(new URI("15"));
		expectedItem.setSubject(new URI("9"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("10"));
		expectedNiiu.setSemanticUnitsGraph(new URI("11"));
		expectedNiiu.setSubject(new URI("9"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri());
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedHpsu.setUri(new URI("7"));
		expectedHpsu.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu.setSubject(new URI("0"));
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(expectedItem.getUri());
		expectedHpsu.setCurrentVersion(true);
		expectedHpsu.setDeletedBy(null);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject.setInputTypeLabel("has-part-resource");
		hpsuResourceObject.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject.setCurrentVersion(true);
		hpsuResourceObject.setResourceURI(expectedNiiu.getSubject());
		hpsuResourceObject.setResourceLabel(null);
		hpsuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject);
		
		subjectUnit.addLinkedSemanticUnit(expectedItem.getUri());
		subjectUnit.addAssociatedSemanticUnit(expectedNiiu.getUri());
		subjectUnit.addAssociatedSemanticUnit(new URI("1")); //NIIU of subject Unit
		subjectUnit.addAssociatedSemanticUnit(expectedHpsu.getUri());
		
		CompoundUnitResult expectedItemUnitResult = new CompoundUnitResult(expectedItem, expectedNiiu, null, Collections.emptyList());
		TestStatementUnit.assertStatementResultEquals(new StatementUnitResult(expectedHpsu, Map.of(STATEMENT_RESOURCE_POSITION, expectedItemUnitResult), subjectUnit, Collections.emptyList(), Collections.emptyList(), Collections.emptyList()), actual);
	}
	
	@Test
	default void testMissingInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder().build(), new URI("5"));
		});
		assertEquals("Missing input for object position " + STATEMENT_RESOURCE_POSITION.toString(), exception.getMessage());
	}
	
	@Test
	default void testCreateStatementUnitInstanceInput() throws Exception
	{
		CompoundUnitResult createMulticellularOrganismItem = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		CompoundUnitResult createEyeItem = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "eye", ResourceType.CLASS)
				.build(), null);
		
		StatementUnitResult actual = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, createEyeItem.compound().getUri(), "eye", ResourceType.INSTANCE)
				.build(), createMulticellularOrganismItem.compound().getUri());
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu.setUri(new URI("21"));
		expectedHpsu.setSemanticUnitsGraph(new URI("22"));
		expectedHpsu.setSubject(createMulticellularOrganismItem.compound().getSubject());
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(createEyeItem.compound().getUri());
		expectedHpsu.setCurrentVersion(true);
		expectedHpsu.setDeletedBy(null);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject = new ResourceObjectNode(new URI("23"));
		hpsuResourceObject.setInputTypeLabel("has-part-resource");
		hpsuResourceObject.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject.setCurrentVersion(true);
		hpsuResourceObject.setResourceURI(createEyeItem.niiu().getSubject());
		hpsuResourceObject.setResourceLabel(null);
		hpsuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject);
		
		SemanticUnit subjectUnit = createMulticellularOrganismItem.compound();
		subjectUnit.addLinkedSemanticUnit(createEyeItem.compound().getUri());
		subjectUnit.addAssociatedSemanticUnit(createEyeItem.niiu().getUri());
		subjectUnit.addAssociatedSemanticUnit(expectedHpsu.getUri());
		
		TestStatementUnit.assertStatementResultEquals(new StatementUnitResult(expectedHpsu, Collections.emptyMap(), subjectUnit, Collections.emptyList(), Collections.emptyList(), Collections.emptyList()), actual);
	}
	
	@Test
	default void testLiteralInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
					.addLiteralInput(STATEMENT_RESOURCE_POSITION, null)
					.build(), new URI("5"));
		});
		assertEquals("Expected input for object position " + STATEMENT_RESOURCE_POSITION + " to be of type literal", exception.getMessage());
	}
	
	@Test
	default void testEmptyInputError() throws Exception
	{
		assertThrows(OntologyServiceException.class, () ->
		{
			this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
					.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI(""), null, ResourceType.CLASS)
					.build(), new URI("5"));
		});
	}
	
	@Test
	default void testMissingKggbiInputError() throws Exception
	{
		AssociationException exception = assertThrows(AssociationException.class, () ->
		{
			this.engine().createStatementUnit(new URI("Missing"), USER_ID, Input.builder().build(), new URI("5"));
		});
		assertEquals("Could not find requested association with target Missing for KGBB instance " + ITEM_KGBBI.toString(), exception.getMessage());
	}
	
	@Test
	default void testConstraintViolationInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
					.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/ERROR_0000000"), "error", ResourceType.CLASS)
					.build(), new URI("5"));
		});
		assertEquals("http://purl.obolibrary.org/obo/ERROR_0000000 is not a subclass of http://purl.obolibrary.org/obo/BFO_0000040", exception.getMessage());
	}
	
	@Test
	default void testMissingSubjectError() throws Exception
	{
		NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
		{
			this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
					.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
					.build(), new URI("Missing"));
		});
		assertEquals("Tried to find missing semantic unit Missing", exception.getMessage());
	}
	
	@Test
	default void testInvalidUserInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createStatementUnit(STATEMENT_KGBBI, new URI(""), Input.builder()
					.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
					.build(), new URI("5"));
		});
		assertEquals("Invalid user ", exception.getMessage());
	}
	
	@Test
	default void testAssociationQuantityExceededError() throws Exception
	{
		AssociationException exception = assertThrows(AssociationException.class, () ->
		{
			CompoundUnitResult itemResult = this.engine().createCompoundUnit(new URI("WeightMeasurementCompoundKGBBInstance"), USER_ID, Input.builder()
					.addResourceInput(new URI("WeightMeasurementCompoundKGBBInstance"), new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
					.addResourceInput(new URI("QualityStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/PATO_0000128"), "weight", ResourceType.CLASS)
					.addResourceInput(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/NCIT_C28252"), "kg", ResourceType.CLASS)
					.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition1"), String.valueOf(5.0))
					.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition2"), String.valueOf(4.0))
					.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition3"), String.valueOf(6.0))
					.build(), null);
			
			this.engine().createStatementUnit(new URI("QualityStatementKGBBInstance"), USER_ID, Input.builder()
					.addResourceInput(new URI("QualityStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/PATO_0000128"), "weight", ResourceType.CLASS)
					.addResourceInput(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/NCIT_C28252"), "kg", ResourceType.CLASS)
					.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition1"), String.valueOf(5.0))
					.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition2"), String.valueOf(4.0))
					.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition3"), String.valueOf(6.0))
					.build(), itemResult.compound().getUri());
		});
		assertEquals("Exceeded quantity limit for QualityStatementKGBBInstance in WeightMeasurementCompoundKGBBInstance", exception.getMessage());
	}
	
	@Test
	default void testCreateStatementLinkedToStatement() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(new URI("WeightMeasurementCompoundKGBBInstance"), USER_ID, Input.builder()
				.addResourceInput(new URI("WeightMeasurementCompoundKGBBInstance"), new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.addResourceInput(new URI("QualityStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/PATO_0000128"), "weight", ResourceType.CLASS)
				.addResourceInput(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/NCIT_C28252"), "kg", ResourceType.CLASS)
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition1"), String.valueOf(5.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition2"), String.valueOf(4.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition3"), String.valueOf(6.0))
				.build(), null);
		StatementUnitResult actual = itemResult.statements().get(0);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("17"));
		expectedNiiu.setSemanticUnitsGraph(new URI("18"));
		expectedNiiu.setSubject(new URI("16"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.addObjectDescribedBySemanticUnit(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("19"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("20"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		SemanticUnit expectedWmu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("WeightMeasurementStatementKGBB")));
		expectedWmu.setUri(new URI("21"));
		expectedWmu.setSemanticUnitsGraph(new URI("22"));
		expectedWmu.setSubject(new URI("16"));
		expectedWmu.setLabel("Weight Measurement Statement Unit");
		expectedWmu.setDescription("Weight Measurement Statement Unit Description");
		expectedWmu.setKgbb(new URI("WeightMeasurementStatementKGBB"));
		expectedWmu.setKgbbi(new URI("WeightMeasurementStatementKGBBInstance"));
		expectedWmu.setCreator(USER_ID);
//		expectedWmu.setCreationDate(LocalDateTime.now());
		expectedWmu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedWmu.setLicense(null);
//		expectedWmu.addAccessRestrictedTo(null);
		expectedWmu.setCurrentVersion(true);
		expectedWmu.setDeletedBy(null);
		expectedWmu.setDeletionDate(null);
		expectedWmu.addAssociatedSemanticUnit(expectedNiiu.getUri());
		
		ResourceObjectNode wmuResourceObject = new ResourceObjectNode(new URI("24"));
		wmuResourceObject.setInputTypeLabel("Unit");
		wmuResourceObject.setObjectPositionClass(new URI("WeightMeasurementUnitResourceObjectPosition"));
		wmuResourceObject.setCreator(USER_ID);
//		wmuResourceObject.setCreationDate(LocalDateTime.now());
		wmuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuResourceObject.setCurrentVersion(true);
		wmuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/NCIT_C28252"));
		wmuResourceObject.setResourceLabel("Kilogram");
		wmuResourceObject.setConstraint(null);
		expectedWmu.addResourceObject(wmuResourceObject);
		
		LiteralObjectNode wmuLiteralObject = new LiteralObjectNode(new URI("25"));
		wmuLiteralObject.setInputTypeLabel("weight-value-label");
		wmuLiteralObject.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition1"));
		wmuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		wmuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject.setCurrentVersion(true);
		wmuLiteralObject.setLiteral(5.0);
		expectedWmu.addLiteralObject(wmuLiteralObject);
		
		LiteralObjectNode wmuLiteralObject2 = new LiteralObjectNode(new URI("23"));
		wmuLiteralObject2.setInputTypeLabel("lower-bound-confidence-interval-label");
		wmuLiteralObject2.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition2"));
		wmuLiteralObject2.setCreator(USER_ID);
//		wmuLiteralObject2.setCreationDate(LocalDateTime.now());
		wmuLiteralObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject2.setCurrentVersion(true);
		wmuLiteralObject2.setLiteral(4.0);
		expectedWmu.addLiteralObject(wmuLiteralObject2);
		
		LiteralObjectNode wmuLiteralObject3 = new LiteralObjectNode(new URI("26"));
		wmuLiteralObject3.setInputTypeLabel("upper-bound-confidence-interval-label");
		wmuLiteralObject3.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition3"));
		wmuLiteralObject3.setCreator(USER_ID);
//		wmuLiteralObject3.setCreationDate(LocalDateTime.now());
		wmuLiteralObject3.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject3.setCurrentVersion(true);
		wmuLiteralObject3.setLiteral(6.0);
		expectedWmu.addLiteralObject(wmuLiteralObject3);
		
		SemanticUnit expectedQsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("QualityStatementKGBB")));
		expectedQsu.setUri(new URI("14"));
		expectedQsu.setSemanticUnitsGraph(new URI("15"));
		expectedQsu.setSubject(new URI("7"));
		expectedQsu.setLabel("Quality Statement Unit");
		expectedQsu.setDescription("Quality Statement Unit Description");
		expectedQsu.setKgbb(new URI("QualityStatementKGBB"));
		expectedQsu.setKgbbi(new URI("QualityStatementKGBBInstance"));
		expectedQsu.setCreator(USER_ID);
//		expectedQsu.setCreationDate(LocalDateTime.now());
		expectedQsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedQsu.setLicense(null);
//		expectedQsu.addAccessRestrictedTo(null);
		expectedQsu.addObjectDescribedBySemanticUnit(expectedWmu.getUri());
		expectedQsu.setCurrentVersion(true);
		expectedQsu.setDeletedBy(null);
		expectedQsu.setDeletionDate(null);
		
		ResourceObjectNode qsuResourceObject = new ResourceObjectNode(new URI("27"));
		qsuResourceObject.setInputTypeLabel("Quality");
		qsuResourceObject.setObjectPositionClass(new URI("QualityStatementUnitResourceObjectPosition"));
		qsuResourceObject.setCreator(USER_ID);
//		qsuResourceObject.setCreationDate(LocalDateTime.now());
		qsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		qsuResourceObject.setCurrentVersion(true);
		qsuResourceObject.setResourceURI(expectedNiiu.getSubject());
		qsuResourceObject.setResourceLabel(null);
		qsuResourceObject.setConstraint(null);
		expectedQsu.addResourceObject(qsuResourceObject);
		
		//SemanticUnit statement, Map<URI, ItemUnitResult> items, SemanticUnit subjectUnit, List<StatementUnitResult> statements, List<SemanticUnit> niius
		TestStatementUnit.assertStatementResultEquals(new StatementUnitResult(expectedQsu, Collections.emptyMap(), itemResult.compound(), List.of(new StatementUnitResult(expectedWmu, Collections.emptyMap(), expectedQsu, Collections.emptyList(), Collections.emptyList(), Collections.emptyList())), List.of(expectedNiiu), Collections.emptyList()), actual);
	}
}
