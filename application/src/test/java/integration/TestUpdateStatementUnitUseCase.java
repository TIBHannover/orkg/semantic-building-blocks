package integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnitType;
import eu.tib.kgbbengine.domain.util.Constants;
import util.TestSemanticUnit;

public interface TestUpdateStatementUnitUseCase extends TestSemanticUnit
{
	static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	static final URI STATEMENT_KGBBI = new URI("HasPartStatementKGBBInstance");
	static final URI STATEMENT_RESOURCE_POSITION = new URI("HasPartStatementUnitResourceObjectPosition");
	
	@Test
	default void testUpdateLiteral() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		SemanticUnit actualNiiu = this.engine().updateStatementUnit(itemResult.niiu().getUri(), USER_ID, Input.builder()
				.addLiteralInput(Constants.NIIU_LITERAL_POSITION_URI, "eye")
				.build(), itemResult.compound().getUri());
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("3"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000468"));
		niiuResourceObject.setResourceLabel("multicellular organism");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject1 = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject1.setInputTypeLabel("niiu-label");
		niiuLiteralObject1.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject1.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject1.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject1.setCurrentVersion(false);
		niiuLiteralObject1.setLiteral("multicellular organism");
		expectedNiiu.addLiteralObject(niiuLiteralObject1);
		
		LiteralObjectNode niiuLiteralObject2 = new LiteralObjectNode(new URI("7"));
		niiuLiteralObject2.setInputTypeLabel("niiu-label");
		niiuLiteralObject2.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject2.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject2.setCurrentVersion(true);
		niiuLiteralObject2.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject2);
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedNiiu, actualNiiu);
	}
	
	@Test
	default void testUpdateClassResource() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		SemanticUnit actualNiiu = this.engine().updateStatementUnit(itemResult.niiu().getUri(), USER_ID, Input.builder()
				.addResourceInput(Constants.NIIU_RESOURCE_POSITION_URI, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult.compound().getUri());
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject1 = new ResourceObjectNode(new URI("3"));
		niiuResourceObject1.setInputTypeLabel("Class Affilitation");
		niiuResourceObject1.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject1.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject1.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject1.setCurrentVersion(false);
		niiuResourceObject1.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000468"));
		niiuResourceObject1.setResourceLabel("multicellular organism");
		niiuResourceObject1.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject1);
		
		ResourceObjectNode niiuResourceObject2 = new ResourceObjectNode(new URI("7"));
		niiuResourceObject2.setInputTypeLabel("Class Affilitation");
		niiuResourceObject2.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject2.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject2.setCurrentVersion(true);
		niiuResourceObject2.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject2.setResourceLabel("eye");
		niiuResourceObject2.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject2);
		
		LiteralObjectNode niiuLiteralObject1 = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject1.setInputTypeLabel("niiu-label");
		niiuLiteralObject1.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject1.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject1.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject1.setCurrentVersion(true);
		niiuLiteralObject1.setLiteral("multicellular organism");
		expectedNiiu.addLiteralObject(niiuLiteralObject1);
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedNiiu, actualNiiu);
	}
	
	@Test
	default void testUpdateInstanceResource() throws Exception
	{
		CompoundUnitResult itemResult1 = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		StatementUnitResult statement = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult1.compound().getUri());
		
		CompoundUnitResult itemResult2 = statement.compounds().get(STATEMENT_RESOURCE_POSITION);
		
		CompoundUnitResult itemResult3 = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), null);
		
		SemanticUnit actualHpsu = this.engine().updateStatementUnit(statement.statement().getUri(), USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, itemResult3.compound().getUri(), "eye", ResourceType.INSTANCE)
				.build(), itemResult1.compound().getUri());
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu.setUri(new URI("7"));
		expectedHpsu.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu.setSubject(new URI("0"));
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(USER_ID);
//		expectedHpsu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedHpsu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(itemResult3.compound().getUri());
		expectedHpsu.addObjectDescribedBySemanticUnit(itemResult2.compound().getUri(), false);
		expectedHpsu.setCurrentVersion(true);
		expectedHpsu.setDeletedBy(null);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject1 = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject1.setInputTypeLabel("has-part-resource");
		hpsuResourceObject1.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject1.setCreator(USER_ID);
//		hpsuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject1.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject1.setCurrentVersion(false);
		hpsuResourceObject1.setResourceURI(itemResult2.niiu().getSubject());
		hpsuResourceObject1.setResourceLabel(null);
		hpsuResourceObject1.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject1);
		
		ResourceObjectNode hpsuResourceObject2 = new ResourceObjectNode(new URI("24"));
		hpsuResourceObject2.setInputTypeLabel("has-part-resource");
		hpsuResourceObject2.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject2.setCreator(USER_ID);
//		hpsuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject2.setCurrentVersion(true);
		hpsuResourceObject2.setResourceURI(itemResult3.niiu().getSubject());
		hpsuResourceObject2.setResourceLabel(null);
		hpsuResourceObject2.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject2);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		expectedItem.addLinkedSemanticUnit(new URI("14"), false);
		expectedItem.addLinkedSemanticUnit(new URI("22"));
		expectedItem.addAssociatedSemanticUnit(new URI("1"));
		expectedItem.addAssociatedSemanticUnit(new URI("7"));
		expectedItem.addAssociatedSemanticUnit(new URI("10"), false);
		expectedItem.addAssociatedSemanticUnit(new URI("18"));
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu, actualHpsu);
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, this.engine().findSemanticUnit(new URI("5")));
	}
	
	@Test
	default void testUpdateInstanceResourceToSelfError() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(itemResult.niiu().getUri(), USER_ID, Input.builder()
					.addResourceInput(Constants.NIIU_RESOURCE_POSITION_URI, itemResult.compound().getUri(), "eye", ResourceType.INSTANCE)
					.build(), itemResult.compound().getUri());
		});
		assertEquals("Cannot describe self", exception.getMessage());
	}
	
	@Test
	default void testConstraintViolationInstanceInputError() throws Exception
	{
		CompoundUnitResult itemResult1 = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		StatementUnitResult statement = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult1.compound().getUri());
		
		CompoundUnitResult itemResult3 = this.engine().createCompoundUnit(new URI("WeightMeasurementCompoundKGBBInstance"), USER_ID, Input.builder()
				.addResourceInput(new URI("WeightMeasurementCompoundKGBBInstance"), new URI("http://purl.obolibrary.org/obo/BFO_0000001"), "error", ResourceType.CLASS)
				.addResourceInput(new URI("QualityStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/PATO_0000128"), "weight", ResourceType.CLASS)
				.addResourceInput(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/NCIT_C28252"), "kg", ResourceType.CLASS)
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition1"), String.valueOf(5.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition2"), String.valueOf(4.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition3"), String.valueOf(6.0))
				.build(), null);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(statement.statement().getUri(), USER_ID, Input.builder()
					.addResourceInput(STATEMENT_RESOURCE_POSITION, itemResult3.compound().getUri(), "error", ResourceType.INSTANCE)
					.build(), itemResult1.compound().getUri());
		});
		assertEquals("http://purl.obolibrary.org/obo/BFO_0000001 does not satisfy constraint http://purl.obolibrary.org/obo/BFO_0000040", exception.getMessage());
	}
	
	@Test
	default void testConstraintViolationClassInputError() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(itemResult.niiu().getUri(), TestSemanticUnit.USER_ID, Input.builder()
					.addResourceInput(Constants.NIIU_RESOURCE_POSITION_URI, new URI("http://purl.obolibrary.org/obo/ERROR_0000000"), "error", ResourceType.CLASS)
					.build(), itemResult.compound().getUri());
		});
		assertEquals("http://purl.obolibrary.org/obo/ERROR_0000000 does not satisfy all constraints [http://purl.obolibrary.org/obo/BFO_0000040]", exception.getMessage());
	}
	
	@Test
	default void testUpdateDeletedUnitError() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		StatementUnitResult statement = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult.compound().getUri());
		
		this.engine().deleteStatementUnit(statement.statement().getUri(), USER_ID, itemResult.compound().getUri());
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(statement.statement().getUri(), USER_ID, Input.builder().build(), itemResult.compound().getUri());
		});
		assertEquals("Tried to update deleted semantic unit " + statement.statement().getUri(), exception.getMessage());
	}
	
	@Test
	default void testUpdateResourceInputButIsLiteralInputError() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(itemResult.niiu().getUri(), USER_ID, Input.builder()
					.addLiteralInput(Constants.NIIU_RESOURCE_POSITION_URI, "eye")
					.build(), itemResult.compound().getUri());
		});
		assertEquals("Expected input for object position " + Constants.NIIU_RESOURCE_POSITION_URI + " to be of type resource", exception.getMessage());
	}
	
	@Test
	default void testUpdateLiteralInputButIsResourceInputError() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(itemResult.niiu().getUri(), USER_ID, Input.builder()
					.addResourceInput(Constants.NIIU_LITERAL_POSITION_URI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
					.build(), itemResult.compound().getUri());
		});
		assertEquals("Expected input for object position " + Constants.NIIU_LITERAL_POSITION_URI + " to be of type literal", exception.getMessage());
	}
	
	@Test
	default void testUpdateItemUnitError() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(itemResult.compound().getUri(), USER_ID, Input.builder().build(), null);
		});
		assertEquals("Expected semantic unit " + ITEM_KGBBI + " to be of type " + SemanticUnitType.STATEMENT, exception.getMessage());
	}
	
	@Test
	default void testInvalidUserInputError() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(itemResult.niiu().getUri(), new URI(""), Input.builder().build(), itemResult.compound().getUri());
		});
		assertEquals("Invalid user ", exception.getMessage());
	}
	
	@Test
	default void testStatementAndItemUnitDoNotShareSubjectError() throws Exception
	{
		CompoundUnitResult itemResult1 = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		StatementUnitResult statement = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult1.compound().getUri());
		
		CompoundUnitResult itemResult2 = statement.compounds().get(STATEMENT_RESOURCE_POSITION);
		
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().updateStatementUnit(statement.statement().getUri(), USER_ID, Input.builder()
					.addResourceInput(STATEMENT_RESOURCE_POSITION, itemResult2.compound().getUri(), "eye", ResourceType.INSTANCE)
					.build(), itemResult2.compound().getUri());
		});
		assertEquals("Statement unit and compound unit must share the same subject", exception.getMessage());
	}
}
