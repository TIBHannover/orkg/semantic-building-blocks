package integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.util.Constants;
import util.TestSemanticUnit;

public interface TestDeleteCompoundUnitUseCase extends TestSemanticUnit
{
	static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	static final URI STATEMENT_KGBBI = new URI("HasPartStatementKGBBInstance");
	static final URI STATEMENT_RESOURCE_POSITION = new URI("HasPartStatementUnitResourceObjectPosition");
	
	@BeforeEach
	default void setup() throws Exception
	{
		TestSemanticUnit.super.setup();
		this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
	}
	
	@Test
	default void testDeleteStandaloneItemUnit() throws Exception
	{
		this.engine().deleteCompoundUnit(new URI("5"), USER_ID);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(false);
		expectedItem.setDeletedBy(USER_ID);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(false);
		expectedNiiu.setDeletedBy(USER_ID);
//		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("3"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000468"));
		niiuResourceObject.setResourceLabel("multicellular organism");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		niiuResourceObject.setCurrentVersion(false);
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setLiteral("multicellular organism");
		niiuLiteralObject.setCurrentVersion(false);
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri(), false);
		
		SemanticUnit actualItem = this.engine().findSemanticUnit(new URI("5"));
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
		TestSemanticUnit.assertSemanticUnitEquals(expectedNiiu, this.engine().findSemanticUnit(new URI("1")));
	}
	
	@Test
	default void testDeleteItemUnitReferencedBySingleItemUnit() throws Exception
	{
		StatementUnitResult statement = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), new URI("5"));
		
		CompoundUnitResult itemResult = statement.compounds().get(STATEMENT_RESOURCE_POSITION);
		
		this.engine().deleteCompoundUnit(itemResult.compound().getUri(), USER_ID);
		
		SemanticUnit expectedReferencingItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedReferencingItem.setUri(new URI("5"));
		expectedReferencingItem.setSemanticUnitsGraph(new URI("6"));
		expectedReferencingItem.setSubject(new URI("0"));
		expectedReferencingItem.setLabel("Material Entity Item Unit");
		expectedReferencingItem.setDescription("Material Entity Item Unit Description");
		expectedReferencingItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedReferencingItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedReferencingItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedReferencingItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedReferencingItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedReferencingItem.setCurrentVersion(true);
		expectedReferencingItem.setDeletedBy(null);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu.setUri(new URI("7"));
		expectedHpsu.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu.setSubject(new URI("0"));
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(itemResult.compound().getUri(), false);
		expectedHpsu.setCurrentVersion(true);
		expectedHpsu.setDeletedBy(null);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject.setInputTypeLabel("has-part-resource");
		hpsuResourceObject.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject.setCurrentVersion(true);
		hpsuResourceObject.setResourceURI(itemResult.compound().getSubject());
		hpsuResourceObject.setResourceLabel(null);
		hpsuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject);
		
		expectedReferencingItem.addAssociatedSemanticUnit(expectedHpsu.getUri());
		expectedReferencingItem.addAssociatedSemanticUnit(new URI("1")); //NUUI of subject unit
		expectedReferencingItem.addAssociatedSemanticUnit(itemResult.niiu().getUri());
		
		expectedReferencingItem.addLinkedSemanticUnit(itemResult.compound().getUri(), false);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("14"));
		expectedItem.setSemanticUnitsGraph(new URI("15"));
		expectedItem.setSubject(new URI("9"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(false);
		expectedItem.setDeletedBy(USER_ID);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("10"));
		expectedNiiu.setSemanticUnitsGraph(new URI("11"));
		expectedNiiu.setSubject(new URI("9"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
//		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri(), false);
		
		SemanticUnit actualReferencingItem = this.engine().findSemanticUnit(new URI("5"));
		SemanticUnit actualHpsu = this.engine().findSemanticUnit(statement.statement().getUri());
		SemanticUnit actualItem = this.engine().findSemanticUnit(itemResult.compound().getUri());
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedReferencingItem, actualReferencingItem);
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu, actualHpsu);
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
	}
	
	@Test
	default void testDeleteItemUnitReferencedMultiBySingleItemUnit() throws Exception
	{
		StatementUnitResult statement1 = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), new URI("5"));
		
		CompoundUnitResult itemResult1 = statement1.compounds().get(STATEMENT_RESOURCE_POSITION);
		
		StatementUnitResult statement2 = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, itemResult1.compound().getUri(), null, ResourceType.INSTANCE)
				.build(), new URI("5"));
		
		this.engine().deleteCompoundUnit(itemResult1.compound().getUri(), USER_ID);
		
		SemanticUnit expectedReferencingItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedReferencingItem.setUri(new URI("5"));
		expectedReferencingItem.setSemanticUnitsGraph(new URI("6"));
		expectedReferencingItem.setSubject(new URI("0"));
		expectedReferencingItem.setLabel("Material Entity Item Unit");
		expectedReferencingItem.setDescription("Material Entity Item Unit Description");
		expectedReferencingItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedReferencingItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedReferencingItem.setCreator(USER_ID);
//		expectedReferencingItem.setCreationDate(LocalDateTime.now());
		expectedReferencingItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedReferencingItem.setLicense(null);
//		expectedReferencingItem.addAccessRestrictedTo(null);
		expectedReferencingItem.setCurrentVersion(true);
		expectedReferencingItem.setDeletedBy(null);
//		expectedReferencingItem.setDeletionDate(null);
		
		SemanticUnit expectedHpsu1 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu1.setUri(new URI("7"));
		expectedHpsu1.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu1.setSubject(new URI("0"));
		expectedHpsu1.setLabel("Has Part Statement Unit");
		expectedHpsu1.setDescription("Has Part Statement Unit Description");
		expectedHpsu1.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu1.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu1.setCreator(USER_ID);
//		expectedHpsu1.setCreationDate(LocalDateTime.now());
		expectedHpsu1.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu1.setLicense(null);
//		expectedHpsu1.addAccessRestrictedTo(null);
		expectedHpsu1.addObjectDescribedBySemanticUnit(itemResult1.compound().getUri(), false);
		expectedHpsu1.setCurrentVersion(true);
		expectedHpsu1.setDeletedBy(null);
		expectedHpsu1.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject1 = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject1.setInputTypeLabel("has-part-resource");
		hpsuResourceObject1.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject1.setCreator(USER_ID);
//		hpsuResourceObject1.setCreationDate(LocalDateTime.now());
		hpsuResourceObject1.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject1.setCurrentVersion(true);
		hpsuResourceObject1.setResourceURI(itemResult1.compound().getSubject());
		hpsuResourceObject1.setResourceLabel(null);
		hpsuResourceObject1.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu1.addResourceObject(hpsuResourceObject1);
		
		SemanticUnit expectedHpsu2 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu2.setUri(new URI("17"));
		expectedHpsu2.setSemanticUnitsGraph(new URI("18"));
		expectedHpsu2.setSubject(new URI("0"));
		expectedHpsu2.setLabel("Has Part Statement Unit");
		expectedHpsu2.setDescription("Has Part Statement Unit Description");
		expectedHpsu2.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu2.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu2.setCreator(USER_ID);
//		expectedHpsu2.setCreationDate(LocalDateTime.now());
		expectedHpsu2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu2.setLicense(null);
//		expectedHpsu2.addAccessRestrictedTo(null);
		expectedHpsu2.addObjectDescribedBySemanticUnit(itemResult1.compound().getUri(), false);
		expectedHpsu2.setCurrentVersion(true);
		expectedHpsu2.setDeletedBy(null);
		expectedHpsu2.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject2 = new ResourceObjectNode(new URI("19"));
		hpsuResourceObject2.setInputTypeLabel("has-part-resource");
		hpsuResourceObject2.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject2.setCreator(USER_ID);
//		hpsuResourceObject2.setCreationDate(LocalDateTime.now());
		hpsuResourceObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject2.setCurrentVersion(true);
		hpsuResourceObject2.setResourceURI(itemResult1.compound().getSubject());
		hpsuResourceObject2.setResourceLabel(null);
		hpsuResourceObject2.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu2.addResourceObject(hpsuResourceObject2);
		
		expectedReferencingItem.addAssociatedSemanticUnit(expectedHpsu1.getUri());
		expectedReferencingItem.addAssociatedSemanticUnit(expectedHpsu2.getUri());
		expectedReferencingItem.addAssociatedSemanticUnit(new URI("1")); //NUUI of subject unit
		expectedReferencingItem.addAssociatedSemanticUnit(itemResult1.niiu().getUri());
		
		expectedReferencingItem.addLinkedSemanticUnit(itemResult1.compound().getUri(), false);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("14"));
		expectedItem.setSemanticUnitsGraph(new URI("15"));
		expectedItem.setSubject(new URI("9"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(false);
		expectedItem.setDeletedBy(USER_ID);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("10"));
		expectedNiiu.setSemanticUnitsGraph(new URI("11"));
		expectedNiiu.setSubject(new URI("9"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
//		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri(), false);
		
		SemanticUnit actualReferencingItem = this.engine().findSemanticUnit(new URI("5"));
		SemanticUnit actualHpsu1 = this.engine().findSemanticUnit(statement1.statement().getUri());
		SemanticUnit actualHpsu2 = this.engine().findSemanticUnit(statement2.statement().getUri());
		SemanticUnit actualItem = this.engine().findSemanticUnit(itemResult1.compound().getUri());
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedReferencingItem, actualReferencingItem);
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu1, actualHpsu1);
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu2, actualHpsu2);
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
	}
	
	@Test
	default void testDeleteItemUnitReferencedMulti() throws Exception
	{
		// multicellular1 hasPart eye
		// multicellular2 hasPart eye
		// delete eye
		
		StatementUnitResult statement1 = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), new URI("5"));
		
		CompoundUnitResult itemResult1 = statement1.compounds().get(STATEMENT_RESOURCE_POSITION);
		
		CompoundUnitResult itemResult2 = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		StatementUnitResult statement2 = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, itemResult1.compound().getUri(), "eye", ResourceType.INSTANCE)
				.build(), itemResult2.compound().getUri());
		
		this.engine().deleteCompoundUnit(itemResult1.compound().getUri(), USER_ID);
		
		SemanticUnit expectedReferencingItem1 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedReferencingItem1.setUri(new URI("5"));
		expectedReferencingItem1.setSemanticUnitsGraph(new URI("6"));
		expectedReferencingItem1.setSubject(new URI("0"));
		expectedReferencingItem1.setLabel("Material Entity Item Unit");
		expectedReferencingItem1.setDescription("Material Entity Item Unit Description");
		expectedReferencingItem1.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedReferencingItem1.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedReferencingItem1.setCreator(USER_ID);
//		expectedReferencingItem1.setCreationDate(LocalDateTime.now());
		expectedReferencingItem1.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedReferencingItem1.setLicense(null);
//		expectedReferencingItem1.addAccessRestrictedTo(null);
		expectedReferencingItem1.setCurrentVersion(true);
		expectedReferencingItem1.setDeletedBy(null);
//		expectedReferencingItem1.setDeletionDate(null);
		
		SemanticUnit expectedReferencingItem2 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedReferencingItem2.setUri(new URI("22"));
		expectedReferencingItem2.setSemanticUnitsGraph(new URI("23"));
		expectedReferencingItem2.setSubject(new URI("17"));
		expectedReferencingItem2.setLabel("Material Entity Item Unit");
		expectedReferencingItem2.setDescription("Material Entity Item Unit Description");
		expectedReferencingItem2.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedReferencingItem2.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedReferencingItem2.setCreator(USER_ID);
//		expectedReferencingItem2.setCreationDate(LocalDateTime.now());
		expectedReferencingItem2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedReferencingItem2.setLicense(null);
//		expectedReferencingItem2.addAccessRestrictedTo(null);
		expectedReferencingItem2.setCurrentVersion(true);
		expectedReferencingItem2.setDeletedBy(null);
//		expectedReferencingItem2.setDeletionDate(null);
		
		SemanticUnit expectedHpsu1 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu1.setUri(new URI("7"));
		expectedHpsu1.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu1.setSubject(new URI("0"));
		expectedHpsu1.setLabel("Has Part Statement Unit");
		expectedHpsu1.setDescription("Has Part Statement Unit Description");
		expectedHpsu1.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu1.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu1.setCreator(USER_ID);
//		expectedHpsu1.setCreationDate(LocalDateTime.now());
		expectedHpsu1.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu1.setLicense(null);
//		expectedHpsu1.addAccessRestrictedTo(null);
		expectedHpsu1.addObjectDescribedBySemanticUnit(itemResult1.compound().getUri(), false);
		expectedHpsu1.setCurrentVersion(true);
		expectedHpsu1.setDeletedBy(null);
		expectedHpsu1.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject1 = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject1.setInputTypeLabel("has-part-resource");
		hpsuResourceObject1.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject1.setCreator(USER_ID);
//		hpsuResourceObject1.setCreationDate(LocalDateTime.now());
		hpsuResourceObject1.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject1.setCurrentVersion(true);
		hpsuResourceObject1.setResourceURI(itemResult1.compound().getSubject());
		hpsuResourceObject1.setResourceLabel(null);
		hpsuResourceObject1.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu1.addResourceObject(hpsuResourceObject1);
		
		SemanticUnit expectedHpsu2 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu2.setUri(new URI("24"));
		expectedHpsu2.setSemanticUnitsGraph(new URI("25"));
		expectedHpsu2.setSubject(itemResult2.compound().getSubject()); //17
		expectedHpsu2.setLabel("Has Part Statement Unit");
		expectedHpsu2.setDescription("Has Part Statement Unit Description");
		expectedHpsu2.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu2.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu2.setCreator(USER_ID);
//		expectedHpsu2.setCreationDate(LocalDateTime.now());
		expectedHpsu2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu2.setLicense(null);
//		expectedHpsu2.addAccessRestrictedTo(null);
		expectedHpsu2.addObjectDescribedBySemanticUnit(itemResult1.compound().getUri(), false);
		expectedHpsu2.setCurrentVersion(true);
		expectedHpsu2.setDeletedBy(null);
		expectedHpsu2.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject2 = new ResourceObjectNode(new URI("26"));
		hpsuResourceObject2.setInputTypeLabel("has-part-resource");
		hpsuResourceObject2.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject2.setCreator(USER_ID);
//		hpsuResourceObject2.setCreationDate(LocalDateTime.now());
		hpsuResourceObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject2.setCurrentVersion(true);
		hpsuResourceObject2.setResourceURI(itemResult1.compound().getSubject());
		hpsuResourceObject2.setResourceLabel(null);
		hpsuResourceObject2.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu2.addResourceObject(hpsuResourceObject2);
		
		expectedReferencingItem1.addAssociatedSemanticUnit(expectedHpsu1.getUri());
		expectedReferencingItem1.addAssociatedSemanticUnit(new URI("1")); //NUUI of subject unit
		expectedReferencingItem1.addAssociatedSemanticUnit(itemResult1.niiu().getUri());
		
		expectedReferencingItem1.addLinkedSemanticUnit(itemResult1.compound().getUri(), false);
		
		expectedReferencingItem2.addAssociatedSemanticUnit(expectedHpsu2.getUri());
		expectedReferencingItem2.addAssociatedSemanticUnit(itemResult2.niiu().getUri());
		expectedReferencingItem2.addAssociatedSemanticUnit(itemResult1.niiu().getUri());
		
//		System.out.println("expectedHpsu2 " + expectedHpsu2.getUri()); 18
//		System.out.println("itemResult2.niiu().getUri() " + itemResult2.niiu().getUri()); 19
//		System.out.println("itemResult1.niiu().getUri() " + itemResult1.niiu().getUri()); 11
		
		expectedReferencingItem2.addLinkedSemanticUnit(itemResult1.compound().getUri(), false);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("14"));
		expectedItem.setSemanticUnitsGraph(new URI("15"));
		expectedItem.setSubject(new URI("9"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(false);
		expectedItem.setDeletedBy(USER_ID);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("10"));
		expectedNiiu.setSemanticUnitsGraph(new URI("11"));
		expectedNiiu.setSubject(new URI("9"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
//		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri(), false);
		
		SemanticUnit actualReferencingItem1 = this.engine().findSemanticUnit(new URI("5"));
		SemanticUnit actualReferencingItem2 = this.engine().findSemanticUnit(itemResult2.compound().getUri());
		SemanticUnit actualHpsu1 = this.engine().findSemanticUnit(statement1.statement().getUri());
		SemanticUnit actualHpsu2 = this.engine().findSemanticUnit(statement2.statement().getUri());
		SemanticUnit actualItem = this.engine().findSemanticUnit(itemResult1.compound().getUri());
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedReferencingItem1, actualReferencingItem1);
		TestSemanticUnit.assertSemanticUnitEquals(expectedReferencingItem2, actualReferencingItem2);
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu1, actualHpsu1);
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu2, actualHpsu2);
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
	}
	
	@Test
	default void testInvalidUserInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().deleteCompoundUnit(new URI("5"), new URI(""));
		});
		assertEquals("Invalid user ", exception.getMessage());
	}
	
	@Test
	default void testDeleteItemWhereSubjectHasMultipleItems() throws Exception
	{
		CompoundUnitResult result = this.engine().createCompoundUnit(new URI("WeightMeasurementCompoundKGBBInstance"), new URI("KGBBEngineUser"), Input.builder()
				.addResourceInput(new URI("WeightMeasurementCompoundKGBBInstance"), new URI("0"), null, ResourceType.INSTANCE)
				.addResourceInput(new URI("QualityStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/PATO_0000128"), "weight", ResourceType.CLASS)
				.addResourceInput(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/NCIT_C28252"), null, ResourceType.CLASS)
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition1"), String.valueOf(5.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition2"), String.valueOf(4.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition3"), String.valueOf(6.0))
				.build(), new URI("5"));
		
		this.engine().deleteCompoundUnit(result.compound().getUri(), USER_ID);
		
		SemanticUnit expectedNiiu2 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu2.setUri(new URI("12"));
		expectedNiiu2.setSemanticUnitsGraph(new URI("13"));
		expectedNiiu2.setSubject(new URI("11"));
		expectedNiiu2.setLabel("Named Individual Identification Unit");
		expectedNiiu2.setDescription("Named Individual Identification Unit Description");
		expectedNiiu2.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu2.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu2.setCreator(USER_ID);
//		expectedNiiu2.setCreationDate(LocalDateTime.now());
		expectedNiiu2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu2.setLicense(null);
//		expectedNiiu2.addAccessRestrictedTo(null);
		expectedNiiu2.setCurrentVersion(false);
		expectedNiiu2.setDeletedBy(USER_ID);
		expectedNiiu2.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject2 = new ResourceObjectNode(new URI("14"));
		niiuResourceObject2.setInputTypeLabel("Class Affilitation");
		niiuResourceObject2.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject2.setCreator(USER_ID);
//		niiuResourceObject2.setCreationDate(LocalDateTime.now());
		niiuResourceObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject2.setCurrentVersion(false);
		niiuResourceObject2.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject2.setResourceLabel("eye");
		niiuResourceObject2.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu2.addResourceObject(niiuResourceObject2);
		
		LiteralObjectNode niiuLiteralObject2 = new LiteralObjectNode(new URI("15"));
		niiuLiteralObject2.setInputTypeLabel("niiu-label");
		niiuLiteralObject2.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject2.setCreator(USER_ID);
//		niiuLiteralObject2.setCreationDate(LocalDateTime.now());
		niiuLiteralObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject2.setCurrentVersion(false);
		niiuLiteralObject2.setLiteral("eye");
		expectedNiiu2.addLiteralObject(niiuLiteralObject2);
		
		SemanticUnit expectedWmu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("WeightMeasurementStatementKGBB")));
		expectedWmu.setUri(new URI("16"));
		expectedWmu.setSemanticUnitsGraph(new URI("17"));
		expectedWmu.setSubject(new URI("11"));
		expectedWmu.setLabel("Weight Measurement Statement Unit");
		expectedWmu.setDescription("Weight Measurement Statement Unit Description");
		expectedWmu.setKgbb(new URI("WeightMeasurementStatementKGBB"));
		expectedWmu.setKgbbi(new URI("WeightMeasurementStatementKGBBInstance"));
		expectedWmu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedWmu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedWmu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedWmu.setCurrentVersion(false);
		expectedWmu.setDeletedBy(USER_ID);
		expectedWmu.setDeletionDate(null);
		
		ResourceObjectNode wmuResourceObject = new ResourceObjectNode(new URI("18"));
		wmuResourceObject.setInputTypeLabel("Unit");
		wmuResourceObject.setObjectPositionClass(new URI("WeightMeasurementUnitResourceObjectPosition"));
		wmuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		wmuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuResourceObject.setCurrentVersion(false);
		wmuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/NCIT_C28252"));
		wmuResourceObject.setResourceLabel("Kilogram");
		wmuResourceObject.setConstraint(null);
		expectedWmu.addResourceObject(wmuResourceObject);
		
		LiteralObjectNode wmuLiteralObject = new LiteralObjectNode(new URI("19"));
		wmuLiteralObject.setInputTypeLabel("weight-value-label");
		wmuLiteralObject.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition1"));
		wmuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		wmuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject.setCurrentVersion(false);
		wmuLiteralObject.setLiteral(5.0);
		expectedWmu.addLiteralObject(wmuLiteralObject);
		
		LiteralObjectNode wmuLiteralObject2 = new LiteralObjectNode(new URI("18"));
		wmuLiteralObject2.setInputTypeLabel("lower-bound-confidence-interval-label");
		wmuLiteralObject2.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition2"));
		wmuLiteralObject2.setCreator(USER_ID);
//		wmuLiteralObject2.setCreationDate(LocalDateTime.now());
		wmuLiteralObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject2.setCurrentVersion(false);
		wmuLiteralObject2.setLiteral(4.0);
		expectedWmu.addLiteralObject(wmuLiteralObject2);
		
		LiteralObjectNode wmuLiteralObject3 = new LiteralObjectNode(new URI("21"));
		wmuLiteralObject3.setInputTypeLabel("upper-bound-confidence-interval-label");
		wmuLiteralObject3.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition3"));
		wmuLiteralObject3.setCreator(USER_ID);
//		wmuLiteralObject3.setCreationDate(LocalDateTime.now());
		wmuLiteralObject3.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject3.setCurrentVersion(false);
		wmuLiteralObject3.setLiteral(6.0);
		expectedWmu.addLiteralObject(wmuLiteralObject3);
		
		SemanticUnit expectedQsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("QualityStatementKGBB")));
		expectedQsu.setUri(new URI("9"));
		expectedQsu.setSemanticUnitsGraph(new URI("10"));
		expectedQsu.setSubject(new URI("0"));
		expectedQsu.setLabel("Quality Statement Unit");
		expectedQsu.setDescription("Quality Statement Unit Description");
		expectedQsu.setKgbb(new URI("QualityStatementKGBB"));
		expectedQsu.setKgbbi(new URI("QualityStatementKGBBInstance"));
		expectedQsu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedQsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedQsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedQsu.addObjectDescribedBySemanticUnit(expectedWmu.getUri());
		expectedQsu.setCurrentVersion(false);
		expectedQsu.setDeletedBy(USER_ID);
		expectedQsu.setDeletionDate(null);
		
		ResourceObjectNode qsuResourceObject = new ResourceObjectNode(new URI("20"));
		qsuResourceObject.setInputTypeLabel("Quality");
		qsuResourceObject.setObjectPositionClass(new URI("QualityStatementUnitResourceObjectPosition"));
		qsuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		qsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		qsuResourceObject.setCurrentVersion(false);
		qsuResourceObject.setResourceURI(expectedNiiu2.getSubject());
		qsuResourceObject.setResourceLabel(null);
		qsuResourceObject.setConstraint(null);
		expectedQsu.addResourceObject(qsuResourceObject);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("3"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000468"));
		niiuResourceObject.setResourceLabel("multicellular organism");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("multicellular organism");
		expectedNiiu.addLiteralObject(niiuLiteralObject);

		SemanticUnit expectedItem2 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("WeightMeasurementCompoundKGBB")));
		expectedItem2.setUri(new URI("7"));
		expectedItem2.setSemanticUnitsGraph(new URI("8"));
		expectedItem2.setSubject(new URI("0"));
		expectedItem2.setLabel("Weight Measurement Compound Unit");
		expectedItem2.setDescription("Weight Measurement Compound Unit Description");
		expectedItem2.setKgbb(new URI("WeightMeasurementCompoundKGBB"));
		expectedItem2.setKgbbi(new URI("WeightMeasurementCompoundKGBBInstance"));
		expectedItem2.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem2.setCreationDate(LocalDateTime.now());
		expectedItem2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem2.setLicense(null);
//		expectedItem2.addAccessRestrictedTo(null);
		expectedItem2.setCurrentVersion(false);
		expectedItem2.setDeletedBy(USER_ID);
		expectedItem2.setDeletionDate(null);
		expectedItem2.addAssociatedSemanticUnit(expectedNiiu2.getUri(), false);
		expectedItem2.addAssociatedSemanticUnit(expectedQsu.getUri(), false);
		expectedItem2.addAssociatedSemanticUnit(expectedNiiu.getUri(), false);
		expectedItem2.addAssociatedSemanticUnit(expectedWmu.getUri(), false);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri());
		expectedItem.addAssociatedSemanticUnit(expectedItem2.getUri(), false);
		
		SemanticUnit actualItem = this.engine().findSemanticUnit(new URI("5"));
		SemanticUnit actualNiiu = this.engine().findSemanticUnit(new URI("1"));
		SemanticUnit actualItem2 = this.engine().findSemanticUnit(new URI("7"));
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
		TestSemanticUnit.assertSemanticUnitEquals(expectedNiiu, actualNiiu);
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem2, actualItem2);
	}
}
