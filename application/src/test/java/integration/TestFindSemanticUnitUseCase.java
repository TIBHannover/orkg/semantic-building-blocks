package integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.util.Constants;
import util.TestSemanticUnit;

public interface TestFindSemanticUnitUseCase extends TestSemanticUnit
{
	static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	static final URI STATEMENT_KGBBI = new URI("HasPartStatementKGBBInstance");
	static final URI STATEMENT_RESOURCE_POSITION = new URI("HasPartStatementUnitResourceObjectPosition");
	
	@Test
	default void testFindStatementUnit() throws Exception
	{
		this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		StatementUnitResult statement = this.engine().createStatementUnit(STATEMENT_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), new URI("5"));
		CompoundUnitResult itemResult = statement.compounds().get(STATEMENT_RESOURCE_POSITION);
		
		SemanticUnit actualHpsu = this.engine().findSemanticUnit(statement.statement().getUri());
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedHpsu.setUri(new URI("7"));
		expectedHpsu.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu.setSubject(new URI("0"));
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(TestSemanticUnit.USER_ID);
//		expectedHpsu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedHpsu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(itemResult.compound().getUri());
		expectedHpsu.setCurrentVersion(true);
		expectedHpsu.setDeletedBy(null);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject.setInputTypeLabel("has-part-resource");
		hpsuResourceObject.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject.setCreator(USER_ID);
//		hpsuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject.setCurrentVersion(true);
		hpsuResourceObject.setResourceURI(itemResult.niiu().getSubject());
		hpsuResourceObject.setResourceLabel(null);
		hpsuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject);
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu, actualHpsu);
	}
	
	@Test
	default void testFindItemUnit() throws Exception
	{
		CompoundUnitResult itemResult = this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		SemanticUnit actualItem = this.engine().findSemanticUnit(itemResult.compound().getUri());
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		
		expectedItem.addAssociatedSemanticUnit(new URI("1")); //NIIU of subject
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
	}
	
	@Test
	default void testFindMissingUnit() throws Exception
	{
		NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
		{
			this.engine().findSemanticUnit(new URI("Missing"));
		});
		assertEquals("Tried to find missing semantic unit Missing", exception.getMessage());
	}
}
