package integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.spi.OntologyServiceException;
import util.TestCompoundUnit;
import util.TestSemanticUnit;

public interface TestCreateCompoundUnitUseCase extends TestSemanticUnit
{
	static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	
	@BeforeEach
	default void setup() throws Exception
	{
		TestSemanticUnit.super.setup();
	}
	
	@Test
	default void testCreateItemUnit() throws Exception
	{
		CompoundUnitResult actual = this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("3"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000468"));
		niiuResourceObject.setResourceLabel("multicellular organism");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("multicellular organism");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri());
		
		TestCompoundUnit.assertCompoundResultEquals(new CompoundUnitResult(expectedItem, expectedNiiu, null, Collections.emptyList()), actual);
	}
	
	@Test
	default void testMissingInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder().build(), null);
		});
		assertEquals("Missing input for compound unit MaterialEntityItemKGBBInstance", exception.getMessage());
	}
	
	@Test
	default void testCreateItemUnitWithExistingNiiu() throws Exception
	{
		CompoundUnitResult base = this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		CompoundUnitResult actual = this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, base.niiu().getSubject(), "multicellular organism", ResourceType.INSTANCE)
				.build(), null);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("7"));
		expectedItem.setSemanticUnitsGraph(new URI("8"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("3"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000468"));
		niiuResourceObject.setResourceLabel("multicellular organism");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("multicellular organism");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri());
		
		TestCompoundUnit.assertCompoundResultEquals(new CompoundUnitResult(expectedItem, expectedNiiu, null, Collections.emptyList()), actual);
	}
	
	@Test
	default void testLiteralInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
					.addLiteralInput(ITEM_KGBBI, null)
					.build(), null);
		});
		assertEquals("Missing input for compound unit MaterialEntityItemKGBBInstance", exception.getMessage());
	}
	
	@Test
	default void testEmptyInputError() throws Exception
	{
		assertThrows(OntologyServiceException.class, () ->
		{
			this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
					.addResourceInput(ITEM_KGBBI, new URI(""), null, ResourceType.CLASS)
					.build(), null);
		});
	}
	
	@Test
	default void testMissingKggbiInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createCompoundUnit(new URI("Missing"), TestSemanticUnit.USER_ID, Input.builder().build(), null);
		});
		assertEquals("Tried to find missing kggbi Missing", exception.getMessage());
	}
	
	@Test
	default void testConstraintViolationInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
					.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/ERROR_0000000"), "error", ResourceType.CLASS)
					.build(), null);
		});
		assertEquals("http://purl.obolibrary.org/obo/ERROR_0000000 is not a subclass of http://purl.obolibrary.org/obo/BFO_0000040", exception.getMessage());
	}
	
	@Test
	default void testInvalidUserInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().createCompoundUnit(ITEM_KGBBI, new URI(""), Input.builder()
					.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
					.build(), null);
		});
		assertEquals("Invalid user ", exception.getMessage());
	}
	
	@Test
	default void testCreateItemUnitWithSubjectUnit() throws Exception
	{
		CompoundUnitResult actual1 = this.engine().createCompoundUnit(ITEM_KGBBI, TestSemanticUnit.USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		CompoundUnitResult actual2 = this.engine().createCompoundUnit(new URI("WeightMeasurementCompoundKGBBInstance"), new URI("KGBBEngineUser"), Input.builder()
				.addResourceInput(new URI("WeightMeasurementCompoundKGBBInstance"), actual1.compound().getSubject(), null, ResourceType.INSTANCE)
				.addResourceInput(new URI("QualityStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/PATO_0000128"), "weight", ResourceType.CLASS)
				.addResourceInput(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/NCIT_C28252"), null, ResourceType.CLASS)
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition1"), String.valueOf(5.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition2"), String.valueOf(4.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition3"), String.valueOf(6.0))
				.build(), actual1.compound().getUri());
		
		SemanticUnit expectedNiiu2 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu2.setUri(new URI("12"));
		expectedNiiu2.setSemanticUnitsGraph(new URI("13"));
		expectedNiiu2.setSubject(new URI("11"));
		expectedNiiu2.setLabel("Named Individual Identification Unit");
		expectedNiiu2.setDescription("Named Individual Identification Unit Description");
		expectedNiiu2.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu2.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu2.setCreator(USER_ID);
//		expectedNiiu2.setCreationDate(LocalDateTime.now());
		expectedNiiu2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu2.setLicense(null);
//		expectedNiiu2.addAccessRestrictedTo(null);
		expectedNiiu2.setCurrentVersion(true);
		expectedNiiu2.setDeletedBy(null);
		expectedNiiu2.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject2 = new ResourceObjectNode(new URI("14"));
		niiuResourceObject2.setInputTypeLabel("Class Affilitation");
		niiuResourceObject2.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject2.setCreator(USER_ID);
//		niiuResourceObject2.setCreationDate(LocalDateTime.now());
		niiuResourceObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject2.setCurrentVersion(true);
		niiuResourceObject2.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject2.setResourceLabel("eye");
		niiuResourceObject2.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu2.addResourceObject(niiuResourceObject2);
		
		LiteralObjectNode niiuLiteralObject2 = new LiteralObjectNode(new URI("15"));
		niiuLiteralObject2.setInputTypeLabel("niiu-label");
		niiuLiteralObject2.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject2.setCreator(USER_ID);
//		niiuLiteralObject2.setCreationDate(LocalDateTime.now());
		niiuLiteralObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject2.setCurrentVersion(true);
		niiuLiteralObject2.setLiteral("eye");
		expectedNiiu2.addLiteralObject(niiuLiteralObject2);
		
		SemanticUnit expectedWmu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("WeightMeasurementStatementKGBB")));
		expectedWmu.setUri(new URI("16"));
		expectedWmu.setSemanticUnitsGraph(new URI("17"));
		expectedWmu.setSubject(new URI("11"));
		expectedWmu.setLabel("Weight Measurement Statement Unit");
		expectedWmu.setDescription("Weight Measurement Statement Unit Description");
		expectedWmu.setKgbb(new URI("WeightMeasurementStatementKGBB"));
		expectedWmu.setKgbbi(new URI("WeightMeasurementStatementKGBBInstance"));
		expectedWmu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedWmu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedWmu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedWmu.setCurrentVersion(true);
		expectedWmu.setDeletedBy(null);
		expectedWmu.setDeletionDate(null);
		expectedWmu.addAssociatedSemanticUnit(expectedNiiu2.getUri());
		
		ResourceObjectNode wmuResourceObject = new ResourceObjectNode(new URI("19"));
		wmuResourceObject.setInputTypeLabel("Unit");
		wmuResourceObject.setObjectPositionClass(new URI("WeightMeasurementUnitResourceObjectPosition"));
		wmuResourceObject.setCreator(USER_ID);
//		wmuResourceObject.setCreationDate(LocalDateTime.now());
		wmuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuResourceObject.setCurrentVersion(true);
		wmuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/NCIT_C28252"));
		wmuResourceObject.setResourceLabel("Kilogram");
		wmuResourceObject.setConstraint(null);
		expectedWmu.addResourceObject(wmuResourceObject);
		
		LiteralObjectNode wmuLiteralObject = new LiteralObjectNode(new URI("20"));
		wmuLiteralObject.setInputTypeLabel("weight-value-label");
		wmuLiteralObject.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition1"));
		wmuLiteralObject.setCreator(USER_ID);
//		wmuLiteralObject.setCreationDate(LocalDateTime.now());
		wmuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject.setCurrentVersion(true);
		wmuLiteralObject.setLiteral(5.0);
		expectedWmu.addLiteralObject(wmuLiteralObject);
		
		LiteralObjectNode wmuLiteralObject2 = new LiteralObjectNode(new URI("18"));
		wmuLiteralObject2.setInputTypeLabel("lower-bound-confidence-interval-label");
		wmuLiteralObject2.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition2"));
		wmuLiteralObject2.setCreator(USER_ID);
//		wmuLiteralObject2.setCreationDate(LocalDateTime.now());
		wmuLiteralObject2.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject2.setCurrentVersion(true);
		wmuLiteralObject2.setLiteral(4.0);
		expectedWmu.addLiteralObject(wmuLiteralObject2);
		
		LiteralObjectNode wmuLiteralObject3 = new LiteralObjectNode(new URI("21"));
		wmuLiteralObject3.setInputTypeLabel("upper-bound-confidence-interval-label");
		wmuLiteralObject3.setObjectPositionClass(new URI("WeightMeasurementUnitLiteralObjectPosition3"));
		wmuLiteralObject3.setCreator(USER_ID);
//		wmuLiteralObject3.setCreationDate(LocalDateTime.now());
		wmuLiteralObject3.setCreatedWithApplication(Constants.APPLICATION_URI);
		wmuLiteralObject3.setCurrentVersion(true);
		wmuLiteralObject3.setLiteral(6.0);
		expectedWmu.addLiteralObject(wmuLiteralObject3);
		
		SemanticUnit expectedQsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("QualityStatementKGBB")));
		expectedQsu.setUri(new URI("9"));
		expectedQsu.setSemanticUnitsGraph(new URI("10"));
		expectedQsu.setSubject(new URI("0"));
		expectedQsu.setLabel("Quality Statement Unit");
		expectedQsu.setDescription("Quality Statement Unit Description");
		expectedQsu.setKgbb(new URI("QualityStatementKGBB"));
		expectedQsu.setKgbbi(new URI("QualityStatementKGBBInstance"));
		expectedQsu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedQsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedQsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedQsu.addObjectDescribedBySemanticUnit(expectedWmu.getUri());
		expectedQsu.setCurrentVersion(true);
		expectedQsu.setDeletedBy(null);
		expectedQsu.setDeletionDate(null);
		
		ResourceObjectNode qsuResourceObject = new ResourceObjectNode(new URI("22"));
		qsuResourceObject.setInputTypeLabel("Quality");
		qsuResourceObject.setObjectPositionClass(new URI("QualityStatementUnitResourceObjectPosition"));
		qsuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		qsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		qsuResourceObject.setCurrentVersion(true);
		qsuResourceObject.setResourceURI(expectedNiiu2.getSubject());
		qsuResourceObject.setResourceLabel(null);
		qsuResourceObject.setConstraint(null);
		expectedQsu.addResourceObject(qsuResourceObject);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("3"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000468"));
		niiuResourceObject.setResourceLabel("multicellular organism");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("multicellular organism");
		expectedNiiu.addLiteralObject(niiuLiteralObject);

		SemanticUnit expectedItem2 = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("WeightMeasurementCompoundKGBB")));
		expectedItem2.setUri(new URI("7"));
		expectedItem2.setSemanticUnitsGraph(new URI("8"));
		expectedItem2.setSubject(new URI("0"));
		expectedItem2.setLabel("Weight Measurement Compound Unit");
		expectedItem2.setDescription("Weight Measurement Compound Unit Description");
		expectedItem2.setKgbb(new URI("WeightMeasurementCompoundKGBB"));
		expectedItem2.setKgbbi(new URI("WeightMeasurementCompoundKGBBInstance"));
		expectedItem2.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem2.setCreationDate(LocalDateTime.now());
		expectedItem2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem2.setLicense(null);
//		expectedItem2.addAccessRestrictedTo(null);
		expectedItem2.setCurrentVersion(true);
		expectedItem2.setDeletedBy(null);
		expectedItem2.setDeletionDate(null);
		expectedItem2.addAssociatedSemanticUnit(expectedNiiu2.getUri());
		expectedItem2.addAssociatedSemanticUnit(expectedQsu.getUri());
		expectedItem2.addAssociatedSemanticUnit(expectedNiiu.getUri());
		expectedItem2.addAssociatedSemanticUnit(expectedWmu.getUri());
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri());
		expectedItem.addAssociatedSemanticUnit(expectedItem2.getUri());
		
		TestCompoundUnit.assertCompoundResultEquals(new CompoundUnitResult(expectedItem2, expectedNiiu, expectedItem, List.of(new StatementUnitResult(expectedQsu, Collections.emptyMap(), expectedItem2, List.of(new StatementUnitResult(expectedWmu, Collections.emptyMap(), expectedQsu, Collections.emptyList(), Collections.emptyList(), Collections.emptyList())), List.of(expectedNiiu2), Collections.emptyList()))), actual2);
	}
	
	@Test
	default void testCreateItemUnitWithSubjectUnitMissingError() throws Exception
	{
		NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
		{
			this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
					.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
					.build(), new URI("Missing"));
		});
		assertEquals("Tried to find missing semantic unit Missing", exception.getMessage());
	}
	
//	@Test
//	default void testAssociationQuantityExceededError() throws Exception
//	{
//		TODO TEST implement if kgbbi exists for it
//	}
}
