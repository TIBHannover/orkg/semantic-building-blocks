package integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.util.Constants;
import util.TestSemanticUnit;

public interface TestDeleteStatementUnitUseCase extends TestSemanticUnit
{
	static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	static final URI STATEMENT_KGBBI = new URI("HasPartStatementKGBBInstance");
	static final URI STATEMENT_RESOURCE_POSITION = new URI("HasPartStatementUnitResourceObjectPosition");
	
	@BeforeEach
	default void setup() throws Exception
	{
		TestSemanticUnit.super.setup();
		this.engine().createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
	}
	
	@Test
	default void deleteStatementUnit() throws Exception
	{
		StatementUnitResult statement = this.engine().createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), new URI("5"));
		
		this.engine().deleteStatementUnit(statement.statement().getUri(), USER_ID, new URI("5"));
		
		CompoundUnitResult itemResult = statement.compounds().get(STATEMENT_RESOURCE_POSITION);
		
		SemanticUnit expectedItem = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.engine().getStorageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedHpsu.setUri(new URI("7"));
		expectedHpsu.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu.setSubject(new URI("0"));
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(itemResult.compound().getUri(), false);
		expectedHpsu.setCurrentVersion(false);
		expectedHpsu.setDeletedBy(USER_ID);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject.setInputTypeLabel("has-part-resource");
		hpsuResourceObject.setObjectPositionClass(STATEMENT_RESOURCE_POSITION);
		hpsuResourceObject.setCreator(USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject.setCurrentVersion(false);
		hpsuResourceObject.setResourceURI(itemResult.compound().getSubject());
		hpsuResourceObject.setResourceLabel(null);
		hpsuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedHpsu.getUri());
		expectedItem.removeAssociatedSemanticUnit(expectedHpsu.getUri());
		expectedItem.addAssociatedSemanticUnit(new URI("1")); //NUUI of subject unit
		expectedItem.addAssociatedSemanticUnit(itemResult.niiu().getUri());
		expectedItem.removeAssociatedSemanticUnit(itemResult.niiu().getUri());
		
		expectedItem.addLinkedSemanticUnit(itemResult.compound().getUri(), false);
		
		SemanticUnit actualItem = this.engine().findSemanticUnit(new URI("5"));
		SemanticUnit actualHpsu = this.engine().findSemanticUnit(statement.statement().getUri());
		
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
		TestSemanticUnit.assertSemanticUnitEquals(expectedHpsu, actualHpsu);
	}
	
	@Test
	default void testInvalidUserInputError() throws Exception
	{
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
		{
			this.engine().deleteStatementUnit(new URI("1"), new URI(""), new URI("6"));
		});
		assertEquals("Invalid user ", exception.getMessage());
	}
}
