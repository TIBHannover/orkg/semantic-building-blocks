package unit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.adapter.output.FakeOntologyLookup;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.spi.OntologyServiceException;

public class TestFakeOntologyService
{
	FakeOntologyLookup fakeOntologyLookup;
	
	@BeforeEach
	void setup()
	{
		this.fakeOntologyLookup = new FakeOntologyLookup();
	}
	
	@Test
	void testResourceLabel() throws Exception
	{
		assertEquals("multicellular organism", this.fakeOntologyLookup.resourceLabel(new URI("http://purl.obolibrary.org/obo/UBERON_0000468")));
		assertEquals("eye", this.fakeOntologyLookup.resourceLabel(new URI("http://purl.obolibrary.org/obo/UBERON_0000970")));
		
		OntologyServiceException error = assertThrows(OntologyServiceException.class, () ->
		{
			this.fakeOntologyLookup.resourceLabel(new URI("http://purl.obolibrary.org/obo/ERROR_0000000"));
		});
		assertTrue(error.getMessage().contains("Could not retrieve resource label"));
	}
	
	@Test
	void testHasAncestor() throws Exception
	{
		assertEquals(true, this.fakeOntologyLookup.hasAncestor(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), new URI("http://purl.obolibrary.org/obo/BFO_0000040")));
		assertEquals(false, this.fakeOntologyLookup.hasAncestor(new URI("http://purl.obolibrary.org/obo/BFO_0000040"), new URI("http://purl.obolibrary.org/obo/UBERON_0000970")));
	}
}
