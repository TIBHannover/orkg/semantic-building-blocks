package unit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.tib.kgbbengine.adapter.output.FakeInMemoryKGBBRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryStorageModelRepository;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.SemanticUnitStorageModel;
import eu.tib.kgbbengine.domain.storage.StorageModelManager;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;
import util.Delegate;
import util.SemanticUnitRepositorySupplier;
import util.TestSemanticUnit;

public interface TestSemanticUnitRepository extends SemanticUnitRepositorySupplier
{
	static Delegate<StorageModelManager> STORAGE_MODEL_MANAGER = new Delegate<StorageModelManager>();
	static Delegate<SemanticUnitRepository> SEMANTIC_UNIT_REPOSITORY = new Delegate<SemanticUnitRepository>();
	
	@BeforeAll
	static void setupAll() throws Exception
	{
		FakeInMemoryKGBBRepository kgbbRepo = new FakeInMemoryKGBBRepository();
		FakeInMemoryStorageModelRepository storageModelRepo = new FakeInMemoryStorageModelRepository();
		STORAGE_MODEL_MANAGER.set(new StorageModelManager(storageModelRepo, kgbbRepo));
	}
	
	@BeforeEach
	default void setup() throws Exception
	{
		SEMANTIC_UNIT_REPOSITORY.set(this.newSemanticUnitRepository(STORAGE_MODEL_MANAGER.get().getStorageModel(Constants.KGBB).getSemanticUnitStorageModel(), STORAGE_MODEL_MANAGER.get().getObjectStorageModel(Constants.RESOURCE_OBJECT_POSITION)));
	}
	
	default StorageModelManager storageModelManager()
	{
		return STORAGE_MODEL_MANAGER.get();
	}
	
	default SemanticUnitRepository semanticUnitRepository()
	{
		return SEMANTIC_UNIT_REPOSITORY.get();
	}
	
	@Test
	default void testWriteReadItem()
	{
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit expected = new SemanticUnit(model);
		expected.setUri(new URI("1"));
		expected.setSemanticUnitsGraph(new URI("2"));
		expected.setSubject(new URI("0"));
		expected.setLabel("Material Entity Item Unit");
		expected.setDescription("Material Entity Item Unit Description");
		expected.setKgbb(new URI("MaterialEntityItemKGBB"));
		expected.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expected.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		expected.setCreatedWithApplication(Constants.APPLICATION_URI);
		expected.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		expected.setCurrentVersion(true);
		expected.setDeletedBy(null);
		expected.setDeletionDate(null);
		
		this.semanticUnitRepository().save(expected);
		SemanticUnit actual = this.semanticUnitRepository().findByUpri(new URI("1"), model);
		TestSemanticUnit.assertSemanticUnitEquals(expected, actual);
	}
	
	@Test
	default void testWriteReadStatement()
	{
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI);
		SemanticUnit expected = new SemanticUnit(model);
		expected.setUri(new URI("1"));
		expected.setSemanticUnitsGraph(new URI("2"));
		expected.setSubject(new URI("0"));
		expected.setLabel("Named Individual Identification Unit");
		expected.setDescription("Named Individual Identification Unit Description");
		expected.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expected.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expected.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expected.setCreatedWithApplication(Constants.APPLICATION_URI);
		expected.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
//		expected.setObjectDescribedBySemanticUnit(null);
		expected.setCurrentVersion(true);
		expected.setDeletedBy(null);
		expected.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expected.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expected.addLiteralObject(niiuLiteralObject);
		
		this.semanticUnitRepository().save(expected);
		SemanticUnit actual = this.semanticUnitRepository().findByUpri(new URI("1"), model);
		TestSemanticUnit.assertSemanticUnitEquals(expected, actual);
	}
	
	@Test
	default void testFindBySubject()
	{
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit expected = new SemanticUnit(model);
		expected.setUri(new URI("1"));
		expected.setSemanticUnitsGraph(new URI("2"));
		expected.setSubject(new URI("0"));
		expected.setLabel("Material Entity Item Unit");
		expected.setDescription("Material Entity Item Unit Description");
		expected.setKgbb(new URI("MaterialEntityItemKGBB"));
		expected.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expected.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		expected.setCreatedWithApplication(Constants.APPLICATION_URI);
		expected.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		expected.setCurrentVersion(true);
		expected.setDeletedBy(null);
		expected.setDeletionDate(null);
		
		this.semanticUnitRepository().save(expected);
		SemanticUnit actual = this.semanticUnitRepository().findBySubject(expected.getSubject(), new URI("MaterialEntityItemKGBB"), model);
		TestSemanticUnit.assertSemanticUnitEquals(expected, actual);
	}
	
	@Test
	default void testFindBySubjectMissingError()
	{
		NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
		{
			this.semanticUnitRepository().findBySubject(new URI("Missing"), new URI("MissingKGBB"), this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		});
		assertEquals("Tried to find missing semantic unit with subject Missing", exception.getMessage());
	}
	
	@Test
	default void testFindBySubjectTooManyError()
	{
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit item1 = new SemanticUnit(model);
		item1.setUri(new URI("1"));
		item1.setSemanticUnitsGraph(new URI("2"));
		item1.setSubject(new URI("0"));
		item1.setLabel("Material Entity Item Unit");
		item1.setDescription("Material Entity Item Unit Description");
		item1.setKgbb(new URI("MaterialEntityItemKGBB"));
		item1.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		item1.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		item1.setCreatedWithApplication(Constants.APPLICATION_URI);
		item1.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		item1.setCurrentVersion(true);
		item1.setDeletedBy(null);
		item1.setDeletionDate(null);
		
		this.semanticUnitRepository().save(item1);
		
		SemanticUnit item2 = new SemanticUnit(model);
		item2.setUri(new URI("3"));
		item2.setSemanticUnitsGraph(new URI("4"));
		item2.setSubject(new URI("0"));
		item2.setLabel("Material Entity Item Unit");
		item2.setDescription("Material Entity Item Unit Description");
		item2.setKgbb(new URI("MaterialEntityItemKGBB"));
		item2.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		item2.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		item2.setCreatedWithApplication(Constants.APPLICATION_URI);
		item2.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		item2.setCurrentVersion(true);
		item2.setDeletedBy(null);
		item2.setDeletionDate(null);
		
		this.semanticUnitRepository().save(item2);
		
		IllegalStateException exception = assertThrows(IllegalStateException.class, () ->
		{
			this.semanticUnitRepository().findBySubject(item1.getSubject(), new URI("MaterialEntityItemKGBB"), model);
		});
		assertEquals("Storage contains multiple semantic units with type " + model.getSemanticUnitStorageModel().getName() + " for subject " + item1.getSubject(), exception.getMessage());
	}
	
	@Test
	default void testFindKggbi()
	{
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit expected = new SemanticUnit(model);
		expected.setUri(new URI("1"));
		expected.setSemanticUnitsGraph(new URI("2"));
		expected.setSubject(new URI("0"));
		expected.setLabel("Material Entity Item Unit");
		expected.setDescription("Material Entity Item Unit Description");
		expected.setKgbb(new URI("MaterialEntityItemKGBB"));
		expected.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expected.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		expected.setCreatedWithApplication(Constants.APPLICATION_URI);
		expected.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		expected.setCurrentVersion(true);
		expected.setDeletedBy(null);
		expected.setDeletionDate(null);
		
		this.semanticUnitRepository().save(expected);
		URI actual = this.semanticUnitRepository().findKggbi(expected.getUri());
		assertEquals(expected.getKgbbi(), actual);
	}
	
	@Test
	default void testFindKggbiUriBySemanticUnitError()
	{
		NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
		{
			this.semanticUnitRepository().findKggbi(new URI("Missing"));
		});
		assertEquals("Tried to find missing semantic unit Missing", exception.getMessage());
	}
	
	@Test
	default void testIsUnique()
	{
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit expected = new SemanticUnit(model);
		expected.setUri(new URI("1"));
		expected.setSemanticUnitsGraph(new URI("2"));
		expected.setSubject(new URI("0"));
		expected.setLabel("Material Entity Item Unit");
		expected.setDescription("Material Entity Item Unit Description");
		expected.setKgbb(new URI("MaterialEntityItemKGBB"));
		expected.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expected.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		expected.setCreatedWithApplication(Constants.APPLICATION_URI);
		expected.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		expected.setCurrentVersion(true);
		expected.setDeletedBy(null);
		expected.setDeletionDate(null);
		this.semanticUnitRepository().save(expected);
		
		assertEquals(false, this.semanticUnitRepository().isUnique(new URI("0")));
		assertEquals(false, this.semanticUnitRepository().isUnique(new URI("1")));
		assertEquals(false, this.semanticUnitRepository().isUnique(new URI("2")));
		assertEquals(true, this.semanticUnitRepository().isUnique(new URI("3")));
	}
	
	@Test
	default void testFindByUpriMissingError()
	{
		NoSuchElementException exception = assertThrows(NoSuchElementException.class, () ->
		{
			this.semanticUnitRepository().findByUpri(new URI("Missing"), this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		});
		assertEquals("Tried to find missing semantic unit Missing", exception.getMessage());
	}
	
	@Test
	default void testDelete()
	{
		SemanticUnitStorageModel niiuModel = this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI);
		SemanticUnit expectedNiiu = new SemanticUnit(niiuModel);
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		this.semanticUnitRepository().save(expectedNiiu);
		
		SemanticUnitStorageModel itemModel = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit expectedItem = new SemanticUnit(itemModel);
		expectedItem.setUri(new URI("3"));
		expectedItem.setSemanticUnitsGraph(new URI("4"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
		expectedItem.setDeletionDate(null);
		expectedItem.addAssociatedSemanticUnit(new URI("1"), true);
		
		this.semanticUnitRepository().save(expectedItem);
		this.semanticUnitRepository().delete(expectedNiiu);
		expectedItem.removeAssociatedSemanticUnit(new URI("1"));
		
		SemanticUnit actualNiiu = this.semanticUnitRepository().findByUpri(new URI("1"), niiuModel);
		SemanticUnit actualItem = this.semanticUnitRepository().findByUpri(new URI("3"), itemModel);
		TestSemanticUnit.assertSemanticUnitEquals(expectedNiiu, actualNiiu);		
		TestSemanticUnit.assertSemanticUnitEquals(expectedItem, actualItem);
	}
	
	@Test
	default void testIsSubjectUsedAsObject()
	{
		assertEquals(false, this.semanticUnitRepository().isSubjectUsedAsObject(new URI("0")));
		
		SemanticUnit niiu = new SemanticUnit(this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		niiu.setUri(new URI("1"));
		niiu.setSemanticUnitsGraph(new URI("2"));
		niiu.setSubject(new URI("0"));
		niiu.setLabel("Named Individual Identification Unit");
		niiu.setDescription("Named Individual Identification Unit Description");
		niiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		niiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		niiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		niiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		niiu.setCurrentVersion(true);
		niiu.setDeletedBy(null);
		niiu.setDeletionDate(null);
		
		this.semanticUnitRepository().save(niiu);
		assertEquals(false, this.semanticUnitRepository().isSubjectUsedAsObject(new URI("0")));
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("0"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		niiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		niiu.addLiteralObject(niiuLiteralObject);
		
		this.semanticUnitRepository().save(niiu);
		assertEquals(true, this.semanticUnitRepository().isSubjectUsedAsObject(new URI("0")));
	}
	
	@Test
	default void testFindAllConstraints()
	{
		SemanticUnit expectedReferencingItem = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedReferencingItem.setUri(new URI("5"));
		expectedReferencingItem.setSemanticUnitsGraph(new URI("6"));
		expectedReferencingItem.setSubject(new URI("0"));
		expectedReferencingItem.setLabel("Material Entity Item Unit");
		expectedReferencingItem.setDescription("Material Entity Item Unit Description");
		expectedReferencingItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedReferencingItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedReferencingItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedReferencingItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedReferencingItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedReferencingItem.setCurrentVersion(true);
		expectedReferencingItem.setDeletedBy(null);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu.setUri(new URI("7"));
		expectedHpsu.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu.setSubject(new URI("0"));
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(new URI("10"));
		expectedHpsu.setCurrentVersion(true);
		expectedHpsu.setDeletedBy(null);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject.setInputTypeLabel("has-part-resource");
		hpsuResourceObject.setObjectPositionClass(new URI("HasPartStatementUnitResourceObjectPosition"));
		hpsuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject.setCurrentVersion(true);
		hpsuResourceObject.setResourceURI(new URI("9"));
		hpsuResourceObject.setResourceLabel("eye");
		hpsuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedHpsu.addResourceObject(hpsuResourceObject);
		
		expectedReferencingItem.addAssociatedSemanticUnit(expectedHpsu.getUri());
//		expectedReferencingItem.addAssociatedSemanticUnit(new URI("2")); //NUUI of subject unit
		expectedReferencingItem.addAssociatedSemanticUnit(new URI("10"));
		
		expectedReferencingItem.addLinkedSemanticUnit(new URI("14"), false);
		
		SemanticUnit expectedItem = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("14"));
		expectedItem.setSemanticUnitsGraph(new URI("15"));
		expectedItem.setSubject(new URI("9"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(TestSemanticUnit.USER_ID);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("10"));
		expectedNiiu.setSemanticUnitsGraph(new URI("11"));
		expectedNiiu.setSubject(new URI("9"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
//		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri(), false);
		
		this.semanticUnitRepository().save(expectedNiiu);
		this.semanticUnitRepository().save(expectedItem);
		this.semanticUnitRepository().save(expectedHpsu);
		this.semanticUnitRepository().save(expectedReferencingItem);
		
		TestSemanticUnit.assertSetEquals(Set.of(new URI("http://purl.obolibrary.org/obo/BFO_0000040")), this.semanticUnitRepository().findAllConstraints(new URI("0"), this.storageModelManager()), (a, b) -> a.compareTo(b), (a, b) -> a.equals(b));
		TestSemanticUnit.assertSetEquals(Set.of(new URI("http://purl.obolibrary.org/obo/BFO_0000040")), this.semanticUnitRepository().findAllConstraints(new URI("9"), this.storageModelManager()), (a, b) -> a.compareTo(b), (a, b) -> a.equals(b));
	}
	
	@Test
	default void testFindAllConstraints2()
	{
		SemanticUnit expectedReferencingItem = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedReferencingItem.setUri(new URI("5"));
		expectedReferencingItem.setSemanticUnitsGraph(new URI("6"));
		expectedReferencingItem.setSubject(new URI("0"));
		expectedReferencingItem.setLabel("Material Entity Item Unit");
		expectedReferencingItem.setDescription("Material Entity Item Unit Description");
		expectedReferencingItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedReferencingItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedReferencingItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedReferencingItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedReferencingItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedReferencingItem.setCurrentVersion(true);
		expectedReferencingItem.setDeletedBy(null);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedHpsu = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("HasPartStatementKGBB")));
		expectedHpsu.setUri(new URI("7"));
		expectedHpsu.setSemanticUnitsGraph(new URI("8"));
		expectedHpsu.setSubject(new URI("0"));
		expectedHpsu.setLabel("Has Part Statement Unit");
		expectedHpsu.setDescription("Has Part Statement Unit Description");
		expectedHpsu.setKgbb(new URI("HasPartStatementKGBB"));
		expectedHpsu.setKgbbi(new URI("HasPartStatementKGBBInstance"));
		expectedHpsu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedHpsu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedHpsu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedHpsu.addObjectDescribedBySemanticUnit(new URI("10"));
		expectedHpsu.setCurrentVersion(true);
		expectedHpsu.setDeletedBy(null);
		expectedHpsu.setDeletionDate(null);
		
		ResourceObjectNode hpsuResourceObject = new ResourceObjectNode(new URI("16"));
		hpsuResourceObject.setInputTypeLabel("has-part-resource");
		hpsuResourceObject.setObjectPositionClass(new URI("HasPartStatementUnitResourceObjectPosition"));
		hpsuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		hpsuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		hpsuResourceObject.setCurrentVersion(true);
		hpsuResourceObject.setResourceURI(new URI("9"));
		hpsuResourceObject.setResourceLabel("eye");
		hpsuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000041"));
		expectedHpsu.addResourceObject(hpsuResourceObject);
		
		expectedReferencingItem.addAssociatedSemanticUnit(expectedHpsu.getUri());
//		expectedReferencingItem.addAssociatedSemanticUnit(new URI("2")); //NUUI of subject unit
		expectedReferencingItem.addAssociatedSemanticUnit(new URI("10"));
		
		expectedReferencingItem.addLinkedSemanticUnit(new URI("14"), false);
		
		SemanticUnit expectedItem = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("14"));
		expectedItem.setSemanticUnitsGraph(new URI("15"));
		expectedItem.setSubject(new URI("9"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(null);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("10"));
		expectedNiiu.setSemanticUnitsGraph(new URI("11"));
		expectedNiiu.setSubject(new URI("9"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
//		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("12"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("13"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri(), false);
		
		this.semanticUnitRepository().save(expectedNiiu);
		this.semanticUnitRepository().save(expectedItem);
		this.semanticUnitRepository().save(expectedHpsu);
		this.semanticUnitRepository().save(expectedReferencingItem);
		
		TestSemanticUnit.assertSetEquals(Set.of(new URI("http://purl.obolibrary.org/obo/BFO_0000040")), this.semanticUnitRepository().findAllConstraints(new URI("0"), this.storageModelManager()), (a, b) -> a.compareTo(b), (a, b) -> a.equals(b));
		TestSemanticUnit.assertSetEquals(Set.of(new URI("http://purl.obolibrary.org/obo/BFO_0000040"), new URI("http://purl.obolibrary.org/obo/BFO_0000041")), this.semanticUnitRepository().findAllConstraints(new URI("9"), this.storageModelManager()), (a, b) -> a.compareTo(b), (a, b) -> a.equals(b));
	}
	
	@Test
	default void testFindAll()
	{
		TestSemanticUnit.assertSetEquals(Collections.emptySet(), this.semanticUnitRepository().findAll(new URI("MaterialEntityItemKGBB")), URI::compareTo, Assertions::assertEquals);
		
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit expected1 = new SemanticUnit(model);
		expected1.setUri(new URI("1"));
		expected1.setSemanticUnitsGraph(new URI("2"));
		expected1.setSubject(new URI("0"));
		expected1.setLabel("Material Entity Item Unit");
		expected1.setDescription("Material Entity Item Unit Description");
		expected1.setKgbb(new URI("MaterialEntityItemKGBB"));
		expected1.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expected1.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		expected1.setCreatedWithApplication(Constants.APPLICATION_URI);
		expected1.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		expected1.setCurrentVersion(true);
		expected1.setDeletedBy(null);
		expected1.setDeletionDate(null);
		
		this.semanticUnitRepository().save(expected1);
		
		TestSemanticUnit.assertSetEquals(Set.of(expected1.getUri()), this.semanticUnitRepository().findAll(new URI("MaterialEntityItemKGBB")), URI::compareTo, Assertions::assertEquals);
		TestSemanticUnit.assertSetEquals(Collections.emptySet(), this.semanticUnitRepository().findAll(new URI("Missing")), URI::compareTo, Assertions::assertEquals);
		
		SemanticUnit expected2 = new SemanticUnit(model);
		expected2.setUri(new URI("3"));
		expected2.setSemanticUnitsGraph(new URI("4"));
		expected2.setSubject(new URI("0"));
		expected2.setLabel("Material Entity Item Unit");
		expected2.setDescription("Material Entity Item Unit Description");
		expected2.setKgbb(new URI("MaterialEntityItemKGBB"));
		expected2.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expected2.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		expected2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expected2.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		expected2.setCurrentVersion(true);
		expected2.setDeletedBy(null);
		expected2.setDeletionDate(null);
		
		this.semanticUnitRepository().save(expected2);
		
		TestSemanticUnit.assertSetEquals(Set.of(expected1.getUri(), expected2.getUri()), this.semanticUnitRepository().findAll(new URI("MaterialEntityItemKGBB")), URI::compareTo, Assertions::assertEquals);
		TestSemanticUnit.assertSetEquals(Collections.emptySet(), this.semanticUnitRepository().findAll(new URI("Missing")), URI::compareTo, Assertions::assertEquals);
	}
	
	@Test
	default void testIsSubjectUsedByOtherUnit()
	{
		assertEquals(false, this.semanticUnitRepository().isSubjectUsedByOtherUnit(new URI("0"), new URI("7")));
		
		SemanticUnit niiu = new SemanticUnit(this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		niiu.setUri(new URI("1"));
		niiu.setSemanticUnitsGraph(new URI("2"));
		niiu.setSubject(new URI("0"));
		niiu.setLabel("Named Individual Identification Unit");
		niiu.setDescription("Named Individual Identification Unit Description");
		niiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		niiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		niiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		niiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		niiu.setCurrentVersion(true);
		niiu.setDeletedBy(null);
		niiu.setDeletionDate(null);
		
		this.semanticUnitRepository().save(niiu);
		assertEquals(false, this.semanticUnitRepository().isSubjectUsedByOtherUnit(new URI("0"), new URI("7")));
		
		SemanticUnitStorageModel model = this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB"));
		SemanticUnit item3 = new SemanticUnit(model);
		item3.setUri(new URI("7"));
		item3.setSemanticUnitsGraph(new URI("8"));
		item3.setSubject(new URI("0"));
		item3.setLabel("Material Entity Item Unit");
		item3.setDescription("Material Entity Item Unit Description");
		item3.setKgbb(new URI("MaterialEntityItemKGBB"));
		item3.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		item3.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		item3.setCreatedWithApplication(Constants.APPLICATION_URI);
		item3.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		item3.setCurrentVersion(true);
		item3.setDeletedBy(null);
		item3.setDeletionDate(null);
		
		this.semanticUnitRepository().save(niiu);
		assertEquals(false, this.semanticUnitRepository().isSubjectUsedByOtherUnit(new URI("0"), new URI("7")));
		
		SemanticUnit item1 = new SemanticUnit(model);
		item1.setUri(new URI("3"));
		item1.setSemanticUnitsGraph(new URI("4"));
		item1.setSubject(new URI("0"));
		item1.setLabel("Material Entity Item Unit");
		item1.setDescription("Material Entity Item Unit Description");
		item1.setKgbb(new URI("MaterialEntityItemKGBB"));
		item1.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		item1.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		item1.setCreatedWithApplication(Constants.APPLICATION_URI);
		item1.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		item1.setCurrentVersion(false);
		item1.setDeletedBy(null);
		item1.setDeletionDate(null);
		
		this.semanticUnitRepository().save(item1);
		assertEquals(false, this.semanticUnitRepository().isSubjectUsedByOtherUnit(new URI("0"), new URI("7")));
		
		SemanticUnit item2 = new SemanticUnit(model);
		item2.setUri(new URI("5"));
		item2.setSemanticUnitsGraph(new URI("6"));
		item2.setSubject(new URI("0"));
		item2.setLabel("Material Entity Item Unit");
		item2.setDescription("Material Entity Item Unit Description");
		item2.setKgbb(new URI("MaterialEntityItemKGBB"));
		item2.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		item2.setCreator(TestSemanticUnit.USER_ID);
//		expected.setCreationDate(LocalDateTime.now());
		item2.setCreatedWithApplication(Constants.APPLICATION_URI);
		item2.setLicense(null);
//		expected.addAccessRestrictedTo(null);
		item2.setCurrentVersion(true);
		item2.setDeletedBy(null);
		item2.setDeletionDate(null);
		
		this.semanticUnitRepository().save(item2);
		assertEquals(true, this.semanticUnitRepository().isSubjectUsedByOtherUnit(new URI("0"), new URI("7")));
	}
	
	@Test
	default void testFindAssociatingUnits()
	{
		TestSemanticUnit.assertSetEquals(Collections.emptySet(), this.semanticUnitRepository().findAssociatingUnits(new URI("1")), URI::compareTo, Assertions::assertEquals);
		
		SemanticUnit expectedItem = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem.setUri(new URI("5"));
		expectedItem.setSemanticUnitsGraph(new URI("6"));
		expectedItem.setSubject(new URI("0"));
		expectedItem.setLabel("Material Entity Item Unit");
		expectedItem.setDescription("Material Entity Item Unit Description");
		expectedItem.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem.setCurrentVersion(true);
		expectedItem.setDeletedBy(TestSemanticUnit.USER_ID);
//		expectedItem.setDeletionDate(null);
		
		SemanticUnit expectedNiiu = new SemanticUnit(this.storageModelManager().getStorageModel(Constants.NIIU_KGBB_URI));
		expectedNiiu.setUri(new URI("1"));
		expectedNiiu.setSemanticUnitsGraph(new URI("2"));
		expectedNiiu.setSubject(new URI("0"));
		expectedNiiu.setLabel("Named Individual Identification Unit");
		expectedNiiu.setDescription("Named Individual Identification Unit Description");
		expectedNiiu.setKgbb(new URI("NamedIndividualIdentificationKGBB"));
		expectedNiiu.setKgbbi(new URI("NamedIndividualIdentificationKGBBInstance"));
		expectedNiiu.setCreator(TestSemanticUnit.USER_ID);
//		expectedNiiu.setCreationDate(LocalDateTime.now());
		expectedNiiu.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedNiiu.setLicense(null);
//		expectedNiiu.addAccessRestrictedTo(null);
		expectedNiiu.setCurrentVersion(true);
		expectedNiiu.setDeletedBy(null);
//		expectedNiiu.setDeletionDate(null);
		
		ResourceObjectNode niiuResourceObject = new ResourceObjectNode(new URI("3"));
		niiuResourceObject.setInputTypeLabel("Class Affilitation");
		niiuResourceObject.setObjectPositionClass(Constants.NIIU_RESOURCE_POSITION_URI);
		niiuResourceObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuResourceObject.setCreationDate(LocalDateTime.now());
		niiuResourceObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuResourceObject.setCurrentVersion(true);
		niiuResourceObject.setResourceURI(new URI("http://purl.obolibrary.org/obo/UBERON_0000970"));
		niiuResourceObject.setResourceLabel("eye");
		niiuResourceObject.setConstraint(new URI("http://purl.obolibrary.org/obo/BFO_0000040"));
		expectedNiiu.addResourceObject(niiuResourceObject);
		
		LiteralObjectNode niiuLiteralObject = new LiteralObjectNode(new URI("4"));
		niiuLiteralObject.setInputTypeLabel("niiu-label");
		niiuLiteralObject.setObjectPositionClass(Constants.NIIU_LITERAL_POSITION_URI);
		niiuLiteralObject.setCreator(TestSemanticUnit.USER_ID);
//		niiuLiteralObject.setCreationDate(LocalDateTime.now());
		niiuLiteralObject.setCreatedWithApplication(Constants.APPLICATION_URI);
		niiuLiteralObject.setCurrentVersion(true);
		niiuLiteralObject.setLiteral("eye");
		expectedNiiu.addLiteralObject(niiuLiteralObject);
		
		expectedItem.addAssociatedSemanticUnit(expectedNiiu.getUri());

		SemanticUnit expectedItem2 = new SemanticUnit(this.storageModelManager().getStorageModel(new URI("MaterialEntityItemKGBB")));
		expectedItem2.setUri(new URI("7"));
		expectedItem2.setSemanticUnitsGraph(new URI("8"));
		expectedItem2.setSubject(new URI("0"));
		expectedItem2.setLabel("Material Entity Item Unit");
		expectedItem2.setDescription("Material Entity Item Unit Description");
		expectedItem2.setKgbb(new URI("MaterialEntityItemKGBB"));
		expectedItem2.setKgbbi(new URI("MaterialEntityItemKGBBInstance"));
		expectedItem2.setCreator(TestSemanticUnit.USER_ID);
//		expectedItem.setCreationDate(LocalDateTime.now());
		expectedItem2.setCreatedWithApplication(Constants.APPLICATION_URI);
		expectedItem2.setLicense(null);
//		expectedItem.addAccessRestrictedTo(null);
		expectedItem2.setCurrentVersion(true);
		expectedItem2.setDeletedBy(TestSemanticUnit.USER_ID);
//		expectedItem.setDeletionDate(null);
		
		expectedItem2.addAssociatedSemanticUnit(expectedNiiu.getUri());
		
		this.semanticUnitRepository().save(expectedNiiu);
		TestSemanticUnit.assertSetEquals(Collections.emptySet(), this.semanticUnitRepository().findAssociatingUnits(new URI("1")), URI::compareTo, Assertions::assertEquals);
		
		this.semanticUnitRepository().save(expectedItem);
		TestSemanticUnit.assertSetEquals(Set.of(new URI("5")), this.semanticUnitRepository().findAssociatingUnits(new URI("1")), URI::compareTo, Assertions::assertEquals);
		
		this.semanticUnitRepository().save(expectedItem2);
		TestSemanticUnit.assertSetEquals(Set.of(new URI("5"), new URI("7")), this.semanticUnitRepository().findAssociatingUnits(new URI("1")), URI::compareTo, Assertions::assertEquals);
		
		expectedItem2.setDeletedBy(TestSemanticUnit.USER_ID);
		this.semanticUnitRepository().delete(expectedItem2);
		TestSemanticUnit.assertSetEquals(Set.of(new URI("5")), this.semanticUnitRepository().findAssociatingUnits(new URI("1")), URI::compareTo, Assertions::assertEquals);
	}
}
