package eu.tib.kgbbengine.spi;

import java.util.Collections;
import java.util.Map;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Either;

public interface ValueProvider<T>
{
	Either<Object, Map<URI, Boolean>> provide(String name, Slot<?> slot, T object);
	
	static Either<Object, Map<URI, Boolean>> literal(@Nullable URI literal)
	{
		return literal(literal != null ? literal.toString() : null);
	}
	
	static Either<Object, Map<URI, Boolean>> literal(Object literal)
	{
		return Either.left(literal);
	}
	
	static Either<Object, Map<URI, Boolean>> resource(URI uri)
	{
		return uri != null ? resource(uri, true) : resource(Collections.emptyMap());
	}
	
	static Either<Object, Map<URI, Boolean>> resource(URI uri, boolean currentVersion)
	{
		return Either.right(Map.of(uri, currentVersion));
	}
	
	static Either<Object, Map<URI, Boolean>> resource(Map<URI, Boolean> links)
	{
		return Either.right(links);
	}
}
