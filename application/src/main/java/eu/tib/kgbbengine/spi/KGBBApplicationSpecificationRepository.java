package eu.tib.kgbbengine.spi;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.KGBBInstance;

public interface KGBBApplicationSpecificationRepository
{
	KGBBInstance findById(URI uri);
}
