package eu.tib.kgbbengine.spi;

import eu.tib.kgbbengine.domain.URI;

public interface UserRepository
{
	public Object findById(URI id);
}
