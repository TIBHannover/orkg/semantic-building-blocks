package eu.tib.kgbbengine.spi;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.KGBB;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnitType;

public interface KGBBRepository
{
	KGBB findById(URI uri);
	
	SemanticUnitType getType(URI kgbb);
}
