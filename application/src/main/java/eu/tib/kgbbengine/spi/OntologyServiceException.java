package eu.tib.kgbbengine.spi;

public class OntologyServiceException extends Exception
{
	private static final long serialVersionUID = -3059712536006622997L;
	
	public OntologyServiceException(String message)
	{
		super(message);
	}
}
