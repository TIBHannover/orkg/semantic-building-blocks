package eu.tib.kgbbengine.spi;

import java.util.NoSuchElementException;
import java.util.Set;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.SemanticUnitStorageModel;
import eu.tib.kgbbengine.domain.storage.StorageModelManager;

public interface SemanticUnitRepository
{
	void save(SemanticUnit unit);
	
	SemanticUnit findByUpri(URI upri, SemanticUnitStorageModel model);
	
	/**
	 * Finds a semantic unit based on the subject uri. If no matching semantic unit is found then a {@link NoSuchElementException} will be thrown.
	 * If more that one semantic unit is found matching the storage model then an {@link IllegalStateException} is thrown.
	 */
	SemanticUnit findBySubject(URI subject, URI kgbb, SemanticUnitStorageModel model);
	
	URI findKggbi(URI upri);
	
	boolean isSubjectUsedAsObject(URI subject);
	
	boolean isSubjectUsedByOtherUnit(URI subject, URI uri);
	
	Set<URI> findAllConstraints(URI uri, StorageModelManager storageModelManager);
	
	/**
	 * Checks if the uri it not yet used by this repository
	 */
	boolean isUnique(URI uri);
	
	Set<URI> findAssociatingUnits(URI uri);
	
	/**
	 * Updates the properties of the semantic unit and sets currentVersion to false and sets references from all other semantic units to this semantic unit to currentVersion false
	 */
	void delete(SemanticUnit unit);
	
	Set<URI> findAll(URI kgbb);
}
