package eu.tib.kgbbengine.spi;

import java.util.Collection;
import java.util.Collections;

import eu.tib.kgbbengine.domain.URI;

public interface OntologyLookupService
{
	default boolean hasAncestor(URI uri, URI ancestor) throws OntologyServiceException
	{
		return this.hasAncestors(uri, Collections.singleton(ancestor));
	}
	
	boolean hasAncestors(URI uri, Collection<URI> ancestors) throws OntologyServiceException;
	
	String resourceLabel(URI uri) throws OntologyServiceException;
}
