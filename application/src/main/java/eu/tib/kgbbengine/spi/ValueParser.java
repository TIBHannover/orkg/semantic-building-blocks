package eu.tib.kgbbengine.spi;

import eu.tib.kgbbengine.domain.storage.slot.Slot;

public interface ValueParser<T, S>
{
	void parse(String name, S value, boolean currentVersion, T object, Slot<?> slot);
}
