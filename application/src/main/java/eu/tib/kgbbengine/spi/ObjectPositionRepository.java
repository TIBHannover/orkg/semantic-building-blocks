package eu.tib.kgbbengine.spi;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.ObjectPosition;

public interface ObjectPositionRepository
{
	ObjectPosition findById(URI uri);
	
	boolean isResourceObjectPosition(ObjectPosition position);
}
