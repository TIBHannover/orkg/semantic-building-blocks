package eu.tib.kgbbengine.spi;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.storage.PartialStorageModel;

public interface StorageModelRepository
{
	PartialStorageModel findById(URI uri);
}
