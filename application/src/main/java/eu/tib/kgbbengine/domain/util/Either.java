package eu.tib.kgbbengine.domain.util;

import java.util.Optional;

public interface Either<L, R>
{
	Optional<L> left();
	Optional<R> right();
	
    boolean isLeft();
    boolean isRight();
    
	public static <L, R> Either<L, R> left(L left)
	{
		return new Left<L, R>(left);
	}
	
	public static <L, R> Either<L, R> right(R right)
	{
		return new Right<L, R>(right);
	}
	
	public static final class Left<L, R> implements Either<L, R>
	{
		private final L left;
		
		public Left(L left)
		{
			this.left = left;
		}
		
		@Override
		public Optional<L> left()
		{
			return Optional.ofNullable(this.left);
		}
		
		@Override
		public Optional<R> right()
		{
			return Optional.empty();
		}
		
		@Override
		public boolean isLeft()
		{
			return true;
		}
		
		@Override
		public boolean isRight()
		{
			return false;
		}
	}
	
	public static final class Right<L, R> implements Either<L, R>
	{
		private final R right;
		
		public Right(R right)
		{
			this.right = right;
		}
		
		@Override
		public Optional<L> left()
		{
			return Optional.empty();
		}
		
		@Override
		public Optional<R> right()
		{
			return Optional.ofNullable(this.right);
		}
		
		@Override
		public boolean isLeft()
		{
			return false;
		}
		
		@Override
		public boolean isRight()
		{
			return true;
		}
	}
}