package eu.tib.kgbbengine.domain.semanticunit;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.storage.SemanticUnitStorageModel;

public class SemanticUnit
{
	private URI semanticUnitsGraph;
	private URI uri;
	private final SemanticUnitStorageModel model;
	private URI subject;
	private String label;
	private String description;
	private URI kgbb;
	private URI kgbbi;
	private Map<URI, Boolean> associatedSemanticUnits;
	private Map<URI, Boolean> linkedSemanticUnits;
	private Set<ResourceObjectNode> resourceObjects;
	private Set<LiteralObjectNode> literalObjects;
	private URI license;
	private Set<URI> accessRestrictedTo;
	private URI creator;
	private LocalDateTime creationDate;
	private URI createdWithApplication;
	private Map<URI, Boolean> objectDescribedBySemanticUnits;
	private boolean currentVersion = true;
	@Nullable
	private URI deletedBy;
	@Nullable
	private LocalDateTime deletionDate;
	
	//optional:
	//versionID (upri)
	//datasetUnitID (upri)
	
	public SemanticUnit(SemanticUnitStorageModel model)
	{
		this.model = model;
	}
	
	public SemanticUnit(URI uri, URI semanticUnitsGraph, SemanticUnitStorageModel model)
	{
		this.semanticUnitsGraph = semanticUnitsGraph;
		this.uri = uri;
		this.model = model;
	}
	
	@Nullable
	public LiteralObjectNode findLiteralObject(URI uri)
	{
		for(LiteralObjectNode node : this.getLiteralObjects())
		{
			if(node.isCurrentVersion() && node.getObjectPositionClass().equals(uri))
			{
				return node;
			}
		}
		
		return null;
	}
	
	@Nullable
	public ResourceObjectNode findResourceObject(URI uri)
	{
		for(ResourceObjectNode node : this.getResourceObjects())
		{
			if(node.isCurrentVersion() && node.getObjectPositionClass().equals(uri))
			{
				return node;
			}
		}
		
		return null;
	}
	
	public URI getSemanticUnitsGraph()
	{
		return this.semanticUnitsGraph;
	}
	
	public void setSemanticUnitsGraph(URI semanticUnitsGraph)
	{
		this.semanticUnitsGraph = semanticUnitsGraph;
	}
	
	public URI getUri()
	{
		return this.uri;
	}
	
	public void setUri(URI uri)
	{
		this.uri = uri;
	}
	
	@JsonIgnore
	public SemanticUnitStorageModel getStorageModel()
	{
		return this.model;
	}
	
	public URI getSubject()
	{
		return this.subject;
	}
	
	public void setSubject(URI subject)
	{
		this.subject = subject;
	}
	
	public String getLabel()
	{
		return this.label;
	}
	
	public void setLabel(String label)
	{
		this.label = label;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public URI getKgbb()
	{
		return this.kgbb;
	}
	
	public void setKgbb(URI kgbb)
	{
		this.kgbb = kgbb;
	}
	
	public URI getKgbbi()
	{
		return this.kgbbi;
	}
	
	public void setKgbbi(URI kgbbi)
	{
		this.kgbbi = kgbbi;
	}
	
	public Set<URI> getCurrentAssociatedSemanticUnits()
	{
		if(this.associatedSemanticUnits == null)
		{
			return Collections.emptySet();
		}
		
		return this.associatedSemanticUnits.entrySet().stream().filter(Entry::getValue).map(Entry::getKey).collect(Collectors.toSet());
	}
	
	public Map<URI, Boolean> getAssociatedSemanticUnits()
	{
		if(this.associatedSemanticUnits == null)
		{
			this.associatedSemanticUnits = new HashMap<URI, Boolean>();
		}
		
		return this.associatedSemanticUnits;
	}
	
	public void addAssociatedSemanticUnit(URI associatedSemanticUnit)
	{
		this.addAssociatedSemanticUnit(associatedSemanticUnit, true);
	}
	
	public void addAssociatedSemanticUnit(URI associatedSemanticUnit, boolean currentVersion)
	{
		this.getAssociatedSemanticUnits().put(associatedSemanticUnit, currentVersion);
	}
	
	public void removeAssociatedSemanticUnit(URI associatedSemanticUnit)
	{
		if(this.getAssociatedSemanticUnits().containsKey(associatedSemanticUnit))
		{
			this.associatedSemanticUnits.put(associatedSemanticUnit, false);
		}
	}
	
	public Set<URI> getCurrentLinkedSemanticUnits()
	{
		if(this.linkedSemanticUnits == null)
		{
			return Collections.emptySet();
		}
		
		return this.linkedSemanticUnits.entrySet().stream().filter(Entry::getValue).map(Entry::getKey).collect(Collectors.toSet());
	}
	
	public Map<URI, Boolean> getLinkedSemanticUnits()
	{
		if(this.linkedSemanticUnits == null)
		{
			this.linkedSemanticUnits = new HashMap<URI, Boolean>();
		}
		
		return this.linkedSemanticUnits;
	}
	
	public void addLinkedSemanticUnit(URI linkedSemanticUnit)
	{
		this.addLinkedSemanticUnit(linkedSemanticUnit, true);
	}
	
	public void addLinkedSemanticUnit(URI linkedSemanticUnit, boolean currentVersion)
	{
		this.getLinkedSemanticUnits().put(linkedSemanticUnit, currentVersion);
	}
	
	public void removeLinkedSemanticUnit(URI linkedSemanticUnit)
	{
		if(this.getLinkedSemanticUnits().containsKey(linkedSemanticUnit))
		{
			this.linkedSemanticUnits.put(linkedSemanticUnit, false);
		}
	}
	
	public List<URI> getContributors()
	{
		return Stream.concat(this.getResourceObjects().stream(), this.getLiteralObjects().stream()).map(ObjectNode::getCreator).distinct().toList();
	}
	
	@JsonIgnore
	public LocalDateTime getLastUpdate()
	{
		return Stream.concat(Stream.concat(this.getResourceObjects().stream(), this.getLiteralObjects().stream()).map(ObjectNode::getCreationDate), Stream.ofNullable(this.creationDate)).sorted(Collections.reverseOrder()).findFirst().get();
	}
	
	public Set<ResourceObjectNode> getResourceObjects()
	{
		if(this.resourceObjects == null)
		{
			this.resourceObjects = new HashSet<ResourceObjectNode>();
		}
		
		return this.resourceObjects;
	}
	
	public void addResourceObject(ResourceObjectNode resourceObject)
	{
		this.getResourceObjects().add(resourceObject);
	}
	
	public Set<LiteralObjectNode> getLiteralObjects()
	{
		if(this.literalObjects == null)
		{
			this.literalObjects = new HashSet<LiteralObjectNode>();
		}
		
		return this.literalObjects;
	}
	
	public void addLiteralObject(LiteralObjectNode literalObject)
	{
		this.getLiteralObjects().add(literalObject);
	}
	
	public URI getLicense()
	{
		return this.license;
	}
	
	public void setLicense(URI license)
	{
		this.license = license;
	}
	
	public Set<URI> getAccessRestrictedTo()
	{
		if(this.accessRestrictedTo == null)
		{
			this.accessRestrictedTo = new HashSet<URI>();
		}
		
		return this.accessRestrictedTo;
	}
	
	public void addAccessRestrictedTo(URI accessRestrictedTo)
	{
		this.getAccessRestrictedTo().add(accessRestrictedTo);
	}
	
	public URI getCreator()
	{
		return this.creator;
	}
	
	public void setCreator(URI creator)
	{
		this.creator = creator;
	}
	
	public LocalDateTime getCreationDate()
	{
		return this.creationDate;
	}
	
	public void setCreationDate(LocalDateTime creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public URI getCreatedWithApplication()
	{
		return this.createdWithApplication;
	}
	
	public void setCreatedWithApplication(URI createdWithApplication)
	{
		this.createdWithApplication = createdWithApplication;
	}
	
	public boolean isCurrentVersion()
	{
		return this.currentVersion;
	}
	
	public void setCurrentVersion(boolean currentVersion)
	{
		this.currentVersion = currentVersion;
	}
	
	public Map<URI, Boolean> getObjectDescribedBySemanticUnits()
	{
		if(this.objectDescribedBySemanticUnits == null)
		{
			this.objectDescribedBySemanticUnits = new HashMap<URI, Boolean>();
		}
		
		return this.objectDescribedBySemanticUnits;
	}
	
	public void addObjectDescribedBySemanticUnit(URI objectDescribedBySemanticUnit)
	{
		this.getObjectDescribedBySemanticUnits().put(objectDescribedBySemanticUnit, true);
	}
	
	public void addObjectDescribedBySemanticUnit(URI objectDescribedBySemanticUnit, boolean currentVersion)
	{
		this.getObjectDescribedBySemanticUnits().put(objectDescribedBySemanticUnit, currentVersion);
	}
	
	public Set<URI> getCurrentObjectDescribedBySemanticUnits()
	{
		if(this.objectDescribedBySemanticUnits == null)
		{
			return Collections.emptySet();
		}
		
		return this.objectDescribedBySemanticUnits.entrySet().stream().filter(Entry::getValue).map(Entry::getKey).collect(Collectors.toSet());
	}
	
	public void removeObjectDescribedBySemanticUnit(URI objectDescribedBySemanticUnit)
	{
		if(this.getObjectDescribedBySemanticUnits().containsKey(objectDescribedBySemanticUnit))
		{
			this.objectDescribedBySemanticUnits.put(objectDescribedBySemanticUnit, false);
		}
	}
	
	@Nullable
	public URI getDeletedBy()
	{
		return this.deletedBy;
	}
	
	public void setDeletedBy(URI deletedBy)
	{
		this.deletedBy = deletedBy;
	}
	
	@Nullable
	public LocalDateTime getDeletionDate()
	{
		return this.deletionDate;
	}
	
	public void setDeletionDate(LocalDateTime deletionDate)
	{
		this.deletionDate = deletionDate;
	}
	
	public SemanticUnit shallowCopy()
	{
		SemanticUnit copy = new SemanticUnit(this.model);
		copy.setSemanticUnitsGraph(this.semanticUnitsGraph);
		copy.setUri(this.uri);
		copy.setSubject(this.subject);
		copy.setLabel(this.label);
		copy.setDescription(this.description);
		copy.setKgbb(this.kgbb);
		copy.setKgbbi(this.kgbbi);
		
		if(this.associatedSemanticUnits != null && !this.associatedSemanticUnits.isEmpty())
		{
			copy.getAssociatedSemanticUnits().putAll(this.associatedSemanticUnits);
		}
		
		if(this.linkedSemanticUnits != null && !this.linkedSemanticUnits.isEmpty())
		{
			copy.getLinkedSemanticUnits().putAll(this.linkedSemanticUnits);
		}
		
		if(this.resourceObjects != null && !this.resourceObjects.isEmpty())
		{
			Set<ResourceObjectNode> nodes = copy.getResourceObjects();
			
			for(ResourceObjectNode node : this.resourceObjects)
			{
				nodes.add(node.shallowCopy());
			}
		}
		
		if(this.literalObjects != null && !this.literalObjects.isEmpty())
		{
			Set<LiteralObjectNode> nodes = copy.getLiteralObjects();
			
			for(LiteralObjectNode node : this.literalObjects)
			{
				nodes.add(node.shallowCopy());
			}
		}
		
		copy.setLicense(this.license);
		
		if(this.accessRestrictedTo != null && !this.accessRestrictedTo.isEmpty())
		{
			copy.getAccessRestrictedTo().addAll(this.accessRestrictedTo);
		}
		
		copy.setCreator(this.creator);
		copy.setCreationDate(this.creationDate);
		copy.setCreatedWithApplication(this.createdWithApplication);
		
		if(this.objectDescribedBySemanticUnits != null && !this.objectDescribedBySemanticUnits.isEmpty())
		{
			copy.getObjectDescribedBySemanticUnits().putAll(this.objectDescribedBySemanticUnits);
		}
		
		copy.setCurrentVersion(this.currentVersion);
		copy.setDeletedBy(this.deletedBy);
		copy.setDeletionDate(this.deletionDate);
		
		return copy;
	}
}