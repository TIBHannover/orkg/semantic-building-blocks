package eu.tib.kgbbengine.domain.kgbb;

import java.util.Objects;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;

public class ObjectPosition
{
	@Nullable
	private final URI parent;
	private final URI uri;
	
	public ObjectPosition(URI uri)
	{
		this((URI) null, uri);
	}
	
	public ObjectPosition(@Nullable ObjectPosition parent, URI uri)
	{
		this(parent != null ? parent.getUri() : null, uri);
	}
	
	public ObjectPosition(@Nullable URI parent, URI uri)
	{
		this.parent = parent;
		this.uri = uri;
	}
	
	@Nullable
	public URI getParent()
	{
		return this.parent;
	}
	
	public URI getUri()
	{
		return this.uri;
	}
	
	@Override
	public int hashCode()
	{
		return this.uri.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj != null && obj instanceof ObjectPosition other)
		{
			return Objects.equals(this.uri, other.uri);
		}
		
		return false;
	}
}
