package eu.tib.kgbbengine.domain.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import eu.tib.kgbbengine.domain.storage.slot.Slot;

public class Util
{
	public static final ObjectMapper YAML_MAPPER = new ObjectMapper(new YAMLFactory());
	
	static
	{
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Slot.class, new Slot.Deserializer());
		YAML_MAPPER.registerModule(module);
	}
}
