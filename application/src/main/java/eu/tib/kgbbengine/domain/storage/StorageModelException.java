package eu.tib.kgbbengine.domain.storage;

public class StorageModelException extends Exception
{
	private static final long serialVersionUID = -6133024736267755060L;
	
	public StorageModelException(String message)
	{
		super(message);
	}
}
