package eu.tib.kgbbengine.domain;

public interface UriSupplier
{
	URI newURI();
}
