package eu.tib.kgbbengine.domain.semanticunit;

public enum SemanticUnitType
{
	STATEMENT,
	COMPOUND;
	
	@Override
	public String toString()
	{
		return this.name().toLowerCase();
	}
}
