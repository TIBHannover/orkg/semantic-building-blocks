package eu.tib.kgbbengine.domain.storage.slot;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;

public abstract class LiteralSlot<T> extends Slot<T>
{
	@Nullable
	private final T ifabsent;
	
	public LiteralSlot(@Nullable URI slotUri, @Nullable String ifabsent, boolean required, boolean multivalued)
	{
		super(slotUri, required, multivalued);
		this.ifabsent = this.unwrapIfAbsent(ifabsent);
	}
	
	protected T unwrapIfAbsent(String ifabsent)
	{
		return null;
	}
	
	@Nullable
	public T getIfAbsent()
	{
		return this.ifabsent;
	}
	
	@Override
	public boolean isClassSlot()
	{
		return false;
	}
}
