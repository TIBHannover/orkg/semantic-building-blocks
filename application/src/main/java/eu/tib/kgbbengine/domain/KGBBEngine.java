package eu.tib.kgbbengine.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.api.CreateCompoundUnitUseCase;
import eu.tib.kgbbengine.api.CreateStatementUnitUseCase;
import eu.tib.kgbbengine.api.DeleteCompoundUnitUseCase;
import eu.tib.kgbbengine.api.DeleteStatementUnitUseCase;
import eu.tib.kgbbengine.api.FindSemanticUnitUseCase;
import eu.tib.kgbbengine.api.UpdateStatementUnitUseCase;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.LiteralInput;
import eu.tib.kgbbengine.domain.input.ResourceInput;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.kgbb.Association;
import eu.tib.kgbbengine.domain.kgbb.AssociationException;
import eu.tib.kgbbengine.domain.kgbb.Interconnect;
import eu.tib.kgbbengine.domain.kgbb.KGBBInstance;
import eu.tib.kgbbengine.domain.kgbb.Link;
import eu.tib.kgbbengine.domain.kgbb.ObjectPosition;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnitType;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.domain.storage.SemanticUnitStorageModel;
import eu.tib.kgbbengine.domain.storage.StorageModelException;
import eu.tib.kgbbengine.domain.storage.StorageModelManager;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.storage.slot.StringSlot;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.domain.util.Either;
import eu.tib.kgbbengine.spi.KGBBApplicationSpecificationRepository;
import eu.tib.kgbbengine.spi.KGBBRepository;
import eu.tib.kgbbengine.spi.ObjectPositionRepository;
import eu.tib.kgbbengine.spi.OntologyLookupService;
import eu.tib.kgbbengine.spi.OntologyServiceException;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;
import eu.tib.kgbbengine.spi.StorageModelRepository;
import eu.tib.kgbbengine.spi.UserRepository;

public class KGBBEngine implements FindSemanticUnitUseCase, CreateCompoundUnitUseCase, CreateStatementUnitUseCase, UpdateStatementUnitUseCase, DeleteStatementUnitUseCase, DeleteCompoundUnitUseCase
{
	private final KGBBRepository kgbbRepo;
	private final KGBBApplicationSpecificationRepository kgbbAppSpecRepo;
	private final ObjectPositionRepository positionRepo;
	private final SemanticUnitRepository unitRepo;
	private final UserRepository userRepo;
	private final OntologyLookupService ontoService;
	private final UriSupplier uriSupplier;
	private final StorageModelManager storageModelManager;
	
	public KGBBEngine(KGBBRepository kgbbRepo, KGBBApplicationSpecificationRepository kgbbAppSpecRepo, ObjectPositionRepository positionRepo, StorageModelRepository storageModelRepo, BiFunction<LinearStorageModel, LinearStorageModel, SemanticUnitRepository> unitRepoFactory, UserRepository userRepo, OntologyLookupService ontoService, UriSupplier uriSupplier)
	{
		this.kgbbRepo = kgbbRepo;
		this.kgbbAppSpecRepo = kgbbAppSpecRepo;
		this.positionRepo = positionRepo;
		this.userRepo = userRepo;
		this.ontoService = ontoService;
		this.uriSupplier = uriSupplier;
		this.storageModelManager = new StorageModelManager(storageModelRepo, this.kgbbRepo);
		this.unitRepo = unitRepoFactory.apply(this.storageModelManager.getStorageModel(Constants.KGBB).getSemanticUnitStorageModel(), this.storageModelManager.getObjectStorageModel(Constants.RESOURCE_OBJECT_POSITION));
	}
	
	@Override
	public SemanticUnit findSemanticUnit(URI uri)
	{
		URI kgbbiUri = this.unitRepo.findKggbi(uri);
		KGBBInstance kgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
		return this.unitRepo.findByUpri(uri, this.storageModelManager.getStorageModel(kgbbi.getKgbb()));
	}
	
	@Override
	public CompoundUnitResult createCompoundUnit(URI kgbbiUri, URI contributor, Input inputs, @Nullable URI subjectUnitUri) throws IllegalArgumentException, AssociationException, StorageModelException, OntologyServiceException
	{
		this.userRepo.findById(contributor);
		SemanticUnit subjectUnit = null;
		
		if(subjectUnitUri != null)
		{
			KGBBInstance subjectUnitKgbbi = this.kgbbAppSpecRepo.findById(this.unitRepo.findKggbi(subjectUnitUri));
			subjectUnit = this.unitRepo.findByUpri(subjectUnitUri, this.storageModelManager.getStorageModel(subjectUnitKgbbi.getKgbb()));
			Interconnect interconnect = subjectUnitKgbbi.findInterconnect(kgbbiUri);
			
			if(interconnect.getQuantity() > 0)
			{
				long count = subjectUnit.getCurrentAssociatedSemanticUnits().stream()
					.filter(associatedUri -> kgbbiUri.equals(this.findSemanticUnit(associatedUri).getKgbbi()))
					.count();
				
				if(count >= interconnect.getQuantity())
				{
					throw new AssociationException("Exceeded quantity limit for " + kgbbiUri + " in " + subjectUnit.getKgbbi());
				}
			}
		}
		
		CompoundUnitResult result = this.buildCompoundUnit(kgbbiUri, contributor, inputs, subjectUnit);
		
		if(subjectUnit != null)
		{
			subjectUnit.addAssociatedSemanticUnit(result.compound().getUri());
		}
		
		result.save(this.unitRepo);
		return result;
	}
	
	public CompoundUnitResult buildCompoundUnit(URI kgbbiUri, URI contributor, Input inputs, @Nullable SemanticUnit subjectUnit) throws IllegalArgumentException, AssociationException, StorageModelException, OntologyServiceException
	{
		KGBBInstance kgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
		LinearStorageModel storageModel = this.storageModelManager.getStorageModel(kgbbi.getKgbb()).getSubjectStorageModel();
		this.assertSemanticUnitType(kgbbi, SemanticUnitType.COMPOUND);
		List<StatementUnitResult> statements = new ArrayList<StatementUnitResult>();
		Either<LiteralInput, ResourceInput> input = inputs.get(kgbbiUri);
		
		if(input == null || input.isLeft())
		{
			throw new IllegalArgumentException("Missing input for compound unit " + kgbbiUri);
		}
		
		ResourceInput resource = input.right().get();
		SemanticUnit niiu;
		
		if(ResourceType.CLASS.equals(resource.getType()))
		{
			if(storageModel.getSubclassOf() != null && !this.ontoService.hasAncestor(resource.getUri(), storageModel.getSubclassOf()))
			{
				throw new IllegalArgumentException(resource.getUri() + " is not a subclass of " + storageModel.getSubclassOf());
			}
			
			String label = this.ontoService.resourceLabel(resource.getUri());
			niiu = this.createNIIU(resource.getUri(), label, (resource.getLabel() == null || resource.getLabel().isBlank()) ? label : resource.getLabel(), contributor, storageModel.getSubclassOf());
		}
		else if(ResourceType.INSTANCE.equals(resource.getType()))
		{
			niiu = this.unitRepo.findBySubject(resource.getUri(), Constants.NIIU_KGBB_URI, this.storageModelManager.getStorageModel(Constants.NIIU_KGBB_URI));
			URI niiuType = niiu.findResourceObject(Constants.NIIU_RESOURCE_POSITION_URI).getResourceURI();
			
			if(storageModel.getSubclassOf() != null && !this.ontoService.hasAncestor(niiuType, storageModel.getSubclassOf()))
			{
				throw new IllegalArgumentException(niiuType + " is not a subclass of " + storageModel.getSubclassOf());
			}
		}
		else
		{
			throw new IllegalArgumentException("Input is neither of type class nor of type instance");
		}
		
		SemanticUnit compound = this.createSemanticUnit(kgbbi, contributor, niiu.getSubject());
		compound.addAssociatedSemanticUnit(niiu.getUri());
		
		for(Association association : kgbbi.getAssociations())
		{
			if(association.isRequired())
			{
				KGBBInstance targetKgbbi = this.kgbbAppSpecRepo.findById(association.getTargetKgbbi());
				SemanticUnitType type = this.kgbbRepo.getType(targetKgbbi.getKgbb());
				
				if(SemanticUnitType.STATEMENT.equals(type))
				{
					StatementUnitResult result = this.buildStatementUnit(association.getTargetKgbbi(), contributor, inputs, compound, kgbbi, association.resolveSubject(compound), association);
					compound.addAssociatedSemanticUnit(result.statement().getUri());
					statements.add(result);
				}
				else
				{
					throw new RuntimeException("Associating a " + association.getTargetKgbbi() + " with type " + type + " to a compound unit is not supported in this prototype");
				}
			}
		}
		
		return new CompoundUnitResult(compound, niiu, subjectUnit, statements);
	}
	
	@Override
	public StatementUnitResult createStatementUnit(URI kgbbiUri, URI contributor, Input inputs, URI subjectUnitUri) throws IllegalArgumentException, AssociationException, StorageModelException, OntologyServiceException
	{
		this.userRepo.findById(contributor);
		KGBBInstance subjectUnitKgbbi = this.kgbbAppSpecRepo.findById(this.unitRepo.findKggbi(subjectUnitUri));
		SemanticUnit subjectUnit = this.unitRepo.findByUpri(subjectUnitUri, this.storageModelManager.getStorageModel(subjectUnitKgbbi.getKgbb()));
		Interconnect interconnect = subjectUnitKgbbi.findInterconnect(kgbbiUri);
		
		if(interconnect.getQuantity() > 0)
		{
			long count = subjectUnit.getCurrentAssociatedSemanticUnits().stream()
				.filter(associatedUri -> kgbbiUri.equals(this.findSemanticUnit(associatedUri).getKgbbi()))
				.count();
			
			if(count >= interconnect.getQuantity())
			{
				throw new AssociationException("Exceeded quantity limit for " + kgbbiUri + " in " + subjectUnit.getKgbbi());
			}
		}
		
		StatementUnitResult result = this.buildStatementUnit(kgbbiUri, contributor, inputs, subjectUnit, subjectUnitKgbbi, interconnect.resolveSubject(subjectUnit), interconnect);
		result.save(this.unitRepo);
		return result;
	}
	
	public StatementUnitResult buildStatementUnit(URI kgbbiUri, URI contributor, Input inputs, SemanticUnit subjectUnit, KGBBInstance subjectUnitKgbbi, URI subject, Interconnect interconnect) throws IllegalArgumentException, AssociationException, StorageModelException, OntologyServiceException
	{
		KGBBInstance statementKgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
		this.assertSemanticUnitType(statementKgbbi, SemanticUnitType.STATEMENT);
		SemanticUnit statement = this.createSemanticUnit(statementKgbbi, contributor, subject);
		
		if(!SemanticUnitType.STATEMENT.equals(this.kgbbRepo.getType(subjectUnit.getKgbb())))
		{
			subjectUnit.addAssociatedSemanticUnit(statement.getUri());
		}
		
		List<SemanticUnit> updatedUnits = new ArrayList<SemanticUnit>();
		
		// The functionality in this code block should be implemented in a better way
		// This code updates the subject unit of the subject unit of this statement unit if possible
		// Proposal:
		// Save the URI of the unit that created this statement unit and find it that way
		if(interconnect instanceof Link)
		{
			subjectUnit.addObjectDescribedBySemanticUnit(statement.getUri());
			
			if(SemanticUnitType.STATEMENT.equals(this.kgbbRepo.getType(subjectUnit.getKgbb())))
			{
				try
				{
					SemanticUnit niiu = this.unitRepo.findBySubject(subject, Constants.NIIU_KGBB_URI, this.storageModelManager.getStorageModel(Constants.NIIU_KGBB_URI));
					statement.addAssociatedSemanticUnit(niiu.getUri());
					Set<URI> associatingUris = this.unitRepo.findAssociatingUnits(subjectUnit.getUri());
					
					for(URI associatingUri : associatingUris)
					{
						URI associatingKgbbiUri = this.unitRepo.findKggbi(associatingUri);
						KGBBInstance associatingUnitKgbbi = this.kgbbAppSpecRepo.findById(associatingKgbbiUri);
						SemanticUnit associatingUnit = this.unitRepo.findByUpri(associatingUri, this.storageModelManager.getStorageModel(associatingUnitKgbbi.getKgbb()));
						associatingUnit.addAssociatedSemanticUnit(statement.getUri());
						updatedUnits.add(associatingUnit);
					}
				}
				catch(Exception e)
				{
					// No need to update associations because the corresponding units do not exist (probably they are just being built)
				}
			}
		}
		
		Map<URI, Link> links = statementKgbbi.getLinks().stream().collect(Collectors.toMap(Link::getUseAsSubject, Function.identity()));
		Map<URI, CompoundUnitResult> compounds = new HashMap<URI, CompoundUnitResult>();
		List<StatementUnitResult> statements = new ArrayList<StatementUnitResult>();
		List<SemanticUnit> niius = new ArrayList<SemanticUnit>();
		
		for(Entry<URI, LinearStorageModel> entry : statement.getStorageModel().getObjectStorageModels().entrySet())
		{
			URI inputClass = entry.getKey();
			Either<LiteralInput, ResourceInput> input = inputs.get(inputClass);
			
			if(input == null)
			{
				if(statement.getStorageModel().getSubjectStorageModel().getSlotOrThrow(inputClass.toString()).isRequired())
				{
					throw new IllegalArgumentException("Missing input for object position " + inputClass);
				}
				
				continue;
			}
			
			ObjectPosition position = this.positionRepo.findById(inputClass);
			boolean isResource = this.positionRepo.isResourceObjectPosition(position);
			
			if(input.isLeft())
			{
				if(isResource)
				{
					throw new IllegalArgumentException("Expected input for object position " + inputClass + " to be of type literal");
				}
				
				this.addLiteralInput(statement, input.left().get().getLiteral(), entry.getValue(), inputClass, contributor);
			}
			else if(input.isRight())
			{
				if(!isResource)
				{
					throw new IllegalArgumentException("Expected input for object position " + inputClass + " to be of type resource");
				}
				
				ResourceInput resourceInput = input.right().get();
				LinearStorageModel subjectUnitSubjectStorageModel = this.storageModelManager.getStorageModel(subjectUnitKgbbi.getKgbb()).getSubjectStorageModel();
				URI uri = resourceInput.getUri();
				URI constraint = interconnect.hasPositionConstraint(inputClass) ? subjectUnitSubjectStorageModel.getSubclassOf() : null;
				Link link = links.get(inputClass);
				
				if(ResourceType.CLASS.equals(resourceInput.getType()))
				{
					if(constraint != null && !this.ontoService.hasAncestor(uri, constraint))
					{
						throw new IllegalArgumentException(uri + " is not a subclass of " + constraint);
					}
					
					if(link != null)
					{
						KGBBInstance targetKgbbi = this.kgbbAppSpecRepo.findById(link.getTargetKgbbi());
						SemanticUnitType type = this.kgbbRepo.getType(targetKgbbi.getKgbb());
						Input delegated = inputs.shallowCopy();
						delegated.put(link.getTargetKgbbi(), input);
						
						if(SemanticUnitType.COMPOUND.equals(type))
						{
							try
							{
								CompoundUnitResult result = this.buildCompoundUnit(link.getTargetKgbbi(), contributor, delegated, null);
								statement.addObjectDescribedBySemanticUnit(result.compound().getUri());
								subjectUnit.addAssociatedSemanticUnit(result.niiu().getUri());
								subjectUnit.addLinkedSemanticUnit(result.compound().getUri());
								compounds.put(inputClass, result);
								this.addResourceInput(statement, result.compound().getSubject(), null, entry.getValue(), inputClass, contributor, constraint);
							}
							catch(Exception e)
							{
								if(link.isRequired())
								{
									throw new IllegalArgumentException("Missing input for linked kgbbi " + link.getTargetKgbbi());
								}
								
								this.addResourceInput(statement, uri, null, entry.getValue(), inputClass, contributor, constraint);
							}
						}
						else if(SemanticUnitType.STATEMENT.equals(type))
						{
							try
							{
								String label = this.ontoService.resourceLabel(uri);
								SemanticUnit niiu = this.createNIIU(uri, label, label, contributor, constraint);
								subjectUnit.addAssociatedSemanticUnit(niiu.getUri());
								StatementUnitResult result = this.buildStatementUnit(link.getTargetKgbbi(), contributor, delegated, statement, statementKgbbi, niiu.getSubject(), link);
								statement.addObjectDescribedBySemanticUnit(result.statement().getUri());
								result.statement().addAssociatedSemanticUnit(niiu.getUri());
								subjectUnit.addAssociatedSemanticUnit(result.statement().getUri());
								statements.add(result);
								this.addResourceInput(statement, niiu.getSubject(), null, entry.getValue(), inputClass, contributor, constraint);
								niius.add(niiu);
							}
							catch(Exception e)
							{
								e.printStackTrace();
								
								if(link.isRequired())
								{
									throw new IllegalArgumentException("Missing input for linked kgbbi " + link.getTargetKgbbi());
								}
								
								this.addResourceInput(statement, uri, null, entry.getValue(), inputClass, contributor, constraint);
							}
						}
						else
						{
							throw new RuntimeException("Linking a " + subjectUnit.getKgbbi() + " with type " + type + " to a statement unit is not supported in this prototype");
						}
					}
					else
					{
						this.addResourceInput(statement, uri, this.ontoService.resourceLabel(uri), entry.getValue(), inputClass, contributor, constraint);
					}
				}
				else if(ResourceType.INSTANCE.equals(resourceInput.getType()))
				{
					URI compoundKgbbiUri = this.unitRepo.findKggbi(resourceInput.getUri());
					
					if(link != null && !compoundKgbbiUri.equals(link.getTargetKgbbi()))
					{
						throw new IllegalArgumentException("Input for " + inputClass + " has to be described by a semantic unit of type " + statementKgbbi.getUri());
					}
					
					KGBBInstance compoundKgbbi = this.kgbbAppSpecRepo.findById(compoundKgbbiUri);
					this.assertSemanticUnitType(compoundKgbbi, SemanticUnitType.COMPOUND);
					SemanticUnit compound = this.unitRepo.findByUpri(resourceInput.getUri(), this.storageModelManager.getStorageModel(compoundKgbbi.getKgbb()));
					SemanticUnit niiu = this.unitRepo.findBySubject(compound.getSubject(), Constants.NIIU_KGBB_URI, this.storageModelManager.getStorageModel(Constants.NIIU_KGBB_URI));
					ResourceObjectNode type = niiu.findResourceObject(Constants.NIIU_RESOURCE_POSITION_URI);
					
					if(constraint != null && !this.ontoService.hasAncestor(type.getResourceURI(), constraint))
					{
						throw new IllegalArgumentException(type.getResourceURI() + " does not satisfy constraint " + constraint);
					}
					
					statement.addObjectDescribedBySemanticUnit(compound.getUri());
					subjectUnit.addAssociatedSemanticUnit(niiu.getUri());
					subjectUnit.addLinkedSemanticUnit(compound.getUri());
					this.addResourceInput(statement, compound.getSubject(), null, entry.getValue(), inputClass, contributor, constraint);
				}
			}
		}
		
		for(Association association : statementKgbbi.getAssociations())
		{
			if(association.isRequired())
			{
				throw new RuntimeException("Associations for for statement units are not supported in this prototype");
			}
		}
		
		return new StatementUnitResult(statement, compounds, subjectUnit, statements, niius, updatedUnits);
	}
	
	@Override
	public SemanticUnit updateStatementUnit(URI upri, URI contributor, Input inputs, @Nullable URI subjectUnitUpri) throws IllegalArgumentException, StorageModelException, AssociationException, OntologyServiceException
	{
		this.userRepo.findById(contributor);
		URI kgbbiUri = this.unitRepo.findKggbi(upri);
		KGBBInstance statementKgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
		this.assertSemanticUnitType(statementKgbbi, SemanticUnitType.STATEMENT);
		SemanticUnit statement = this.unitRepo.findByUpri(upri, this.storageModelManager.getStorageModel(statementKgbbi.getKgbb()));
		
		if(!statement.isCurrentVersion())
		{
			throw new IllegalArgumentException("Tried to update deleted semantic unit " + upri);
		}
		
		SemanticUnit subjectUnit = null;
		Set<URI> potentialAssociationRemove = new HashSet<URI>();
		Set<URI> potentialObjectDescribedByRemove = new HashSet<URI>();
		Set<URI> potentialLinkRemove = new HashSet<URI>();
		
		for(Entry<URI, LinearStorageModel> entry : statement.getStorageModel().getObjectStorageModels().entrySet())
		{
			URI inputClass = entry.getKey();
			LinearStorageModel model = entry.getValue();
			Either<LiteralInput, ResourceInput> input = inputs.get(inputClass);
			
			if(input == null)
			{
				continue;
			}
			
			ObjectPosition position = this.positionRepo.findById(inputClass);
			boolean isResource = this.positionRepo.isResourceObjectPosition(position);
			
			if(input.isLeft())
			{
				if(isResource)
				{
					throw new IllegalArgumentException("Expected input for object position " + inputClass + " to be of type resource");
				}
				
				statement.findLiteralObject(inputClass).setCurrentVersion(false);
				this.addLiteralInput(statement, input.left().get().getLiteral(), model, inputClass, contributor);
			}
			else if(input.isRight())
			{
				if(!isResource)
				{
					throw new IllegalArgumentException("Expected input for object position " + inputClass + " to be of type literal");
				}
				
				ResourceInput resourceInput = input.right().get();
				ResourceObjectNode current = statement.findResourceObject(inputClass);
				URI uri = resourceInput.getUri();
				
				if(ResourceType.CLASS.equals(resourceInput.getType()))
				{
					Set<URI> constraints = this.unitRepo.findAllConstraints(statement.getSubject(), this.storageModelManager);
					
					if(!this.ontoService.hasAncestors(uri, constraints))
					{
						throw new IllegalArgumentException(uri + " does not satisfy all constraints " + constraints);
					}
					
					current.setCurrentVersion(false);
					this.addResourceInput(statement, uri, this.resolveResourceLabel(resourceInput.getLabel(), uri), model, inputClass, contributor, current.getConstraint());
				}
				else if(ResourceType.INSTANCE.equals(resourceInput.getType()))
				{
					URI constraint = current.getConstraint();
					URI describedByKgbbiUri = this.unitRepo.findKggbi(resourceInput.getUri());
					KGBBInstance describedByKgbbi = this.kgbbAppSpecRepo.findById(describedByKgbbiUri);
					this.assertSemanticUnitType(describedByKgbbi, SemanticUnitType.COMPOUND);
					SemanticUnit targetDescribedBy = this.unitRepo.findByUpri(resourceInput.getUri(), this.storageModelManager.getStorageModel(describedByKgbbi.getKgbb()));
					SemanticUnit targetNiiu = this.unitRepo.findBySubject(targetDescribedBy.getSubject(), Constants.NIIU_KGBB_URI, this.storageModelManager.getStorageModel(Constants.NIIU_KGBB_URI));
					ResourceObjectNode type = targetNiiu.findResourceObject(Constants.NIIU_RESOURCE_POSITION_URI);
					
					if(targetDescribedBy.getSubject().equals(statement.getSubject()))
					{
						throw new IllegalArgumentException("Cannot describe self");
					}
					
					if(!this.ontoService.hasAncestor(type.getResourceURI(), constraint))
					{
						throw new IllegalArgumentException(type.getResourceURI() + " does not satisfy constraint " + constraint);
					}
					
					if(statement.getCurrentObjectDescribedBySemanticUnits().isEmpty() && subjectUnitUpri != null || subjectUnitUpri == null && !statement.getCurrentObjectDescribedBySemanticUnits().isEmpty())
					{
						throw new IllegalStateException();
					}
					
					if(subjectUnitUpri != null)
					{
						if(subjectUnit == null)
						{
							URI subjectKgbbiUri = this.unitRepo.findKggbi(subjectUnitUpri);
							KGBBInstance subjectKgbbi = this.kgbbAppSpecRepo.findById(subjectKgbbiUri);
							this.assertSemanticUnitType(subjectKgbbi, SemanticUnitType.COMPOUND);
							subjectUnit = this.unitRepo.findByUpri(subjectUnitUpri, this.storageModelManager.getStorageModel(subjectKgbbi.getKgbb()));
						}
						
						if(!subjectUnit.getSubject().equals(statement.getSubject()))
						{
							throw new IllegalArgumentException("Statement unit and compound unit must share the same subject");
						}
						
						for(URI objectDescribedBy : statement.getCurrentObjectDescribedBySemanticUnits())
						{
							URI currentDescribedByKgbbiUri = this.unitRepo.findKggbi(objectDescribedBy);
							KGBBInstance currentDescribedByKgbbi = this.kgbbAppSpecRepo.findById(currentDescribedByKgbbiUri);
							this.assertSemanticUnitType(currentDescribedByKgbbi, SemanticUnitType.COMPOUND);
							SemanticUnit currentDescribedBy = this.unitRepo.findByUpri(objectDescribedBy, this.storageModelManager.getStorageModel(currentDescribedByKgbbi.getKgbb()));
							SemanticUnit currentNiiu = this.unitRepo.findBySubject(currentDescribedBy.getSubject(), Constants.NIIU_KGBB_URI, this.storageModelManager.getStorageModel(Constants.NIIU_KGBB_URI));
							
							potentialAssociationRemove.add(currentNiiu.getUri());
							potentialObjectDescribedByRemove.add(objectDescribedBy);
							potentialLinkRemove.add(currentDescribedBy.getUri());
						}
						
						subjectUnit.addAssociatedSemanticUnit(targetNiiu.getUri());
						subjectUnit.addLinkedSemanticUnit(targetDescribedBy.getUri());
					}
					
					statement.addObjectDescribedBySemanticUnit(targetDescribedBy.getUri());
					current.setCurrentVersion(false);
					this.addResourceInput(statement, targetDescribedBy.getSubject(), null, model, inputClass, contributor, current.getConstraint());
				}
			}
		}
		
		for(ResourceObjectNode resource : statement.getResourceObjects())
		{
			if(!resource.isCurrentVersion())
			{
				continue;
			}
			
			if(resource.getResourceLabel() == null) //resource object contains an instance
			{
				potentialObjectDescribedByRemove.remove(resource.getResourceURI());
			}
		}
		
		for(URI objectDescribedBy : potentialObjectDescribedByRemove)
		{
			statement.removeObjectDescribedBySemanticUnit(objectDescribedBy);
		}
		
		this.unitRepo.save(statement);
		
		if(subjectUnit != null)
		{
			for(URI uri : subjectUnit.getCurrentAssociatedSemanticUnits())
			{
				if(uri.equals(upri))
				{
					continue;
				}
				
				URI kgbbiUri2 = this.unitRepo.findKggbi(uri);
				KGBBInstance kgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri2);
				
				if(!SemanticUnitType.STATEMENT.equals(this.kgbbRepo.getType(kgbbi.getKgbb())))
				{
					continue;
				}
				
				SemanticUnit associated = this.unitRepo.findByUpri(uri, this.storageModelManager.getStorageModel(kgbbi.getKgbb()));
				potentialLinkRemove.removeAll(associated.getCurrentObjectDescribedBySemanticUnits());
				
				for(ResourceObjectNode resource : associated.getResourceObjects())
				{
					if(!resource.isCurrentVersion())
					{
						continue;
					}
					
					if(resource.getResourceLabel() == null) //resource object contains an instance
					{
						potentialAssociationRemove.remove(resource.getResourceURI());
					}
				}
			}
			
			potentialLinkRemove.forEach(subjectUnit::removeLinkedSemanticUnit);
			potentialAssociationRemove.forEach(subjectUnit::removeAssociatedSemanticUnit);
			this.unitRepo.save(subjectUnit);
		}
		
		return statement;
	}
	
	@Override
	public void deleteCompoundUnit(URI upri, URI contributor)
	{
		this.userRepo.findById(contributor);
		URI kgbbiUri = this.unitRepo.findKggbi(upri);
		KGBBInstance compoundKgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
		this.assertSemanticUnitType(compoundKgbbi, SemanticUnitType.COMPOUND);
		LinearStorageModel resourceStorageModel = this.storageModelManager.getObjectStorageModel(Constants.RESOURCE_OBJECT_POSITION);
		SemanticUnit compound = this.unitRepo.findByUpri(upri, this.storageModelManager.getStorageModel(compoundKgbbi.getKgbb()));
		
		if(!compound.isCurrentVersion())
		{
			throw new IllegalArgumentException("Tried to delete already delteted semantic unit");
		}
		
		compound.setDeletedBy(contributor);
		
		boolean keepNiiu = this.unitRepo.isSubjectUsedAsObject(compound.getSubject()) || this.unitRepo.isSubjectUsedByOtherUnit(compound.getSubject(), compound.getUri());
		Set<SemanticUnit> referencedNiius = new HashSet<SemanticUnit>();
		
		for(URI associatedUri : compound.getCurrentAssociatedSemanticUnits())
		{
			URI associatedKgbbiUri = this.unitRepo.findKggbi(associatedUri);
			KGBBInstance associatedKgbbi = this.kgbbAppSpecRepo.findById(associatedKgbbiUri);
			SemanticUnitStorageModel statementModel = this.storageModelManager.getStorageModel(associatedKgbbi.getKgbb());
			SemanticUnit associated = this.unitRepo.findByUpri(associatedUri, statementModel);
			SemanticUnitType type = this.kgbbRepo.getType(associatedKgbbi.getKgbb());
			
			if(SemanticUnitType.STATEMENT.equals(type))
			{
				if(associatedKgbbi.getKgbb().equals(Constants.NIIU_KGBB_URI))
				{
					if(associated.getSubject().equals(compound.getSubject()))
					{
						if(keepNiiu)
						{
							compound.removeAssociatedSemanticUnit(associated.getUri());
						}
						else
						{
							this.deleteStatementUnit(associated, contributor, compound, resourceStorageModel);
							this.unitRepo.delete(associated);
						}
					}
					else
					{
						// Keep track of niius that do not share the same subject. This is because
						// there might still be units left associated by this compound unit that would
						// indicate that this niiu is still used by another unit.
						referencedNiius.add(associated);
						compound.removeAssociatedSemanticUnit(associated.getUri());
					}
				}
				else
				{
					this.deleteStatementUnit(associated, contributor, compound, resourceStorageModel);
					this.unitRepo.delete(associated);
				}
			}
			else if(SemanticUnitType.COMPOUND.equals(type))
			{
				compound.removeAssociatedSemanticUnit(associated.getUri());
			}
		}
		
		for(SemanticUnit niiu : referencedNiius)
		{
			if(!this.unitRepo.isSubjectUsedAsObject(niiu.getSubject()) && !this.unitRepo.isSubjectUsedByOtherUnit(niiu.getSubject(), compound.getUri()))
			{
				this.deleteStatementUnit(niiu, contributor, compound, resourceStorageModel);
				this.unitRepo.delete(niiu);
			}
		}
		
		this.unitRepo.delete(compound);
	}
	
	@Override
	public void deleteStatementUnit(URI upri, URI contributor, URI subjectUnitUpri) throws AssociationException
	{
		this.userRepo.findById(contributor);
		URI kgbbiUri = this.unitRepo.findKggbi(upri);
		KGBBInstance statementKgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
		this.assertSemanticUnitType(statementKgbbi, SemanticUnitType.STATEMENT);
		LinearStorageModel resourceStorageModel = this.storageModelManager.getObjectStorageModel(Constants.RESOURCE_OBJECT_POSITION);
		SemanticUnit statement = this.unitRepo.findByUpri(upri, this.storageModelManager.getStorageModel(statementKgbbi.getKgbb()));
		
		URI subjectKgbbiUri = this.unitRepo.findKggbi(subjectUnitUpri);
		KGBBInstance subjectKgbbi = this.kgbbAppSpecRepo.findById(subjectKgbbiUri);
		subjectKgbbi.findInterconnect(kgbbiUri);
		SemanticUnit subjectUnit = this.unitRepo.findByUpri(subjectUnitUpri, this.storageModelManager.getStorageModel(subjectKgbbi.getKgbb()));
		
		this.deleteStatementUnit(statement, contributor, subjectUnit, resourceStorageModel);
		
		this.unitRepo.save(subjectUnit);
		this.unitRepo.delete(statement);
	}
	
	private void deleteStatementUnit(SemanticUnit statement, URI contributor, SemanticUnit subjectUnit, LinearStorageModel resourceStorageModel)
	{
		if(!statement.isCurrentVersion())
		{
			throw new IllegalArgumentException("Tried to delete already delteted semantic unit");
		}
		
		statement.setCurrentVersion(false);
		statement.setDeletedBy(contributor);
		
		for(ResourceObjectNode node : statement.getResourceObjects())
		{
			node.setCurrentVersion(false);
		}
		
		for(LiteralObjectNode node : statement.getLiteralObjects())
		{
			node.setCurrentVersion(false);
		}
		
		SemanticUnitType type = this.kgbbRepo.getType(subjectUnit.getKgbb());
		
		if(SemanticUnitType.COMPOUND.equals(type))
		{
			subjectUnit.removeAssociatedSemanticUnit(statement.getUri());
			
			if(!statement.getCurrentObjectDescribedBySemanticUnits().isEmpty())
			{
				Set<URI> describedBys = new HashSet<URI>();
				
				for(URI associatedUri : subjectUnit.getCurrentAssociatedSemanticUnits())
				{
					if(associatedUri.equals(statement.getUri()))
					{
						continue;
					}
					
					URI kgbbiUri = this.unitRepo.findKggbi(associatedUri);
					KGBBInstance kgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
					
					if(!SemanticUnitType.STATEMENT.equals(this.kgbbRepo.getType(kgbbi.getKgbb())))
					{
						continue;
					}
					
					SemanticUnit associated = this.unitRepo.findByUpri(associatedUri, this.storageModelManager.getStorageModel(kgbbi.getKgbb()));
					describedBys.addAll(associated.getCurrentObjectDescribedBySemanticUnits());
				}
				
				for(URI objectDescribedBy : statement.getCurrentObjectDescribedBySemanticUnits())
				{
					if(!describedBys.contains(objectDescribedBy))
					{
						URI kgbbiUri = this.unitRepo.findKggbi(objectDescribedBy);
						KGBBInstance kgbbi = this.kgbbAppSpecRepo.findById(kgbbiUri);
						SemanticUnit describedByCompound = this.unitRepo.findByUpri(objectDescribedBy, this.storageModelManager.getStorageModel(kgbbi.getKgbb()));
						SemanticUnit describedByNiiu = this.unitRepo.findBySubject(describedByCompound.getSubject(), Constants.NIIU_KGBB_URI, this.storageModelManager.getStorageModel(Constants.NIIU_KGBB_URI));
						subjectUnit.removeAssociatedSemanticUnit(describedByNiiu.getUri());
						subjectUnit.removeLinkedSemanticUnit(describedByCompound.getUri());
					}
					
					statement.removeObjectDescribedBySemanticUnit(objectDescribedBy);
				}
			}
		}
		else if(SemanticUnitType.STATEMENT.equals(type))
		{
			subjectUnit.removeObjectDescribedBySemanticUnit(statement.getUri());
		}
	}
	
	private SemanticUnit createSemanticUnit(KGBBInstance kgbbi, URI contributor, URI subject) throws IllegalArgumentException, StorageModelException
	{
		SemanticUnitStorageModel model = this.storageModelManager.getStorageModel(kgbbi.getKgbb());
		SemanticUnit unit = new SemanticUnit(this.newURI(), this.newURI(), model);
		unit.setSubject(subject);
		unit.setKgbb(kgbbi.getKgbb());
		unit.setKgbbi(kgbbi.getUri());
		unit.setCreator(contributor);
		unit.setCreationDate(LocalDateTime.now());
		unit.setCreatedWithApplication(Constants.APPLICATION_URI);
		
		Slot<?> labelSlot = model.getSemanticUnitStorageModel().getSlots().get(Constants.STORAGE_TEMPLATE_SLOT_LABEL);
		
		if(labelSlot != null && labelSlot instanceof StringSlot label)
		{
			unit.setLabel((String) label.getIfAbsent());
		}
		
		Slot<?> descSlot = model.getSemanticUnitStorageModel().getSlots().get(Constants.STORAGE_TEMPLATE_SLOT_DESCRIPTION);
		
		if(descSlot != null && descSlot instanceof StringSlot desc)
		{
			unit.setDescription((String) desc.getIfAbsent());
		}
		
		return unit;
	}
	
	private LiteralObjectNode addLiteralInput(SemanticUnit unit, String literal, URI inputClass, URI contributor) throws IllegalArgumentException, StorageModelException
	{
		return this.addLiteralInput(unit, literal, unit.getStorageModel().getObjectStorageModels().get(inputClass), inputClass, contributor);
	}
	
	private LiteralObjectNode addLiteralInput(SemanticUnit unit, String literal, LinearStorageModel model, URI inputClass, URI contributor) throws IllegalArgumentException, StorageModelException
	{
		Object object = model.parseAndValidateInput(Constants.STORAGE_TEMPLATE_SLOT_LITERAL, literal);
		LiteralObjectNode node = new LiteralObjectNode(this.newURI());
		node.setLiteral(object);
		node.setObjectPositionClass(inputClass);
		node.setCreator(contributor);
		node.setCreationDate(LocalDateTime.now());
		node.setCreatedWithApplication(Constants.APPLICATION_URI);
		unit.addLiteralObject(node);
		
		Slot<?> labelSlot = model.getSlots().get(Constants.STORAGE_TEMPLATE_SLOT_INPUT_TYPE_LABEL);
		
		if(labelSlot != null && labelSlot instanceof StringSlot label)
		{
			node.setInputTypeLabel((String) label.getIfAbsent());
		}
		
		return node;
	}
	
	private ResourceObjectNode addResourceInput(SemanticUnit unit, URI uri, @Nullable String resourceLabel, URI inputClass, URI contributor, @Nullable URI constraint) throws IllegalArgumentException, StorageModelException
	{
		return this.addResourceInput(unit, uri, resourceLabel, unit.getStorageModel().getObjectStorageModels().get(inputClass), inputClass, contributor, constraint);
	}
	
	private ResourceObjectNode addResourceInput(SemanticUnit unit, URI uri, @Nullable String resourceLabel, LinearStorageModel model, URI inputClass, URI contributor, @Nullable URI constraint) throws IllegalArgumentException, StorageModelException
	{
		model.validateSlotInput(Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI, uri);
		ResourceObjectNode node = new ResourceObjectNode(this.newURI());
		node.setResourceURI(uri);
		node.setResourceLabel(resourceLabel);
		node.setObjectPositionClass(inputClass);
		node.setCreator(contributor);
		node.setCreationDate(LocalDateTime.now());
		node.setCreatedWithApplication(Constants.APPLICATION_URI);
		node.setConstraint(constraint);
		unit.addResourceObject(node);
		
		Slot<?> labelSlot = model.getSlots().get(Constants.STORAGE_TEMPLATE_SLOT_INPUT_TYPE_LABEL);
		
		if(labelSlot != null && labelSlot instanceof StringSlot label)
		{
			node.setInputTypeLabel((String) label.getIfAbsent());
		}
		return node;
	}
	
	private SemanticUnit createNIIU(URI uri, String resourceLabel, String label, URI contributor, URI constraint) throws IllegalArgumentException, StorageModelException, OntologyServiceException
	{
		SemanticUnit niiu = this.createSemanticUnit(this.kgbbAppSpecRepo.findById(Constants.NIIU_KGBBI_URI), contributor, this.newURI());
		this.addResourceInput(niiu, uri, resourceLabel, Constants.NIIU_RESOURCE_POSITION_URI, contributor, constraint);
		this.addLiteralInput(niiu, label, Constants.NIIU_LITERAL_POSITION_URI, contributor);
		return niiu;
	}
	
	@Override
	public Set<URI> findAll(URI kgbb)
	{
		return this.unitRepo.findAll(kgbb);
	}
	
	@Override
	public SemanticUnit findBySubject(URI subject, URI kgbb)
	{
		return this.unitRepo.findBySubject(subject, kgbb, this.storageModelManager.getStorageModel(kgbb));
	}
	
	private URI newURI()
	{
		while(true)
		{
			URI uri = this.uriSupplier.newURI();
			
			if(this.unitRepo.isUnique(uri))
			{
				return uri;
			}
		}
	}
	
	private String resolveResourceLabel(@Nullable String label, URI resourceURI) throws OntologyServiceException
	{
		if(label == null || label.isBlank())
		{
			return this.ontoService.resourceLabel(resourceURI);
		}
		
		return label;
	}
	
	public StorageModelManager getStorageModelManager()
	{
		return this.storageModelManager;
	}
	
	public SemanticUnitRepository getSemanticUnitRepo()
	{
		return this.unitRepo;
	}
	
	public SemanticUnitType getSemanticUnitType(SemanticUnit unit)
	{
		return this.kgbbRepo.getType(unit.getKgbb());
	}
	
	private void assertSemanticUnitType(KGBBInstance kgbbi, SemanticUnitType type)
	{
		SemanticUnitType actual = this.kgbbRepo.getType(kgbbi.getKgbb());
		
		if(!type.equals(actual))
		{
			throw new IllegalArgumentException("Expected semantic unit " + kgbbi.getUri() + " to be of type " + type);
		}
	}
	
	public static record CompoundUnitResult(SemanticUnit compound, SemanticUnit niiu, @Nullable SemanticUnit subjectUnit, List<StatementUnitResult> statements)
	{
		public void save(SemanticUnitRepository unitRepo)
		{
			unitRepo.save(this.niiu);
			unitRepo.save(this.compound);
			
			if(this.subjectUnit != null)
			{
				unitRepo.save(this.subjectUnit);
			}
			
			this.statements.forEach(statement -> statement.save(unitRepo));
		}
	}
	
	public static record StatementUnitResult(SemanticUnit statement, Map<URI, CompoundUnitResult> compounds, SemanticUnit subjectUnit, List<StatementUnitResult> statements, List<SemanticUnit> niius, List<SemanticUnit> updatedUnits)
	{
		public void save(SemanticUnitRepository unitRepo)
		{
			this.compounds.values().forEach(compound -> compound.save(unitRepo));
			unitRepo.save(this.statement);
			unitRepo.save(this.subjectUnit);
			this.niius.forEach(unitRepo::save);
			this.statements.forEach(statement -> statement.save(unitRepo));
			this.updatedUnits.forEach(unitRepo::save);
		}
	}
}
