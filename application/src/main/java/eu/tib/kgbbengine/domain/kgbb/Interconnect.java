package eu.tib.kgbbengine.domain.kgbb;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;

public abstract class Interconnect
{
	private final URI targetKgbbInstance;
	private final boolean required;
	private final int quantity;
	
	public Interconnect(URI targetKgbbInstance, boolean required, int quantity)
	{
		this.targetKgbbInstance = targetKgbbInstance;
		this.required = required;
		this.quantity = quantity;
	}
	
	public abstract URI resolveSubject(SemanticUnit unit);
	
	public abstract boolean hasPositionConstraint(URI position);
	
	public URI getTargetKgbbi()
	{
		return this.targetKgbbInstance;
	}
	
	public boolean isRequired()
	{
		return this.required;
	}
	
	public int getQuantity()
	{
		return this.quantity;
	}
}
