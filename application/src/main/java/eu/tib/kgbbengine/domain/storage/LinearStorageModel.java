package eu.tib.kgbbengine.domain.storage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Constants;

public class LinearStorageModel
{
	public static final LinearStorageModel EMPTY = new LinearStorageModel(null, null, Collections.emptyMap(), Collections.emptyMap());
	
	@Nullable
	private final String name;
	@Nullable
	private final URI subclassOf;
	private final Map<String, String> prefixes;
	private final Map<String, Slot<?>> slots;
	
	public LinearStorageModel(@Nullable String name, @Nullable URI subclassOf, Map<String, String> prefixes, Map<String, Slot<?>> slots)
	{
		this.name = name;
		this.subclassOf = subclassOf;
		this.prefixes = prefixes;
		this.slots = slots;
	}
	
	public URI getSubclassOf()
	{
		return this.subclassOf;
	}
	
	public Slot<?> getSlotOrThrow(String name) throws StorageModelException
	{
		Slot<?> slot = this.slots.get(name);
		
		if(slot == null)
		{
			throw new StorageModelException("Storage model is missing required slot " + name);
		}
		
		return slot;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T parseAndValidateInput(String slotName, String input) throws StorageModelException, IllegalArgumentException
	{
		Slot<T> slot = (Slot<T>) this.getSlotOrThrow(slotName);
		T result;
		
		try
		{
			result = slot.parse(input);
			slot.validate(result);
		}
		catch(Exception e)
		{
			throw new IllegalArgumentException("Could not parse input for slot " + slotName, e);
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T validateSlotInput(String slotName, T input) throws StorageModelException, IllegalArgumentException
	{
		Slot<T> slot = (Slot<T>) this.getSlotOrThrow(slotName);
		slot.validate(input);
		return input;
	}
	
	public Optional<String> resolveSlotURI(String slotName, boolean prependPrefix)
	{
		return this.resolveSlotURI(slotName, prependPrefix, null);
	}
	
	public Optional<String> resolveSlotURI(String slotName, boolean prependPrefix, @Nullable String defaultPrefix)
	{
		Slot<?> slot = this.getSlots().get(slotName);
		
		if(slot == null)
		{
			return Optional.empty();
		}
		
		return this.resolveSlotURI(slot, prependPrefix, defaultPrefix);
	}
	
	public Optional<String> resolveSlotURI(Slot<?> slot, boolean prependPrefix)
	{
		return this.resolveSlotURI(slot, prependPrefix, null);
	}
	
	public Optional<String> resolveSlotURI(Slot<?> slot, boolean prependPrefix, @Nullable String defaultPrefix)
	{
		if(slot.getSlotUri() == null)
		{
			return Optional.empty();
		}
		
		Matcher matcher = Constants.PREFIX_PATTERN.matcher(slot.getSlotUri().toString());
		
		if(matcher.matches())
		{
			String prefix = matcher.group(1);
			
			if(prependPrefix && this.prefixes.containsKey(prefix))
			{
				return Optional.of(this.prefixes.get(prefix) + matcher.group(2));
			}
			
			return Optional.of(matcher.group(2));
		}
		
		if(defaultPrefix != null)
		{
			return Optional.of(defaultPrefix + slot.getSlotUri().toString());
		}
		
		return Optional.of(slot.getSlotUri().toString());
	}
	
	public boolean isEmpty()
	{
		return this.name == null;
	}
	
	@Nullable
	public String getName()
	{
		return this.name;
	}
	
	public Map<String, String> getPrefixes()
	{
		return this.prefixes;
	}
	
	public Map<String, Slot<?>> getSlots()
	{
		return this.slots;
	}
	
	public static LinearStorageModelBuilder builder()
	{
		return new LinearStorageModelBuilder();
	}
	
	public static class LinearStorageModelBuilder
	{
		private final Map<String, String> prefixes = new HashMap<String, String>();
		private final Map<String, ClassModel> classes = new HashMap<String, ClassModel>();
		private final Map<String, Slot<?>> slots = new HashMap<String, Slot<?>>();
		
		private LinearStorageModelBuilder()
		{
			super();
		}
		
		public void expand(PartialStorageModel model)
		{
			for(Entry<String, String> entry : model.getPrefixes().entrySet())
			{
				if(!this.prefixes.containsKey(entry.getKey()))
				{
					this.prefixes.put(entry.getKey(), entry.getValue());
				}
				else if(!this.prefixes.get(entry.getKey()).equals(entry.getValue()))
				{
					throw new IllegalStateException("Multiple definitions for prefix " + entry.getKey());
				}
			}
			
			for(Entry<String, Slot<?>> entry : model.getSlots().entrySet())
			{
				if(this.slots.containsKey(entry.getKey()) && !this.slots.get(entry.getKey()).getClass().equals(entry.getValue().getClass()))
				{
					throw new IllegalStateException("Multiple definitions for slot " + entry.getKey());
				}
				
				this.slots.put(entry.getKey(), entry.getValue());
			}
			
			for(Entry<String, ClassModel> entry : model.getClasses().entrySet())
			{
				if(this.classes.containsKey(entry.getKey()))
				{
					throw new IllegalStateException("Multiple definitions for class " + entry.getKey());
				}
				
				ClassModel classModel = entry.getValue();
				
				if(classModel.getParent() != null && !this.classes.containsKey(classModel.getParent()) && !model.getClasses().containsKey(classModel.getParent()))
				{
					throw new IllegalStateException("Undefined parent class for class " + entry.getKey());
				}
				
				if(classModel.getSlots() != null)
				{
					for(String slot : classModel.getSlots())
					{
						if(!this.slots.containsKey(slot))
						{
							throw new IllegalStateException("Undefined slot " + slot + " for class " + entry.getKey());
						}
					}
				}
				
				this.classes.put(entry.getKey(), entry.getValue());
			}
		}
		
		private void checkCyclic()
		{
			Map<String, List<String>> cache = new HashMap<String, List<String>>();
			
			for(Entry<String, ClassModel> entry : this.classes.entrySet())
			{
				ClassModel classModel = entry.getValue();
				List<String> parents = new ArrayList<String>();
				parents.add(entry.getKey());
				
				while(classModel.getParent() != null)
				{
					if(parents.contains(classModel.getParent()) || cache.containsKey(classModel.getParent()) && cache.get(classModel.getParent()).contains(entry.getKey()))
					{
						throw new IllegalStateException("Storage model contains cyclic class structure " + parents.stream().collect(Collectors.joining(" -> ")) + " -> " + entry.getKey());
					}
					
					parents.add(classModel.getParent());
					classModel = this.classes.get(classModel.getParent());
				}
				
				cache.put(entry.getKey(), parents);
			}
		}
		
		public LinearStorageModel build(String containerClassName)
		{
			if(this.classes.isEmpty())
			{
				return new LinearStorageModel(null, null, Collections.emptyMap(), Collections.emptyMap());
			}
			
			this.checkCyclic();
			ClassModel classModel = this.classes.get(containerClassName);
			
			if(classModel == null)
			{
				throw new IllegalStateException("Storage model does not contain requested container class " + containerClassName);
			}
			
			URI subclassOf = classModel.getSubclassOf();
			Set<String> slotNames = new HashSet<String>();
			
			if(classModel.getSlots() != null)
			{
				slotNames.addAll(classModel.getSlots());
			}
			
			while(classModel.getParent() != null)
			{
				classModel = this.classes.get(classModel.getParent());
				
				if(classModel.getSlots() != null)
				{
					slotNames.addAll(classModel.getSlots());
				}
				
				if(subclassOf == null)
				{
					subclassOf = classModel.getSubclassOf();
				}
			}
			
			Map<String, Slot<?>> slots = new HashMap<String, Slot<?>>();
			
			for(String name : slotNames)
			{
				slots.put(name, this.slots.get(name));
			}
			
			return new LinearStorageModel(containerClassName, subclassOf, Collections.unmodifiableMap(this.prefixes), Collections.unmodifiableMap(slots));
		}
	}
}
