package eu.tib.kgbbengine.domain.kgbb;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;

public class Link extends Interconnect
{
	private final URI useAsSubject;
	
	public Link(URI targetKgbbInstance, boolean required, int quantity, URI useAsSubject)
	{
		super(targetKgbbInstance, required, quantity);
		this.useAsSubject = useAsSubject;
	}
	
	@Override
	public URI resolveSubject(SemanticUnit unit)
	{
		ResourceObjectNode node = unit.findResourceObject(this.getUseAsSubject());
		
		if(node == null)
		{
			throw new IllegalStateException("Expected statement unit kgbb " + unit.getKgbbi() + " to have object position with uri " + this.getUseAsSubject());
		}
		
		return node.getResourceURI();
	}
	
	@Override
	public boolean hasPositionConstraint(URI position)
	{
		return false;
	}
	
	public URI getUseAsSubject()
	{
		return this.useAsSubject;
	}
}
