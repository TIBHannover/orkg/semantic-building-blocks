package eu.tib.kgbbengine.domain.storage;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

public class ClassModel
{
	private final String parent;
	private final URI subclassOf;
	private final List<String> slots;
	
	public ClassModel(@JsonProperty("is_a") String parent, @JsonProperty("subclass_of") URI subclassOf, @JsonProperty("slots") List<String> slots)
	{
		this.parent = parent;
		this.subclassOf = subclassOf;
		this.slots = slots;
	}
	
	public String getParent()
	{
		return this.parent;
	}
	
	public URI getSubclassOf()
	{
		return this.subclassOf;
	}
	
	public List<String> getSlots()
	{
		return this.slots;
	}
}