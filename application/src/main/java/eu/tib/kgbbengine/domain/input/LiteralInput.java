package eu.tib.kgbbengine.domain.input;

public class LiteralInput
{
	private final String literal;
	
	public LiteralInput(String literal)
	{
		this.literal = literal;
	}
	
	public String getLiteral()
	{
		return this.literal;
	}
}