package eu.tib.kgbbengine.domain.storage.slot;

import org.jetbrains.annotations.Nullable;

public enum LiteralRange
{
	URI("uri", URISlot.class),
	STRING("string", StringSlot.class),
	INTEGER("integer", IntegerSlot.class),
	DECIMAL("decimal", DecimalSlot.class),
	BOOLEAN("boolean", BooleanSlot.class),
	DATE("date", DateSlot.class);
	
	private final String key;
	private final Class<? extends Slot<?>> slotClass;
	
	private LiteralRange(String key, Class<? extends Slot<?>> slotClass)
	{
		this.key = key;
		this.slotClass = slotClass;
	}
	
	public String getKey()
	{
		return this.key;
	}
	
	public Class<? extends Slot<?>> getSlotClass()
	{
		return this.slotClass;
	}
	
	@Nullable
	public static LiteralRange fromString(String key)
	{
		if(key == null)
		{
			return null;
		}
		
		for(LiteralRange type : LiteralRange.values())
		{
			if(type.getKey().equals(key))
			{
				return type;
			}
		}
		
		return null;
	}
}
