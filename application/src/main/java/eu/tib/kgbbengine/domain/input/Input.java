package eu.tib.kgbbengine.domain.input;

import java.util.HashMap;
import java.util.Map;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.util.Either;

public class Input
{
	private Map<URI, Either<LiteralInput, ResourceInput>> inputs;
	
	private Input(Map<URI, Either<LiteralInput, ResourceInput>> inputs)
	{
		this.inputs = inputs;
	}
	
	public Either<LiteralInput, ResourceInput> get(URI uri)
	{
		return this.inputs.get(uri);
	}
	
	public void put(URI uri, Either<LiteralInput, ResourceInput> input)
	{
		this.inputs.put(uri, input);
	}
	
	public Input shallowCopy()
	{
		return new Input(this.inputs);
	}
	
	public static InputBuilder builder()
	{
		return new InputBuilder();
	}
	
	public static class InputBuilder
	{
		public Map<URI, Either<LiteralInput, ResourceInput>> inputs = new HashMap<URI, Either<LiteralInput, ResourceInput>>();
		
		private InputBuilder()
		{
			super();
		}
		
		public InputBuilder addLiteralInput(URI positionUri, String literal)
		{
			this.inputs.put(positionUri, Either.left(new LiteralInput(literal)));
			return this;
		}
		
		public InputBuilder addResourceInput(URI positionUri, URI uri, @Nullable String label, ResourceType type)
		{
			this.inputs.put(positionUri, Either.right(new ResourceInput(uri, label, type)));
			return this;
		}
		
		public Input build()
		{
			return new Input(this.inputs);
		}
	}
}
