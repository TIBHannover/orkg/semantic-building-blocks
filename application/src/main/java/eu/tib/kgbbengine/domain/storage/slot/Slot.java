package eu.tib.kgbbengine.domain.storage.slot;

import java.io.IOException;

import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import eu.tib.kgbbengine.domain.URI;

public abstract class Slot<T>
{
	@Nullable
	private final URI slotUri;
	private final boolean required;
	private final boolean multivalued;
	
	public Slot(@Nullable URI slotUri, boolean required, boolean multivalued)
	{
		this.slotUri = slotUri;
		this.required = required;
		this.multivalued = multivalued;
	}
	
	public abstract void validate(T input) throws IllegalArgumentException;
	
	public abstract T parse(String input) throws Exception;
	
	public abstract boolean isClassSlot();
	
	@Nullable
	public URI getSlotUri()
	{
		return this.slotUri;
	}
	
	public boolean isRequired()
	{
		return this.required;
	}
	
	public boolean isMultivalued()
	{
		return this.multivalued;
	}
	
	public static class Deserializer extends StdDeserializer<Slot<?>>
	{
		private static final long serialVersionUID = 2183730982590884096L;
		
		public Deserializer()
		{
			this(null);
		}
		
		public Deserializer(Class<?> vc)
		{
			super(vc);
		}
		
		@Override
		public Slot<?> deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException
		{
			ObjectCodec codec = parser.getCodec();
			JsonNode node = codec.readTree(parser);
			LiteralRange range = LiteralRange.fromString(node.path("range").asText());
			
			if(range == null)
			{
				return codec.treeToValue(node, ClassSlot.class);
			}
			
			return codec.treeToValue(node, range.getSlotClass());
		}
	}
}
