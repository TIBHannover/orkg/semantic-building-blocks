package eu.tib.kgbbengine.domain.storage.slot;

import eu.tib.kgbbengine.domain.URI;

public abstract class NumberSlot<T extends Number & Comparable<T>> extends LiteralSlot<T>
{
	private final T min;
	private final T max;
	private final boolean minExclusive;
	private final boolean maxExclusive;
	
	public NumberSlot(URI slotUri, String ifabsent, boolean required, boolean multivalued, T min, T max, boolean minExclusive, boolean maxExclusive)
	{
		super(slotUri, ifabsent, required, multivalued);
		this.min = min;
		this.max = max;
		this.minExclusive = minExclusive;
		this.maxExclusive = maxExclusive;
	}
	
	public T getMin()
	{
		return this.min;
	}
	
	public T getMax()
	{
		return this.max;
	}
	
	public boolean isMinExclusive()
	{
		return this.minExclusive;
	}
	
	public boolean isMaxExclusive()
	{
		return this.maxExclusive;
	}
	
	@Override
	public void validate(T object) throws IllegalArgumentException
	{
		if(object == null)
		{
			throw new IllegalArgumentException("Input value is null");
		}
		
		if(this.getMin() != null)
		{
			int min = object.compareTo(this.getMin());
			
			if(min == 0 && this.isMinExclusive() || min < 0)
			{
				throw new IllegalArgumentException("Input value with " + object + " is less than minimum allowed value " + this.getMin() + (this.isMinExclusive() ? " (min exclusive)" : ""));
			}
		}
		
		if(this.getMax() != null)
		{
			int max = object.compareTo(this.getMax());
			
			if(max == 0 && this.isMaxExclusive() || max > 0)
			{
				throw new IllegalArgumentException("Input value with " + object + " is higher than maximum allowed value " + this.getMax() + (this.isMaxExclusive() ? " (max exclusive)" : ""));
			}
		}
	}
}
