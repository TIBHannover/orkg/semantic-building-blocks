package eu.tib.kgbbengine.domain.kgbb;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;

public class Association extends Interconnect
{
	@Nullable
	private final URI applySubjectRangeConstraintOn;
	
	public Association(URI targetKgbbInstance, boolean required, int quantity, @Nullable URI applySubjectRangeConstraintOn)
	{
		super(targetKgbbInstance, required, quantity);
		this.applySubjectRangeConstraintOn = applySubjectRangeConstraintOn;
	}
	
	@Override
	public URI resolveSubject(SemanticUnit unit)
	{
		return unit.getSubject();
	}
	
	@Override
	public boolean hasPositionConstraint(URI position)
	{
		if(this.applySubjectRangeConstraintOn == null)
		{
			return false;
		}
		
		return position.equals(this.applySubjectRangeConstraintOn);
	}
	
	public URI getApplySubjectRangeConstraintOn()
	{
		return this.applySubjectRangeConstraintOn;
	}
}