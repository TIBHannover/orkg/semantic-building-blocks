package eu.tib.kgbbengine.domain.storage.slot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class URISlot extends LiteralSlot<URI>
{
	public URISlot(@JsonProperty("slot_uri") URI slotUri, @JsonProperty("ifabsent") String ifabsent, @JsonProperty("required") boolean required, @JsonProperty("multivalued") boolean multivalued)
	{
		super(slotUri, ifabsent, required, multivalued);
	}
	
	@Override
	public void validate(URI input) throws IllegalArgumentException
	{
		
	}
	
	@Override
	public URI parse(String input) throws Exception
	{
		return new URI(input);
	}
}
