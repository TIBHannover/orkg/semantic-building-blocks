package eu.tib.kgbbengine.domain.storage.slot;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StringSlot extends LiteralSlot<String>
{
	private static final Pattern IFABSENT_PATTERN = Pattern.compile("string\\((.*)\\)");
	
	private final String pattern;
	
	public StringSlot(@JsonProperty("slot_uri") URI slotUri, @JsonProperty("ifabsent") String ifabsent, @JsonProperty("required") boolean required, @JsonProperty("name") boolean multivalued, @JsonProperty("pattern") String pattern)
	{
		super(slotUri, ifabsent, required, multivalued);
		this.pattern = pattern;
	}
	
	public String getPattern()
	{
		return this.pattern;
	}
	
	@Override
	public void validate(String input) throws IllegalArgumentException
	{
		if(this.pattern != null && !input.matches(this.pattern))
		{
			throw new IllegalArgumentException("Input value \"" + input + "\" does not match pattern \"" + this.pattern + "\"");
		}
	}
	
	@Override
	protected String unwrapIfAbsent(String ifabsent)
	{
		if(ifabsent == null)
		{
			return null;
		}
		
		Matcher matcher = IFABSENT_PATTERN.matcher(ifabsent);
		
		if(!matcher.find())
		{
			throw new IllegalArgumentException("ifabsent value \"" + ifabsent + "\" does not match patttern " + IFABSENT_PATTERN.pattern());
		}
		
		return matcher.group(1);
	}
	
	@Override
	public String parse(String input) throws Exception
	{
		return Objects.requireNonNull(input);
	}
}
