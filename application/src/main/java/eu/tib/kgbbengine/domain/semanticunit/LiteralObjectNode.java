package eu.tib.kgbbengine.domain.semanticunit;

import eu.tib.kgbbengine.domain.URI;

public class LiteralObjectNode extends ObjectNode
{
	private Object literal;
	
	public LiteralObjectNode(URI uri)
	{
		super(uri);
	}
	
	public Object getLiteral()
	{
		return this.literal;
	}
	
	public void setLiteral(Object literal)
	{
		this.literal = literal;
	}
	
	public LiteralObjectNode shallowCopy()
	{
		LiteralObjectNode copy = new LiteralObjectNode(this.getUri());
		copy.setInputTypeLabel(this.getInputTypeLabel());
		copy.setObjectPositionClass(this.getObjectPositionClass());
		copy.setCreator(this.getCreator());
		copy.setCreationDate(this.getCreationDate());
		copy.setCreatedWithApplication(this.getCreatedWithApplication());
		copy.setCurrentVersion(this.isCurrentVersion());
		copy.setLiteral(this.getLiteral());
		return copy;
	}
}