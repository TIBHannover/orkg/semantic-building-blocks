package eu.tib.kgbbengine.domain.semanticunit;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;

public class ResourceObjectNode extends ObjectNode
{
	private URI resourceURI;
	@Nullable
	private String resourceLabel;
	private URI constraint;
	
	public ResourceObjectNode(URI uri)
	{
		super(uri);
	}
	
	public URI getResourceURI()
	{
		return this.resourceURI;
	}
	
	public void setResourceURI(URI resourceURI)
	{
		this.resourceURI = resourceURI;
	}
	
	@Nullable
	public String getResourceLabel()
	{
		return this.resourceLabel;
	}
	
	public void setResourceLabel(String resourceLabel)
	{
		this.resourceLabel = resourceLabel;
	}
	
	public URI getConstraint()
	{
		return this.constraint;
	}
	
	public void setConstraint(URI constraint)
	{
		this.constraint = constraint;
	}
	
	public ResourceObjectNode shallowCopy()
	{
		ResourceObjectNode copy = new ResourceObjectNode(this.getUri());
		copy.setInputTypeLabel(this.getInputTypeLabel());
		copy.setObjectPositionClass(this.getObjectPositionClass());
		copy.setCreator(this.getCreator());
		copy.setCreationDate(this.getCreationDate());
		copy.setCreatedWithApplication(this.getCreatedWithApplication());
		copy.setCurrentVersion(this.isCurrentVersion());
		copy.setResourceURI(this.resourceURI);
		copy.setResourceLabel(this.resourceLabel);
		copy.setConstraint(this.constraint);
		return copy;
	}
}