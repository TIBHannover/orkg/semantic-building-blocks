package eu.tib.kgbbengine.domain.storage.slot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DecimalSlot extends NumberSlot<Double>
{
	public DecimalSlot(@JsonProperty("slot_uri") URI slotUri, @JsonProperty("ifabsent") String ifabsent, @JsonProperty("required") boolean required, @JsonProperty("multivalued") boolean multivalued, @JsonProperty("min") Double min, @JsonProperty("max") Double max, @JsonProperty("min_exclusive") boolean minExclusive, @JsonProperty("max_exclusive") boolean maxExclusive)
	{
		super(slotUri, ifabsent, required, multivalued, min, max, minExclusive, maxExclusive);
	}
	
	@Override
	public void validate(Double input) throws IllegalArgumentException
	{
		super.validate(input);
	}
	
	@Override
	public Double parse(String input) throws Exception
	{
		return Double.parseDouble(input);
	}
}
