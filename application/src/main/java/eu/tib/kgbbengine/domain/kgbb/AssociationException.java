package eu.tib.kgbbengine.domain.kgbb;

public class AssociationException extends Exception
{
	private static final long serialVersionUID = -2996078506859721884L;
	
	public AssociationException(String message)
	{
		super(message);
	}
}
