package eu.tib.kgbbengine.domain.storage.slot;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DateSlot extends LiteralSlot<LocalDateTime>
{
	public DateSlot(@JsonProperty("slot_uri") URI slotUri, @JsonProperty("ifabsent") String ifabsent, @JsonProperty("required") boolean required, @JsonProperty("multivalued") boolean multivalued)
	{
		super(slotUri, ifabsent, required, multivalued);
	}
	
	@Override
	public void validate(LocalDateTime input) throws IllegalArgumentException
	{
		
	}
	
	@Override
	public LocalDateTime parse(String input) throws Exception
	{
		return LocalDateTime.parse(input);
	}
}
