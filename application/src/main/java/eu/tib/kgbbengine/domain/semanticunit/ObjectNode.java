package eu.tib.kgbbengine.domain.semanticunit;

import java.time.LocalDateTime;

import eu.tib.kgbbengine.domain.URI;

public abstract class ObjectNode
{
	private final URI uri;
	private String inputTypeLabel;
	private URI objectPositionClass; //rdf:type
	private URI creator;
	private LocalDateTime creationDate;
	private URI createdWithApplication;
	private boolean currentVersion = true;
	
	//optional:
	//importedFrom (upri of external dataset)
	//versionID (upri)
	//datasetUnitID (upri)
	
	public ObjectNode(URI uri)
	{
		this.uri = uri;
	}
	
	public URI getUri()
	{
		return this.uri;
	}
	
	public String getInputTypeLabel()
	{
		return this.inputTypeLabel;
	}
	
	public void setInputTypeLabel(String inputTypeLabel)
	{
		this.inputTypeLabel = inputTypeLabel;
	}
	
	public URI getObjectPositionClass()
	{
		return this.objectPositionClass;
	}
	
	public void setObjectPositionClass(URI objectPositionClass)
	{
		this.objectPositionClass = objectPositionClass;
	}
	
	public URI getCreator()
	{
		return this.creator;
	}
	
	public void setCreator(URI creator)
	{
		this.creator = creator;
	}
	
	public LocalDateTime getCreationDate()
	{
		return this.creationDate;
	}
	
	public void setCreationDate(LocalDateTime creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public URI getCreatedWithApplication()
	{
		return this.createdWithApplication;
	}
	
	public void setCreatedWithApplication(URI createdWithApplication)
	{
		this.createdWithApplication = createdWithApplication;
	}
	
	public boolean isCurrentVersion()
	{
		return this.currentVersion;
	}
	
	public void setCurrentVersion(boolean currentVersion)
	{
		this.currentVersion = currentVersion;
	}
}