package eu.tib.kgbbengine.domain.kgbb;

import java.util.Set;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;

public class KGBBInstance
{
	private final URI uri;
	private final URI kgbb;
	@Nullable
	private final Set<Association> associations;
	private final Set<Link> links;
	
	public KGBBInstance(URI uri, URI kgbb, Set<Association> associations, Set<Link> links)
	{
		this.uri = uri;
		this.kgbb = kgbb;
		this.associations = associations;
		this.links = links;
	}
	
	public Interconnect findInterconnect(URI targetKgbbiUri) throws AssociationException
	{
		for(Association association : this.getAssociations())
		{
			if(association.getTargetKgbbi().equals(targetKgbbiUri))
			{
				return association;
			}
		}
		
		for(Link link : this.getLinks())
		{
			if(link.getTargetKgbbi().equals(targetKgbbiUri))
			{
				return link;
			}
		}
		
		throw new AssociationException("Could not find requested association with target " + targetKgbbiUri + " for KGBB instance " + this.uri);
	}
	
	public URI getUri()
	{
		return this.uri;
	}
	
	public URI getKgbb()
	{
		return this.kgbb;
	}
	
	public Set<Association> getAssociations()
	{
		return this.associations;
	}
	
	public Set<Link> getLinks()
	{
		return this.links;
	}
}
