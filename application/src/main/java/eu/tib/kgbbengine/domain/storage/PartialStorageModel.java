package eu.tib.kgbbengine.domain.storage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;

import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Util;

public class PartialStorageModel
{
	private final Map<String, String> prefixes;
	private final Map<String, ClassModel> classes;
	private final Map<String, Slot<?>> slots;
	private final List<String> imports;
	
	public PartialStorageModel(@JsonProperty("prefixes") Map<String, String> prefixes, @JsonProperty("classes") Map<String, ClassModel> classes, @JsonProperty("slots") Map<String, Slot<?>> slots, @JsonProperty("imports") List<String> imports)
	{
		this.prefixes = prefixes;
		this.classes = classes;
		this.slots = slots;
		this.imports = imports;
	}
	
	public Map<String, String> getPrefixes()
	{
		return this.prefixes;
	}
	
	public Map<String, ClassModel> getClasses()
	{
		return this.classes;
	}
	
	public Map<String, Slot<?>> getSlots()
	{
		return this.slots;
	}
	
	public List<String> getImports()
	{
		return this.imports;
	}
	
	public static PartialStorageModel fromFile(File file) throws StreamReadException, DatabindException, IOException
	{
		return Util.YAML_MAPPER.readValue(Files.readAllBytes(file.toPath()), PartialStorageModel.class);
	}
}
