package eu.tib.kgbbengine.domain.storage.slot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IntegerSlot extends NumberSlot<Integer>
{
	public IntegerSlot(@JsonProperty("slot_uri") URI slotUri, @JsonProperty("ifabsent") String ifabsent, @JsonProperty("required") boolean required, @JsonProperty("multivalued") boolean multivalued, @JsonProperty("min") Integer min, @JsonProperty("max") Integer max, @JsonProperty("min_exclusive") boolean minExclusive, @JsonProperty("max_exclusive") boolean maxExclusive)
	{
		super(slotUri, ifabsent, required, multivalued, min, max, minExclusive, maxExclusive);
	}
	
	@Override
	public void validate(Integer input) throws IllegalArgumentException
	{
		super.validate(input);
	}
	
	@Override
	public Integer parse(String input) throws Exception
	{
		return Integer.parseInt(input);
	}
}
