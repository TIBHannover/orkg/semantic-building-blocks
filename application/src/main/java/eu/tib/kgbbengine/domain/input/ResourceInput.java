package eu.tib.kgbbengine.domain.input;

import eu.tib.kgbbengine.domain.URI;

public class ResourceInput
{
	private final URI uri;
	private final String label;
	private final ResourceType type;
	
	public ResourceInput(URI uri, String label, ResourceType type)
	{
		this.uri = uri;
		this.label = label;
		this.type = type;
	}
	
	public URI getUri()
	{
		return this.uri;
	}
	
	public String getLabel()
	{
		return this.label;
	}
	
	public ResourceType getType()
	{
		return this.type;
	}
	
	public static enum ResourceType
	{
		CLASS,
		INSTANCE;
	}
}