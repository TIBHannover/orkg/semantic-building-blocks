package eu.tib.kgbbengine.domain.kgbb;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;

public class KGBB
{
	@Nullable
	private final URI parent;
	private final URI uri;
	@Nullable
	private final URI storageModel;
	@Nullable
	private final URI subjectModel;
	private final List<URI> objectPositionClasses;
	
	//access template
	//display template
	//import template
	
	public KGBB(URI uri, @Nullable URI storageModel, @Nullable URI subjectModel, List<URI> objectPositionClasses)
	{
		this((URI) null, uri, storageModel, subjectModel, objectPositionClasses);
	}
	
	public KGBB(@Nullable KGBB parent, URI uri, @Nullable URI storageModel, @Nullable URI subjectModel, List<URI> objectPositionClasses)
	{
		this(parent != null ? parent.getUri() : null, uri, storageModel, subjectModel, objectPositionClasses);
	}
	
	public KGBB(@Nullable URI parent, URI uri, @Nullable URI storageModel, @Nullable URI subjectModel, List<URI> objectPositionClasses)
	{
		this.parent = parent;
		this.uri = uri;
		this.storageModel = storageModel;
		this.subjectModel = subjectModel;
		this.objectPositionClasses = objectPositionClasses;
	}
	
	@Nullable
	public URI getParent()
	{
		return this.parent;
	}
	
	public URI getUri()
	{
		return this.uri;
	}
	
	@Nullable
	public URI getStorageModel()
	{
		return this.storageModel;
	}
	
	@Nullable
	public URI getSubjectModel()
	{
		return this.subjectModel;
	}
	
	public List<URI> getObjectPositionClasses()
	{
		return this.objectPositionClasses;
	}
}
