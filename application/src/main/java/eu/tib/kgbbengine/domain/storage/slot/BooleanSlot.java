package eu.tib.kgbbengine.domain.storage.slot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BooleanSlot extends LiteralSlot<Boolean>
{
	public BooleanSlot(@JsonProperty("slot_uri") URI slotUri, @JsonProperty("ifabsent") String ifabsent, @JsonProperty("required") boolean required, @JsonProperty("multivalued") boolean multivalued)
	{
		super(slotUri, ifabsent, required, multivalued);
	}
	
	@Override
	public void validate(Boolean input) throws IllegalArgumentException
	{
		
	}
	
	@Override
	public Boolean parse(String input) throws Exception
	{
		if(input.equalsIgnoreCase("true"))
		{
			return true;
		}
		else if(input.equalsIgnoreCase("false"))
		{
			return false;
		}
		
		throw new IllegalArgumentException();
	}
}
