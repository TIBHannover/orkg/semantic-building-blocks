package eu.tib.kgbbengine.domain.storage;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.KGBB;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel.LinearStorageModelBuilder;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.spi.KGBBRepository;
import eu.tib.kgbbengine.spi.StorageModelRepository;

public class StorageModelManager
{
	private static final String PREFIX = "http://orkg.org/";
	
	private final KGBBRepository kgbbRepo;
	private final StorageModelRepository storageModelRepo;
	private final Map<URI, SemanticUnitStorageModel> storageModelCache = new HashMap<URI, SemanticUnitStorageModel>();
	private final Map<URI, LinearStorageModel> positionStorageModelCache = new HashMap<URI, LinearStorageModel>();
	
	public StorageModelManager(StorageModelRepository storageModelRepo, KGBBRepository kgbbRepo)
	{
		this.storageModelRepo = storageModelRepo;
		this.kgbbRepo = kgbbRepo;
	}
	
	@Nullable
	private URI findSemanticUnitStorageModelUri(KGBB kgbb)
	{
		URI model = kgbb.getStorageModel();
		
		while(model == null && kgbb.getParent() != null)
		{
			kgbb = this.kgbbRepo.findById(kgbb.getParent());
			model = kgbb.getStorageModel();
		}
		
		return model;
	}
	
	@Nullable
	private URI findSubjectStorageModelUri(KGBB kgbb)
	{
		URI model = kgbb.getSubjectModel();
		
		while(model == null && kgbb.getParent() != null)
		{
			kgbb = this.kgbbRepo.findById(kgbb.getParent());
			model = kgbb.getSubjectModel();
		}
		
		return model;
	}
	
	private void buildLinearStorageModel(LinearStorageModelBuilder builder, URI uri, Set<URI> imported)
	{
		PartialStorageModel partial = this.storageModelRepo.findById(uri);
		
		if(partial.getImports() != null)
		{
			for(String importedModel : partial.getImports())
			{
				URI importedUri = this.resolveImport(importedModel, partial.getPrefixes());
				
				if(!imported.contains(importedUri))
				{
					imported.add(importedUri);
					this.buildLinearStorageModel(builder, importedUri, imported);
				}
			}
		}
		
		builder.expand(partial);
	}
	
	private URI resolveImport(String uri, Map<String, String> prefixes)
	{
		Matcher matcher = Constants.PREFIX_PATTERN.matcher(uri);
		
		if(matcher.matches())
		{
			String prefix = matcher.group(1);
			
			if(prefixes.containsKey(prefix))
			{
				return this.validateImport(prefixes.get(prefix) + matcher.group(2));
			}
			
			throw new IllegalStateException("Unable to resolve prefix " + prefix);
		}
		
		return this.validateImport(uri);
	}
	
	private URI validateImport(String uri)
	{
		if(!uri.startsWith(PREFIX))
		{
			throw new IllegalArgumentException("Imports are only supported for " + PREFIX);
		}
		
		return new URI(uri.substring(PREFIX.length()));
	}
	
	private LinearStorageModel buildStorageModel(URI uri)
	{
		LinearStorageModelBuilder storageModelBuilder = LinearStorageModel.builder();
		this.buildLinearStorageModel(storageModelBuilder, uri, new HashSet<URI>());
		return storageModelBuilder.build(uri.toString());
	}
	
	private SemanticUnitStorageModel buildSemanticUnitStorageModel(KGBB kgbb)
	{
		URI storageModelUri = this.findSemanticUnitStorageModelUri(kgbb);
		LinearStorageModel storageModel = this.buildStorageModel(storageModelUri);
		URI subjectModelUri = this.findSubjectStorageModelUri(kgbb);
		
		if(subjectModelUri != null)
		{
			LinearStorageModel subjectModel = this.buildStorageModel(subjectModelUri);
			Map<URI, LinearStorageModel> objectStorageModels = new HashMap<URI, LinearStorageModel>();
			
			for(URI objectPoisition : kgbb.getObjectPositionClasses())
			{
				objectStorageModels.put(objectPoisition, this.buildStorageModel(objectPoisition));
			}
			
			return new SemanticUnitStorageModel(storageModel, subjectModel, objectStorageModels);
		}
		
		return new SemanticUnitStorageModel(storageModel, LinearStorageModel.EMPTY, Collections.emptyMap()); 
	}
	
	public SemanticUnitStorageModel getStorageModel(URI kgbb)
	{
		return this.storageModelCache.computeIfAbsent(kgbb, key -> this.buildSemanticUnitStorageModel(this.kgbbRepo.findById(kgbb)));
	}
	
	public LinearStorageModel getObjectStorageModel(URI positionUri)
	{
		return this.positionStorageModelCache.computeIfAbsent(positionUri, key -> this.buildStorageModel(positionUri));
	}
}
