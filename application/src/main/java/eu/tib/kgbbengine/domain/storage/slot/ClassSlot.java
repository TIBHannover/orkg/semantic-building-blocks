package eu.tib.kgbbengine.domain.storage.slot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClassSlot extends Slot<URI>
{
	private final String range;
	
	public ClassSlot(@JsonProperty("slot_uri") URI slotUri, @JsonProperty("range") String range, @JsonProperty("required") boolean required, @JsonProperty("multivalued") boolean multivalued)
	{
		super(slotUri, required, multivalued);
		this.range = range;
	}
	
	@Override
	public void validate(URI input) throws IllegalArgumentException
	{
		
	}
	
	@Override
	public boolean isClassSlot()
	{
		return true;
	}
	
	public String getRange()
	{
		return this.range;
	}
	
	@Override
	public URI parse(String input) throws Exception
	{
		return new URI(input);
	}
}
