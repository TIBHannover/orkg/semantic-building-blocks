package eu.tib.kgbbengine.domain.storage;

import java.util.Map;

import eu.tib.kgbbengine.domain.URI;

public class SemanticUnitStorageModel
{
	private final LinearStorageModel semanticUnitStorageModel;
	private final LinearStorageModel subjectStorageModel;
	private final Map<URI, LinearStorageModel> objectStorageModels;
	
	public SemanticUnitStorageModel(LinearStorageModel semanticUnitStorageModel, LinearStorageModel subjectStorageModel, Map<URI, LinearStorageModel> objectStorageModels)
	{
		this.semanticUnitStorageModel = semanticUnitStorageModel;
		this.subjectStorageModel = subjectStorageModel;
		this.objectStorageModels = objectStorageModels;
	}
	
	public LinearStorageModel getSemanticUnitStorageModel()
	{
		return this.semanticUnitStorageModel;
	}
	
	public LinearStorageModel getSubjectStorageModel()
	{
		return this.subjectStorageModel;
	}
	
	public Map<URI, LinearStorageModel> getObjectStorageModels()
	{
		return this.objectStorageModels;
	}
}
