package eu.tib.kgbbengine.domain.util;

import java.util.regex.Pattern;

import eu.tib.kgbbengine.domain.URI;

public class Constants
{
	public static final URI APPLICATION_URI = new URI("KGBBEngine");
	public static final URI NIIU_KGBB_URI = new URI("NamedIndividualIdentificationKGBB");
	public static final URI NIIU_KGBBI_URI = new URI("NamedIndividualIdentificationKGBBInstance");
	public static final URI NIIU_RESOURCE_POSITION_URI = new URI("NamedIndividualIdentificationUnitResourceObjectPosition");
	public static final URI NIIU_LITERAL_POSITION_URI = new URI("NamedIndividualIdentificationUnitLiteralObjectPosition");
	
	public static final String STORAGE_TEMPLATE_SLOT_LITERAL = "literal";
	public static final String STORAGE_TEMPLATE_SLOT_RESOURCE_URI = "resourceURI";
	public static final String STORAGE_TEMPLATE_SLOT_RESOURCE_LABEL = "resourceLabel";
	public static final String STORAGE_TEMPLATE_SLOT_LABEL = "label";
	public static final String STORAGE_TEMPLATE_SLOT_INPUT_TYPE_LABEL = "inputTypeLabel";
	public static final String STORAGE_TEMPLATE_SLOT_DESCRIPTION = "description";
	public static final String STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT = "hasSemanticUnitSubject";
	public static final String STORAGE_TEMPLATE_SLOT_HAS_ASSOCIATED_SEMANTIC_UNIT = "hasAssociatedSemanticUnit";
	public static final String STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNITS_GRAPH = "hasSemanticUnitsGraph";
	public static final String STORAGE_TEMPLATE_SLOT_TYPE = "type";
	public static final String STORAGE_TEMPLATE_SLOT_OBJECT_DESCRIBED_BY_SEMANTIC_UNIT = "objectDescribedBySemanticUnit";
	public static final String STORAGE_TEMPLATE_SLOT_HAS_LINKED_SEMANTIC_UNIT = "hasLinkedSemanticUnit";
	public static final String STORAGE_TEMPLATE_SLOT_KGBB_URI = "KGBB_URI";
	public static final String STORAGE_TEMPLATE_SLOT_CURRENT_VERSION = "currentVersion";
	public static final String STORAGE_TEMPLATE_SLOT_CREATOR = "creator";
	public static final String STORAGE_TEMPLATE_SLOT_CREATION_DATE = "creationDate";
	public static final String STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION = "createdWithApplication";
	public static final String STORAGE_TEMPLATE_SLOT_HAS_CONSTRAINT = "hasConstraint";
	public static final String STORAGE_TEMPLATE_SLOT_DELETED_BY = "deletedBy";
	public static final String STORAGE_TEMPLATE_SLOT_DELETION_DATE = "deletionDate";
	
	public static final Pattern PREFIX_PATTERN = Pattern.compile("^([^:]+):(.*)");
	
	public static final URI RESOURCE_OBJECT_POSITION = new URI("ResourceObjectPosition");
	public static final URI KGBB = new URI("KGBB");
}
