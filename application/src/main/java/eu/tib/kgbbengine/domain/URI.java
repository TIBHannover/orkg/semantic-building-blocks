package eu.tib.kgbbengine.domain;

import java.util.Objects;

public class URI implements Comparable<URI>
{
	private final String uri;
	
	public URI(String uri)
	{
		this.uri = Objects.requireNonNull(uri);
	}
	
	@Override
	public int hashCode()
	{
		return this.uri.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj != null && obj instanceof URI other)
		{
			return Objects.equals(this.uri, other.uri);
		}
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return this.uri;
	}
	
	@Override
	public int compareTo(URI other)
	{
		return this.uri.compareTo(other.uri);
	}
}
