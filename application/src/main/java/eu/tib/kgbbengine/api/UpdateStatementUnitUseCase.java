package eu.tib.kgbbengine.api;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.kgbb.AssociationException;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.StorageModelException;
import eu.tib.kgbbengine.spi.OntologyServiceException;

public interface UpdateStatementUnitUseCase
{
	SemanticUnit updateStatementUnit(URI uri, URI contributor, Input inputs, @Nullable URI itemUnit) throws IllegalArgumentException, StorageModelException, AssociationException, OntologyServiceException;
}
