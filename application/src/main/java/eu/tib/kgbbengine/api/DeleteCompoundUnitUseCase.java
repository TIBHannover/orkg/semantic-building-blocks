package eu.tib.kgbbengine.api;

import eu.tib.kgbbengine.domain.URI;

public interface DeleteCompoundUnitUseCase
{
	void deleteCompoundUnit(URI upri, URI contributor);
}
