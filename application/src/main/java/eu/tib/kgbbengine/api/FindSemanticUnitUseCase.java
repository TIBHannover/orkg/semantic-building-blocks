package eu.tib.kgbbengine.api;

import java.util.Set;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;

public interface FindSemanticUnitUseCase
{
	SemanticUnit findSemanticUnit(URI uri);
	
	Set<URI> findAll(URI kgbb);
	
	SemanticUnit findBySubject(URI subject, URI kgbb);
}
