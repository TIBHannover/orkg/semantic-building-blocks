package eu.tib.kgbbengine.api;

import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;

import org.jetbrains.annotations.Nullable;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.kgbb.AssociationException;
import eu.tib.kgbbengine.domain.storage.StorageModelException;
import eu.tib.kgbbengine.spi.OntologyServiceException;

public interface CreateCompoundUnitUseCase
{
	CompoundUnitResult createCompoundUnit(URI kgbbiUri, URI contributor, Input inputs, @Nullable URI subjectUnitUri) throws IllegalArgumentException, AssociationException, StorageModelException, OntologyServiceException;
}
