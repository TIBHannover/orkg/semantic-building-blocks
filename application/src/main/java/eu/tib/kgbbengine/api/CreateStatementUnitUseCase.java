package eu.tib.kgbbengine.api;

import eu.tib.kgbbengine.domain.KGBBEngine.StatementUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.kgbb.AssociationException;
import eu.tib.kgbbengine.domain.storage.StorageModelException;
import eu.tib.kgbbengine.spi.OntologyServiceException;

public interface CreateStatementUnitUseCase
{
	StatementUnitResult createStatementUnit(URI kgbbiUri, URI contributor, Input inputs, URI compoundUri) throws IllegalArgumentException, AssociationException, StorageModelException, OntologyServiceException;
}
