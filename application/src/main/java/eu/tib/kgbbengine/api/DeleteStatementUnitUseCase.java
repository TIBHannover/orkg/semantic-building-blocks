package eu.tib.kgbbengine.api;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.AssociationException;

public interface DeleteStatementUnitUseCase
{
	void deleteStatementUnit(URI upri, URI contributor, URI itemUnitUpri) throws AssociationException;
}
