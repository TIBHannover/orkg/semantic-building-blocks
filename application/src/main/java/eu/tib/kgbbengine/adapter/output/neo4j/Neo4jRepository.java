package eu.tib.kgbbengine.adapter.output.neo4j;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.jetbrains.annotations.TestOnly;
import org.neo4j.cypherdsl.core.Cypher;
import org.neo4j.cypherdsl.core.Node;
import org.neo4j.cypherdsl.core.Operation;
import org.neo4j.cypherdsl.core.Relationship;
import org.neo4j.cypherdsl.core.ResultStatement;
import org.neo4j.cypherdsl.core.Statement;
import org.neo4j.cypherdsl.core.executables.ExecutableStatement;
import org.neo4j.cypherdsl.core.utils.Strings;
import org.neo4j.driver.AuthToken;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Record;
import org.neo4j.driver.Value;
import org.springframework.data.neo4j.core.Neo4jClient;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.domain.storage.SemanticUnitStorageModel;
import eu.tib.kgbbengine.domain.storage.StorageModelManager;
import eu.tib.kgbbengine.domain.storage.slot.ClassSlot;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.domain.util.Either;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;
import eu.tib.kgbbengine.spi.ValueProvider;

public class Neo4jRepository implements SemanticUnitRepository
{
	private static final SemanticUnitMapper SEMANTIC_UNIT_MAPPER = new SemanticUnitMapper();
	private static final ResourceNodeMapper RESOURCE_NODE_MAPPER = new ResourceNodeMapper();
	private static final LiteralNodeMapper LITERAL_NODE_MAPPER = new LiteralNodeMapper();
	private static final String SEMANTIC_UNIT_LABEL = "SemanticUnit";
	private static final String RESOURCE_OBJECT_LABEL = "ResourceObject";
	private static final String LITERAL_OBJECT_LABEL = "LiteralObject";
	private static final String SUBJECT_LABEL = "Subject";
	private static final String NODE_WITH_URI_LABEL = "URI_Holder";
	
    private final Neo4jClient client;
	private final LinearStorageModel genericSemanticUnitModel;
	private final LinearStorageModel genericResourceObjectModel;
    
	public Neo4jRepository(String uri, String user, String password, LinearStorageModel genericSemanticUnitModel, LinearStorageModel genericResourceObjectModel)
	{
		this(uri, AuthTokens.basic(user, password), genericSemanticUnitModel, genericResourceObjectModel);
	}
	
	public Neo4jRepository(String uri, AuthToken authToken, LinearStorageModel genericSemanticUnitModel, LinearStorageModel genericResourceObjectModel)
	{
		this.client = Neo4jClient.create(GraphDatabase.driver(uri, authToken));
		this.genericSemanticUnitModel = genericSemanticUnitModel;
		this.genericResourceObjectModel = genericResourceObjectModel;
	}
	
	@Override
	public void save(SemanticUnit unit)
	{
		SemanticUnitStorageModel model = unit.getStorageModel();
		LinearStorageModel subjectModel = model.getSubjectStorageModel();
		LinearStorageModel suModel = model.getSemanticUnitStorageModel();
		//Using StringBuilder to create cypher query here because Cypher-DSL does not support prepending query parts
		StringBuilder builder = new StringBuilder();
		Map<String, Object> bindings = new HashMap<String, Object>();
		Map<String, Set<Relation>> links = new HashMap<String, Set<Relation>>();
		Map<URI, Set<URI>> nodes = new HashMap<URI, Set<URI>>();
		Map<URI, String> vars = new HashMap<URI, String>();
		bindings.put("SemanticUnitURI", unit.getUri().toString());
		
		this.appendModelMerge(unit.getUri(), unit, SEMANTIC_UNIT_MAPPER, SEMANTIC_UNIT_LABEL, suModel, builder, links, vars, bindings);
		
		for(ResourceObjectNode object : unit.getResourceObjects())
		{
			nodes.computeIfAbsent(object.getObjectPositionClass(), key -> new HashSet<URI>()).add(object.getUri());
			LinearStorageModel positionModel = model.getObjectStorageModels().get(object.getObjectPositionClass());
			this.appendModelMerge(object.getUri(), object, RESOURCE_NODE_MAPPER, RESOURCE_OBJECT_LABEL, positionModel, builder, links, vars, bindings);
		}
		
		for(LiteralObjectNode object : unit.getLiteralObjects())
		{
			nodes.computeIfAbsent(object.getObjectPositionClass(), key -> new HashSet<URI>()).add(object.getUri());
			LinearStorageModel positionModel = model.getObjectStorageModels().get(object.getObjectPositionClass());
			this.appendModelMerge(object.getUri(), object, LITERAL_NODE_MAPPER, LITERAL_OBJECT_LABEL, positionModel, builder, links, vars, bindings);
		}
		
		String subjectIdentifier = Strings.randomIdentifier(8);
		vars.put(unit.getSubject(), subjectIdentifier);
		bindings.put(subjectIdentifier + "_URI", unit.getSubject().toString());
		//Not using Cypher-DSL here because it does not support setting labels
		builder.append("MERGE (" + subjectIdentifier + ":" + NODE_WITH_URI_LABEL + "{URI: $" + subjectIdentifier + "_URI}) SET " + subjectIdentifier + ":" + SUBJECT_LABEL + "\n");
		
		if(!subjectModel.isEmpty())
		{
			for(Entry<String, Slot<?>> slot : subjectModel.getSlots().entrySet())
			{
				if(!slot.getValue().isClassSlot())
				{
					continue;
				}
				
				ClassSlot classSlot = (ClassSlot) slot.getValue();
				Set<URI> targets = nodes.get(new URI(classSlot.getRange()));
				
				if(targets != null)
				{
					String relationName = subjectModel.resolveSlotURI(slot.getValue(), false).orElse(slot.getKey());
					Relation relation = new Relation(relationName, targets.stream().collect(Collectors.toMap(Function.identity(), key -> unit.isCurrentVersion())));
					Set<Relation> relations = links.computeIfAbsent(subjectIdentifier, key -> new HashSet<Relation>()); 
					relations.add(relation);
				}
			}
		}
		
		for(Entry<String, Set<Relation>> entry : links.entrySet())
		{
			Node from = Cypher.anyNode(entry.getKey());
			
			for(Relation relation : entry.getValue())
			{
				for(Entry<URI, Boolean> targetEntry : relation.targetNodes().entrySet())
				{
					Node to;
					URI targetUri = targetEntry.getKey();
					
					if(vars.containsKey(targetUri))
					{
						to = Cypher.anyNode(vars.get(targetUri));
					}
					else
					{
						String identifier = Strings.randomIdentifier(8);
						//Not using Cypher-DSL here because it does not support generating match queries without return
						builder.insert(0, "MERGE (" + identifier + ":" + NODE_WITH_URI_LABEL + " {URI: $" + identifier + "_URI})\n");
						bindings.put(identifier + "_URI", targetUri.toString());
						to = Cypher.anyNode(identifier);
					}
					
					Relationship r = from.relationshipTo(to, relation.relationName())
							.withProperties(Map.of("SemanticUnitURI", unit.getUri().toString()));
					Operation setCurrentVersion = r.property("currentVersion").to(Cypher.literalOf(targetEntry.getValue()));
					
					builder.append(Cypher.merge(r).set(setCurrentVersion).build().getCypher() + "\n");
				}
			}
		}
		
		this.client.query(builder.toString()).bindAll(bindings).run();
	}
	
	private <T> void appendModelMerge(URI uri, T object, ValueProvider<T> provider, String marker, LinearStorageModel model, StringBuilder builder, Map<String, Set<Relation>> links, Map<URI, String> vars, Map<String, Object> bindings)
	{
		String identifier = Strings.randomIdentifier(8);
		vars.put(uri, identifier);
		bindings.put(identifier + "_URI", uri.toString());
		Set<String> properties = new HashSet<String>();
		properties.add(identifier + ":" + model.getName());
		properties.add(identifier + ":" + marker);
		
		for(Entry<String, Slot<?>> slot : model.getSlots().entrySet())
		{
			Either<Object, Map<URI, Boolean>> value = provider.provide(slot.getKey(), slot.getValue(), object);
			
			if(value == null)
			{
				throw new IllegalStateException("No value provider for slot " + slot.getKey());
			}
			else if(value.isLeft())
			{
				if(value.left().isPresent())
				{
					String key = model.resolveSlotURI(slot.getValue(), false).orElse(slot.getKey());
					properties.add(identifier + ".`" + key + "` = $" + identifier + "_" + key);
					bindings.put(identifier + "_" + key, value.left().get());
				}
			}
			else if(value.isRight())
			{
				Relation relation = new Relation(model.resolveSlotURI(slot.getValue(), false).orElse(slot.getKey()), value.right().get());
				Set<Relation> relations = links.computeIfAbsent(identifier, key -> new HashSet<Relation>());
				relations.add(relation);
			}
		}
		
		builder.append("MERGE (" + identifier + ":" + NODE_WITH_URI_LABEL + "{URI: $" + identifier + "_URI}) SET " + properties.stream().collect(Collectors.joining(", ")) + "\n");
	}
	
	@Override
	public SemanticUnit findByUpri(URI upri, SemanticUnitStorageModel model)
	{
		Node aNode = Cypher.anyNode().named("a");
		Node bNode = Cypher.anyNode().named("b");
		Relationship rNode = aNode.relationshipTo(bNode).named("r").withProperties(Map.of("SemanticUnitURI", upri.toString()));
		ResultStatement match = Cypher.match(rNode).returning(aNode, rNode, bNode).build();
		List<Record> records = ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner());
		
		if(records.isEmpty())
		{
			throw new NoSuchElementException("Tried to find missing semantic unit " + upri);
		}
		
		SemanticUnit unit = new SemanticUnit(model);
		LinearStorageModel suModel = model.getSemanticUnitStorageModel();
		
		for(Record record : records)
		{
			var a = record.get("a").asNode();
			var r = record.get("r").asRelationship();
			var b = record.get("b").asNode();
			
			if(a.hasLabel(SUBJECT_LABEL))
			{
				for(Entry<URI, LinearStorageModel> objectEntry : model.getObjectStorageModels().entrySet())
				{
					if(b.hasLabel(objectEntry.getValue().getName()))
					{
						if(b.hasLabel(RESOURCE_OBJECT_LABEL))
						{
							ResourceObjectNode node = new ResourceObjectNode(new URI(b.get("URI").asString()));
							
							for(Entry<String, Slot<?>> entry : objectEntry.getValue().getSlots().entrySet())
							{
								String slotUri = objectEntry.getValue().resolveSlotURI(entry.getValue(), false).orElse(entry.getKey());
								Value value = b.get(slotUri);
								
								if(!value.isNull())
								{
									RESOURCE_NODE_MAPPER.parse(entry.getKey(), value, true, node, entry.getValue());
								}
							}
							
							unit.addResourceObject(node);
						}
						else if(b.hasLabel(LITERAL_OBJECT_LABEL))
						{
							LiteralObjectNode node = new LiteralObjectNode(new URI(b.get("URI").asString()));
							
							for(Entry<String, Slot<?>> entry : objectEntry.getValue().getSlots().entrySet())
							{
								String slotUri = objectEntry.getValue().resolveSlotURI(entry.getValue(), false).orElse(entry.getKey());
								Value value = b.get(slotUri);
								
								if(!value.isNull())
								{
									LITERAL_NODE_MAPPER.parse(entry.getKey(), value, true, node, entry.getValue());
								}
							}
							
							unit.addLiteralObject(node);
						}
						
						break;
					}
				}
				
				unit.setSubject(new URI(a.get("URI").asString()));
			}
			else if(a.hasLabel(suModel.getName()))
			{
				for(Entry<String, Slot<?>> entry : suModel.getSlots().entrySet())
				{
					String slotUri = suModel.resolveSlotURI(entry.getValue(), false).orElse(entry.getKey());
					Value value = a.get(slotUri);
					
					if(!value.isNull())
					{
						SEMANTIC_UNIT_MAPPER.parse(entry.getKey(), value, true, unit, entry.getValue());
					}
					else if(r.type().equals(slotUri))
					{
						SEMANTIC_UNIT_MAPPER.parse(entry.getKey(), b.get("URI"), r.get("currentVersion").asBoolean(true), unit, entry.getValue());
					}
				}
				
				unit.setUri(new URI(a.get("URI").asString()));
			}
		}
		
		return unit;
	}
	
	@Override
	public SemanticUnit findBySubject(URI subject, URI kgbb, SemanticUnitStorageModel model)
	{
		LinearStorageModel suModel = model.getSemanticUnitStorageModel();
		String slotUri = suModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT, false).get();
		Node unit = Cypher.node(suModel.getName()).named("unit");
		Node subj = Cypher.node(SUBJECT_LABEL).named("subject").withProperties(Map.of("URI", subject.toString()));
		Relationship relation = unit.relationshipTo(subj, slotUri);
		ResultStatement match = Cypher.match(relation).returning(relation.property("SemanticUnitURI").as("uri")).build();
		List<Record> records = ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner());
		
		if(records.isEmpty())
		{
			throw new NoSuchElementException("Tried to find missing semantic unit with subject " + subject);
		}
		
		if(records.size() > 1)
		{
			throw new IllegalStateException("Storage contains multiple semantic units with type " + suModel.getName() + " for subject " + subject);
		}
		
		return this.findByUpri(new URI(records.get(0).get("uri").asString()), model);
	}
	
	@Override
	public URI findKggbi(URI upri)
	{
		String slotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_KGBB_URI, false).get();
		Node unit = Cypher.node(SEMANTIC_UNIT_LABEL, NODE_WITH_URI_LABEL).named("unit").withProperties(Map.of("URI", upri.toString()));
		ResultStatement match = Cypher.match(unit).returning(unit.property(slotUri).as("kgbbi")).build();
		List<Record> records = ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner());
		
		if(records.isEmpty())
		{
			throw new NoSuchElementException("Tried to find missing semantic unit " + upri);
		}
		
		return new URI(records.get(0).get("kgbbi").asString());
	}
	
	@Override
	public boolean isSubjectUsedAsObject(URI uri)
	{
		String resourceUriSlotUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI, false).get();
		Node node = Cypher.node(RESOURCE_OBJECT_LABEL).named("node").withProperties(Map.of(resourceUriSlotUri, uri.toString(), "currentVersion", true));
		ResultStatement match = Cypher.match(node).returning(node.property("URI")).build();
		return !ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner()).isEmpty();
	}
	
	@Override
	public boolean isSubjectUsedByOtherUnit(URI subject, URI uri)
	{
		String subjectSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT, false).get();
		String typeSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, false).get();
		Node unit = Cypher.node(SEMANTIC_UNIT_LABEL).named("unit").withProperties(Map.of("currentVersion", true));
		Node subjectNode = Cypher.node(SUBJECT_LABEL).named("subject").withProperties(Map.of("URI", subject.toString()));
		Relationship relation = unit.relationshipTo(subjectNode, subjectSlotUri).withProperties(Map.of("currentVersion", true));
		ResultStatement match = Cypher.match(relation)
				.where(unit.property("URI").isNotEqualTo(Cypher.literalOf(uri.toString())).and(unit.property(typeSlotUri).isNotEqualTo(Cypher.literalOf(Constants.NIIU_KGBB_URI.toString()))))
				.returning(unit.property("URI"))
				.build();
		return !ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner()).isEmpty();
	}
	
	@Override
	public void delete(SemanticUnit unit)
	{
		unit.setCurrentVersion(false);
		this.save(unit);
		Relationship relation = Cypher.anyNode().relationshipTo(Cypher.node(NODE_WITH_URI_LABEL).withProperties(Map.of("URI", unit.getUri().toString())));
		Statement merge = Cypher.optionalMatch(relation).set(relation.property("currentVersion").to(Cypher.literalFalse())).build();
		ExecutableStatement.of(merge).executeWith(this.client.getQueryRunner());
	}
	
	@Override
	public Set<URI> findAllConstraints(URI uri, StorageModelManager storageModelManager)
	{
		String subjectSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT, false).get();
		String kgbbUriSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, false).get();
		String currentVersionSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, false).get();
		Node unit = Cypher.node(SEMANTIC_UNIT_LABEL).named("unit").withProperties(Map.of(currentVersionSlotUri, true));
		Node subject = Cypher.node(SUBJECT_LABEL, NODE_WITH_URI_LABEL).named("subject").withProperties(Map.of("URI", uri.toString()));
		ResultStatement match1 = Cypher.match(unit.relationshipTo(subject, subjectSlotUri))
				.returning(unit.property(kgbbUriSlotUri).as("kgbb"))
				.build();
		List<Record> units = ExecutableStatement.of(match1).fetchWith(this.client.getQueryRunner());
		Set<URI> constraints = new HashSet<URI>();
		
		for(Record record : units)
		{
			LinearStorageModel subjectStorageModel = storageModelManager.getStorageModel(new URI(record.get("kgbb").asString())).getSubjectStorageModel();
			
			if(subjectStorageModel.getSubclassOf() != null)
			{
				constraints.add(subjectStorageModel.getSubclassOf());
			}
		}
		
		String resourceCurrentVersionSlotUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, false).get();
		String constraintUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_CONSTRAINT, false).get();
		String resourceUriUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI, false).get();
		Node res = Cypher.node(RESOURCE_OBJECT_LABEL)
				.named("res")
				.withProperties(Map.of(resourceCurrentVersionSlotUri, true, resourceUriUri, uri.toString()));
		ResultStatement match2 = Cypher.match(res)
				.returning(res.property(constraintUri).as("constraint"))
				.build();
		List<Record> resourceNodes = ExecutableStatement.of(match2).fetchWith(this.client.getQueryRunner());
		
		for(Record record : resourceNodes)
		{
			constraints.add(new URI(record.get("constraint").asString()));
		}
		
		return constraints;
	}
	
	private static record Relation(String relationName, Map<URI, Boolean> targetNodes) {}
	
	@Override
	public boolean isUnique(URI uri)
	{
		String semanticUnitsGraphUriSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNITS_GRAPH, false).get();
		Node uriNode = Cypher.node(NODE_WITH_URI_LABEL).named("uris").withProperties(Map.of("URI", uri.toString()));
		Node upriNode = Cypher.node(SEMANTIC_UNIT_LABEL).named("graphUris").withProperties(Map.of(semanticUnitsGraphUriSlotUri, uri.toString()));
		ResultStatement match = Cypher.optionalMatch(uriNode).optionalMatch(upriNode).returning(uriNode, upriNode).build();
		var record = ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner()).get(0);
		return record.get("uris").isNull() && record.get("graphUris").isNull();
	}
	
	@Override
	public Set<URI> findAll(URI kgbb)
	{
		String typeSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, false).get();
		String currentVersionSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, false).get();
		Node unit = Cypher.node(NODE_WITH_URI_LABEL, SEMANTIC_UNIT_LABEL).named("unit").withProperties(Map.of(typeSlotUri, kgbb.toString(), currentVersionSlotUri, true));
		ResultStatement match = Cypher.match(unit).returning(unit.property("URI").as("uri")).build();
		List<Record> records = ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner());
		return records.stream().map(record -> new URI(record.get("uri").asString())).collect(Collectors.toSet());
	}
	
	@Override
	public Set<URI> findAssociatingUnits(URI uri)
	{
		String hasAssociatedSemanticUnitSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_ASSOCIATED_SEMANTIC_UNIT, false).get();
		String currentVersionSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, false).get();
		Node unit = Cypher.node(NODE_WITH_URI_LABEL, SEMANTIC_UNIT_LABEL).named("unit").withProperties(Map.of(currentVersionSlotUri, true));
		Node target = Cypher.node(NODE_WITH_URI_LABEL, SEMANTIC_UNIT_LABEL).named("target").withProperties(Map.of("URI", uri.toString()));
		ResultStatement match = Cypher.match(unit.relationshipTo(target, hasAssociatedSemanticUnitSlotUri)).returning(unit.property("URI").as("uri")).build();
		List<Record> records = ExecutableStatement.of(match).fetchWith(this.client.getQueryRunner());
		return records.stream().map(record -> new URI(record.get("uri").asString())).collect(Collectors.toSet());
	}
	
	@TestOnly
	public Neo4jClient getNeo4jClient()
	{
		return this.client;
	}
	
	@TestOnly
	private static void printDebugQuery(String query, Map<String, Object> bindings)
	{
		query = query.replace(" SET ", "\nSET ").replace(", ", ",\n\t");
		
		for(var entry : bindings.entrySet())
		{
			query = query.replace("$" + entry.getKey(), entry.getValue().toString());
		}
		
		System.out.println(query);
	}
}
