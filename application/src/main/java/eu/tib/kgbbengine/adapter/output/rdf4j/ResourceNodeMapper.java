package eu.tib.kgbbengine.adapter.output.rdf4j;

import java.time.LocalDateTime;
import java.util.Map;

import org.eclipse.rdf4j.model.Value;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.domain.util.Either;
import eu.tib.kgbbengine.spi.ValueParser;
import eu.tib.kgbbengine.spi.ValueProvider;

public class ResourceNodeMapper implements ValueProvider<ResourceObjectNode>, ValueParser<ResourceObjectNode, Value>
{
	@Override
	public Either<Object, Map<URI, Boolean>> provide(String name, Slot<?> slot, ResourceObjectNode node)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				return ValueProvider.resource(node.getCreator());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				return ValueProvider.resource(node.getCreatedWithApplication());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_CONSTRAINT:
				return ValueProvider.literal(node.getConstraint());
			case Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_LABEL:
				return ValueProvider.literal(node.getResourceLabel());
			case Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI:
				return ValueProvider.resource(node.getResourceURI());
			case Constants.STORAGE_TEMPLATE_SLOT_INPUT_TYPE_LABEL:
				return ValueProvider.literal(node.getInputTypeLabel());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				return ValueProvider.literal(node.getCreationDate());
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				return ValueProvider.resource(node.getObjectPositionClass());
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				return ValueProvider.literal(node.isCurrentVersion());
			default:
				return null;
		}
	}
	
	@Override
	public void parse(String name, Value value, boolean currentVersion, ResourceObjectNode node, Slot<?> slot)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				node.setCreator(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				node.setCreatedWithApplication(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_CONSTRAINT:
				node.setConstraint(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_LABEL:
				node.setResourceLabel(value.stringValue());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI:
				node.setResourceURI(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_INPUT_TYPE_LABEL:
				node.setInputTypeLabel(value.stringValue());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				node.setCreationDate(LocalDateTime.parse(value.stringValue()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				node.setObjectPositionClass(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				node.setCurrentVersion(Boolean.valueOf(value.stringValue()));
				break;
			default:
				break;
		}
	}
}
