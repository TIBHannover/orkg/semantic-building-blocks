package eu.tib.kgbbengine.adapter.input.webapp.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.tib.kgbbengine.adapter.input.webapp.business.AddInfo;
import eu.tib.kgbbengine.adapter.input.webapp.business.ItemData;
import eu.tib.kgbbengine.adapter.input.webapp.business.ItemInfo;
import eu.tib.kgbbengine.adapter.input.webapp.business.NavigationItem;
import eu.tib.kgbbengine.adapter.input.webapp.business.StatementData;
import eu.tib.kgbbengine.domain.KGBBEngine;
import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.Input.InputBuilder;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnitType;
import eu.tib.kgbbengine.domain.util.Constants;

@Controller
public class SimpleWebController
{
	private static final URI USER_ID = new URI("KGBBEngineUser");
	private static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	private static final URI ITEM_KGBB = new URI("MaterialEntityItemKGBB");
	private static final Pattern RESOURCE_URL_PATTERN = Pattern.compile("http:\\/\\/purl\\.obolibrary\\.org\\/obo\\/([A-Za-z0-9]+)_([A-Za-z0-9]+)");
	private static final Pattern INPUT_PARAMETER_PATTERN = Pattern.compile("@input:(label|resource|type|literal):([a-zA-Z0-9]+)");
	
	@Inject
	private KGBBEngine engine;
	
	public SimpleWebController()
	{
		super();
	}
	
	@ModelAttribute("resourceTypes")
	public List<ResourceType> populateResourceTypes()
	{
		return Arrays.asList(ResourceType.values());
	}
	
	@RequestMapping({"/", "/index"})
	public String index(final AddInfo info)
	{
		info.getItems().addAll(this.findRootItemUnits(this.findAllItems()).stream().map(this::mapToItem).toList());
		return "index";
	}
	
	@RequestMapping({"/item/{uuid}", "/item/{uuid}/{part}"})
	public String item(final ItemInfo info, @PathVariable(value="uuid") String uuid, @PathVariable(value="part", required = false) String part)
	{
		try
		{
			this.buildItemInfo(info, uuid, part);
		}
		catch(Exception e)
		{
			return "redirect:/index";
		}
		
		return "item";
	}
	
	@RequestMapping(value = {"/item/{uuid}", "/item/{uuid}/{part}"}, params = {"addStatement"})
	public String addStatement(final ItemInfo info, @PathVariable(value="uuid") String uuid, @PathVariable(value="part", required = false) String part, final BindingResult bindingResult, final HttpServletRequest req)
	{
		String kgbbi = req.getParameter("addStatement");
		String subject = req.getParameter("subject");
		InputBuilder input = this.parseInput(bindingResult, req);
		
		if(!bindingResult.hasErrors())
		{
			try
			{
				this.engine.createStatementUnit(new URI(kgbbi), USER_ID, input.build(), new URI(subject != null ? subject : part != null ? part : uuid));
			}
			catch(Exception e)
			{
				bindingResult.reject("error", e.getMessage());
			}
		}
		
		try
		{
			this.buildItemInfo(info, uuid, part);
		}
		catch(Exception e)
		{
			return "redirect:/index";
		}
		
		return "item";
	}
	
	@RequestMapping(value = {"/item/{uuid}", "/item/{uuid}/{part}"}, params = {"addItem"})
	public String addItem(final ItemInfo info, @PathVariable(value="uuid") String uuid, @PathVariable(value="part", required = false) String part, final BindingResult bindingResult, final HttpServletRequest req)
	{
		String kgbbi = req.getParameter("addItem");
		String subject = req.getParameter("subject");
		InputBuilder input = this.parseInput(bindingResult, req);
		input.addResourceInput(new URI(kgbbi), new URI(subject), null, ResourceType.INSTANCE);
		
		if(!bindingResult.hasErrors())
		{
			try
			{
				this.engine.createCompoundUnit(new URI(kgbbi), USER_ID, input.build(), new URI(part != null ? part : uuid));
			}
			catch(Exception e)
			{
				bindingResult.reject("error", e.getMessage());
			}
		}
		
		try
		{
			this.buildItemInfo(info, uuid, part);
		}
		catch(Exception e)
		{
			return "redirect:/index";
		}
		
		return "item";
	}
	
	@RequestMapping(value = {"/item/{uuid}", "/item/{uuid}/{part}"}, params = {"removeStatement"})
	public String removeStatement(final ItemInfo info, @PathVariable(value="uuid") String uuid, @PathVariable(value="part", required=false) String part, final BindingResult bindingResult, final HttpServletRequest req)
	{
		try
		{
			this.engine.deleteStatementUnit(new URI(req.getParameter("removeStatement")), USER_ID, new URI(req.getParameter("removeSubject") != null ? req.getParameter("removeSubject") : (part != null ? part : uuid)));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if(part != null)
		{
			return "redirect:/item/" + uuid + "/" + part;
		}
		
		return "redirect:/item/" + uuid;
	}
	
	@RequestMapping(value = {"/item/{uuid}", "/item/{uuid}/{part}"}, params = {"removeItem"})
	public String removeItem(final ItemInfo info, @PathVariable(value="uuid") String uuid, @PathVariable(value="part", required = false) String part, final BindingResult bindingResult, final HttpServletRequest req)
	{
		try
		{
			this.engine.deleteCompoundUnit(new URI(req.getParameter("removeItem")), USER_ID);
		}
		catch(IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		
		if(part != null)
		{
			return "redirect:/item/" + uuid + "/" + part;
		}
		
		return "redirect:/index";
	}
	
	@RequestMapping(value = {"/item/{uuid}/{part}"}, params = {"updateStatement"})
	public String updateStatement(final ItemInfo info, @PathVariable(value="uuid") String uuid, @PathVariable(value="part") String part, final BindingResult bindingResult, final HttpServletRequest req)
	{
		InputBuilder input = this.parseInput(bindingResult, req);
		
		if(!bindingResult.hasErrors())
		{
			try
			{
				this.engine.updateStatementUnit(new URI(req.getParameter("updateStatement")), USER_ID, input.build(), new URI(req.getParameter("updateSubject") != null ? req.getParameter("updateSubject") : (part != null ? part : uuid)));
			}
			catch(Exception e)
			{
				bindingResult.reject("error", e.getMessage());
			}
		}
		
		try
		{
			this.buildItemInfo(info, uuid, part);
		}
		catch(Exception e)
		{
			return "redirect:/index";
		}
		
		return "item";
	}
	
	@RequestMapping(value = "/", params = {"create"})
	public String create(final AddInfo info, final BindingResult bindingResult, final HttpServletRequest req)
	{
		if(info.getResource().isBlank())
		{
			bindingResult.rejectValue("resource", "blank");
		}
		
		if(!bindingResult.hasErrors())
		{
			try
			{
				CompoundUnitResult result = this.engine.createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
						.addResourceInput(ITEM_KGBBI, new URI(req.getParameter("resource")), req.getParameter("label"), ResourceType.CLASS)
						.build(), null);
				
				return "redirect:/item/" + result.compound().getUri() + "/" + result.compound().getUri();
			}
			catch(Exception e)
			{
				bindingResult.reject("error", e.getMessage());
			}
		}
		
		info.getItems().addAll(this.findRootItemUnits(this.findAllItems()).stream().map(this::mapToItem).sorted((a, b) -> a.getItemUri().compareTo(b.getItemUri())).toList());
		return "index";
	}
	
	private void buildItemInfo(final ItemInfo info, String uuid, @Nullable String part) throws Exception
	{
		SemanticUnit unit;
		
		if(part == null)
		{
			unit = this.engine.findSemanticUnit(new URI(uuid));
			info.setUri(uuid);
			info.setSubject(unit.getSubject().toString());
			info.setContext(uuid);
			
			NavigationItem item = this.mapToItem(unit);
			this.buildItemHierarchy(item, unit);
			info.getItems().add(item);
		}
		else
		{
			unit = this.engine.findSemanticUnit(new URI(part));
			info.setUri(part);
			info.setSubject(unit.getSubject().toString());
			info.setContext(uuid);
			
			SemanticUnit root = this.engine.findSemanticUnit(new URI(uuid));
			NavigationItem item = this.mapToItem(root);
			this.buildItemHierarchy(item, root);
			
			if(!item.hasChild(part))
			{
				throw new Exception("Invalid Context");
			}
			
			info.getItems().add(item);
		}
		
		info.getItems().sort((a, b) -> a.getName().compareTo(b.getName()));
		
		if(!ITEM_KGBBI.equals(unit.getKgbbi()))
		{
			throw new Exception("Requested semantic unit is not an item unit");
		}
		
		this.gatherUnitData(unit, info);
	}
	
	private void gatherUnitData(SemanticUnit unit, ItemData info)
	{
		List<SemanticUnit> associations = unit.getCurrentAssociatedSemanticUnits().stream()
				.map(this.engine::findSemanticUnit)
				.toList();
		
		for(SemanticUnit association : associations)
		{
			SemanticUnitType type = this.engine.getSemanticUnitType(association);
			
			if(SemanticUnitType.COMPOUND.equals(type))
			{
				List<ItemData> nodes = info.getChildren().computeIfAbsent(association.getKgbb().toString(), key -> new ArrayList<ItemData>());
				ItemData data = new ItemData();
				data.setUri(association.getUri().toString());
				nodes.add(data);
				
				if(association.getSubject().equals(unit.getSubject()))
				{
					this.gatherUnitData(association, data);
				}
			}
			else if(SemanticUnitType.STATEMENT.equals(type))
			{
				if(Constants.NIIU_KGBB_URI.equals(association.getKgbb()))
				{
					ResourceObjectNode resource = association.findResourceObject(Constants.NIIU_RESOURCE_POSITION_URI);
					LiteralObjectNode literal = association.findLiteralObject(Constants.NIIU_LITERAL_POSITION_URI);
					
					if(association.getSubject().equals(unit.getSubject()))
					{
						info.setLabel((String) literal.getLiteral());
						info.setResourceLabel(resource.getResourceLabel());
						info.setResourceURI(resource.getResourceURI().toString());
						info.setFormattedResourceLabel(formatResource(resource.getResourceLabel(), resource.getResourceURI()));
						info.setNiiu(association.getUri().toString());
					}
				}
				else
				{
					List<Map<String, StatementData>> kgbb2statement = info.getStatements().computeIfAbsent(association.getKgbb().toString(), key -> new ArrayList<Map<String, StatementData>>());
					Map<String, StatementData> position2data = new HashMap<String, StatementData>();
					kgbb2statement.add(position2data);
					
					for(ResourceObjectNode resource : association.getResourceObjects())
					{
						if(!resource.isCurrentVersion())
						{
							continue;
						}
						
						StatementData data = new StatementData();
						data.setUri(association.getUri().toString());
						
						if(!association.getCurrentObjectDescribedBySemanticUnits().isEmpty())
						{
							data.setDescribedBy(association.getCurrentObjectDescribedBySemanticUnits().iterator().next().toString());
						}
						
						if(resource.getResourceLabel() == null) //resource is an instance, so we load the niiu of the subject
						{
							SemanticUnit niiu = this.engine.findBySubject(resource.getResourceURI(), Constants.NIIU_KGBB_URI);
							ResourceObjectNode niiuResource = niiu.findResourceObject(Constants.NIIU_RESOURCE_POSITION_URI);
							LiteralObjectNode niiuLiteral = niiu.findLiteralObject(Constants.NIIU_LITERAL_POSITION_URI);
							data.setResourceLabel((String) niiuLiteral.getLiteral());
							data.setResourceURI(niiuResource.getResourceURI().toString());
							data.setFormattedResourceLabel(formatResource((String) niiuLiteral.getLiteral(), niiuResource.getResourceURI()));
						}
						else
						{
							data.setResourceLabel(resource.getResourceLabel());
							data.setLiteral(resource.getResourceLabel());
							data.setFormattedResourceLabel(formatResourceNode(resource));
						}
						
						position2data.put(resource.getObjectPositionClass().toString(), data);
					}
					
					for(LiteralObjectNode literal : association.getLiteralObjects())
					{
						if(!literal.isCurrentVersion())
						{
							continue;
						}
						
						StatementData data = new StatementData();
						data.setUri(association.getUri().toString());
						data.setLiteral(String.valueOf(literal.getLiteral()));
						position2data.put(literal.getObjectPositionClass().toString(), data);
					}
				}
			}
		}
	}
	
	private NavigationItem mapToItem(SemanticUnit item)
	{
		SemanticUnit niiu = this.engine.findBySubject(item.getSubject(), Constants.NIIU_KGBB_URI);
//		ResourceObjectNode resource = niiu.findResourceObject(Constants.NIIU_RESOURCE_POSITION_URI);
		LiteralObjectNode literal = niiu.findLiteralObject(Constants.NIIU_LITERAL_POSITION_URI);
		return new NavigationItem((String) literal.getLiteral(), item.getUri().toString());
	}
	
	private Set<SemanticUnit> findRootItemUnits(Set<SemanticUnit> items)
	{
		Map<URI, SemanticUnit> result = items.stream().collect(Collectors.toMap(SemanticUnit::getUri, Function.identity()));
		
		for(SemanticUnit unit : items)
		{
			Set<URI> links = unit.getCurrentLinkedSemanticUnits();
			
			for(URI link : links)
			{
				result.remove(link);
			}
		}
		
		return new HashSet<SemanticUnit>(result.values());
	}
	
	private void buildItemHierarchy(NavigationItem item, SemanticUnit unit)
	{
		for(URI uri : unit.getCurrentLinkedSemanticUnits())
		{
			if(item.hasParent(uri.toString()))
			{
				continue;
			}
			
			SemanticUnit linked = this.engine.findSemanticUnit(uri);
			
			if(ITEM_KGBB.equals(linked.getKgbb()))
			{
				NavigationItem child = this.mapToItem(linked);
				child.setParent(item);
				this.buildItemHierarchy(child, linked);
				item.getChildren().add(child);
			}
		}
	}
	
	private InputBuilder parseInput(final BindingResult bindingResult, final HttpServletRequest req)
	{
		Map<String, ParsedInput> inputs = new HashMap<String, ParsedInput>();
		
		for(var parameter : req.getParameterMap().entrySet())
		{
			Matcher matcher = INPUT_PARAMETER_PATTERN.matcher(parameter.getKey());
			
			if(matcher.matches())
			{
				ParsedInput input = inputs.computeIfAbsent(matcher.group(2), key -> new ParsedInput());
				String value = parameter.getValue()[0];
				
				switch(matcher.group(1))
				{
					case "label":
						input.setLabel(value);
						break;
					case "resource":
						input.setResource(value);
						break;
					case "type":
						input.setType(ResourceType.valueOf(value));
						break;
					case "literal":
						input.setLiteral(value);
						break;
					default:
						break;
				}
			}
		}
		
		InputBuilder builder = Input.builder();
		
		for(var entry : inputs.entrySet())
		{
			ParsedInput input = entry.getValue();
			
			if(input.getType() != null)
			{
				builder.addResourceInput(new URI(entry.getKey()), new URI(input.getResource()), input.getLabel(), input.getType());
			}
			else
			{
				if(input.getLiteral().isBlank())
				{
					bindingResult.reject("input_missing");
				}
				else
				{
					builder.addLiteralInput(new URI(entry.getKey()), input.getLiteral());
				}
			}
		}
		
		return builder;
	}
	
	private Set<SemanticUnit> findAllItems()
	{
		return this.engine.findAll(ITEM_KGBB).stream().map(this.engine::findSemanticUnit).collect(Collectors.toSet());
	}
	
	private static String formatResourceNode(ResourceObjectNode resource)
	{
		return formatResource(resource.getResourceLabel(), resource.getResourceURI());
	}
	
	private static String formatResource(String resourceLabel, URI resourceUri)
	{
		return resourceLabel + " [" + shortenResourceUri(resourceUri.toString()) + "]";
	}
	
	private static String shortenResourceUri(String uri)
	{
		Matcher matcher = RESOURCE_URL_PATTERN.matcher(uri);
		
		if(matcher.find())
		{
			return matcher.group(1) + ":" + matcher.group(2);
		}
		
		return uri;
	}
	
	private static class ParsedInput
	{
		private ResourceType type;
		private String resource;
		private String label;
		private String literal;
		
		public ResourceType getType()
		{
			return this.type;
		}
		
		public void setType(ResourceType type)
		{
			this.type = type;
		}
		
		public String getResource()
		{
			return this.resource;
		}
		
		public void setResource(String resource)
		{
			this.resource = resource;
		}
		
		public String getLabel()
		{
			return this.label;
		}
		
		public void setLabel(String label)
		{
			this.label = label;
		}
		
		public String getLiteral()
		{
			return this.literal;
		}
		
		public void setLiteral(String literal)
		{
			this.literal = literal;
		}
	}
}
