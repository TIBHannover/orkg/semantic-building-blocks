package eu.tib.kgbbengine.adapter.input.webapp.business;

import java.util.ArrayList;
import java.util.List;

public class ItemInfo extends ItemData
{
	private String subject;
	private String context;
	private List<NavigationItem> items = new ArrayList<NavigationItem>();
	
	public String getSubject()
	{
		return this.subject;
	}
	
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	
	public List<NavigationItem> getItems()
	{
		return this.items;
	}
	
	public void setItems(List<NavigationItem> items)
	{
		this.items = items;
	}
	
	public String getContext()
	{
		return this.context;
	}
	
	public void setContext(String context)
	{
		this.context = context;
	}
}
