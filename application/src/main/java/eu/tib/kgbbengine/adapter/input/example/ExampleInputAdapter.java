package eu.tib.kgbbengine.adapter.input.example;

import java.util.UUID;

import eu.tib.kgbbengine.adapter.output.FakeInMemoryKGBBApplicationSpecificationRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryKGBBRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryObjectPositionRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryStorageModelRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryUserRepository;
import eu.tib.kgbbengine.adapter.output.FakeOntologyLookup;
import eu.tib.kgbbengine.adapter.output.neo4j.Neo4jRepository;
import eu.tib.kgbbengine.domain.KGBBEngine;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;

public class ExampleInputAdapter
{
	private static final URI USER_ID = new URI("KGBBEngineUser");
	private static final URI ITEM_KGBBI = new URI("MaterialEntityItemKGBBInstance");
	private static final URI STATEMENT_KGBBI = new URI("HasPartStatementKGBBInstance");
	private static final URI STATEMENT_RESOURCE_POSITION = new URI("HasPartStatementUnitResourceObjectPosition");
	
	public static void main(String... args) throws Exception
	{
		var kgbbRepo = new FakeInMemoryKGBBRepository();
		var kgbbAppSpecRepo = new FakeInMemoryKGBBApplicationSpecificationRepository();
		var positionRepo = new FakeInMemoryObjectPositionRepository();
		var storageModelRepo = new FakeInMemoryStorageModelRepository();
		var userRepo = new FakeInMemoryUserRepository();
		var ontoService = new FakeOntologyLookup();
		var engine = new KGBBEngine(kgbbRepo, kgbbAppSpecRepo, positionRepo, storageModelRepo, (suModel, resourceModel) -> new Neo4jRepository("bolt://localhost:7687", "neo4j", "123456", suModel, resourceModel), userRepo, ontoService, () -> new URI(UUID.randomUUID().toString()));
		
		var itemResult = engine.createCompoundUnit(ITEM_KGBBI, USER_ID, Input.builder()
				.addResourceInput(ITEM_KGBBI, new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		
		var statementResult = engine.createStatementUnit(STATEMENT_KGBBI, USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult.compound().getUri());
		
		engine.updateStatementUnit(statementResult.statement().getUri(), USER_ID, Input.builder()
				.addResourceInput(STATEMENT_RESOURCE_POSITION, new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult.compound().getUri());
	}
}
