package eu.tib.kgbbengine.adapter.input.webapp.web;

import java.util.UUID;
import java.util.function.BiFunction;

import org.neo4j.cypherdsl.core.Cypher;
import org.neo4j.cypherdsl.core.Node;
import org.neo4j.cypherdsl.core.Statement;
import org.neo4j.cypherdsl.core.executables.ExecutableStatement;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;

import eu.tib.kgbbengine.adapter.output.FakeInMemoryKGBBApplicationSpecificationRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryKGBBRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryObjectPositionRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemorySemanticUnitRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryStorageModelRepository;
import eu.tib.kgbbengine.adapter.output.FakeInMemoryUserRepository;
import eu.tib.kgbbengine.adapter.output.FakeOntologyLookup;
import eu.tib.kgbbengine.adapter.output.neo4j.Neo4jRepository;
import eu.tib.kgbbengine.adapter.output.rdf4j.Rdf4jRepositiory;
import eu.tib.kgbbengine.domain.KGBBEngine;
import eu.tib.kgbbengine.domain.KGBBEngine.CompoundUnitResult;
import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.UriSupplier;
import eu.tib.kgbbengine.domain.input.Input;
import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;

@Configuration
@EnableWebMvc
@ComponentScan
public class SpringWebConfig implements WebMvcConfigurer, ApplicationContextAware
{
	private ApplicationContext applicationContext;
	private final KGBBEngine engine;
	
	public SpringWebConfig() throws Exception
	{
		var kgbbRepo = new FakeInMemoryKGBBRepository();
		var kgbbInstanceRepo = new FakeInMemoryKGBBApplicationSpecificationRepository();
		var positionRepo = new FakeInMemoryObjectPositionRepository();
		var storageModelRepo = new FakeInMemoryStorageModelRepository();
		var userRepo = new FakeInMemoryUserRepository();
		var ontoService = new FakeOntologyLookup();
		this.engine = new KGBBEngine(kgbbRepo, kgbbInstanceRepo, positionRepo, storageModelRepo, fakeInMemoryRepository(), userRepo, ontoService, incrementingUriSupplier());
		
		CompoundUnitResult itemResult = this.engine.createCompoundUnit(new URI("MaterialEntityItemKGBBInstance"), new URI("KGBBEngineUser"), Input.builder()
				.addResourceInput(new URI("MaterialEntityItemKGBBInstance"), new URI("http://purl.obolibrary.org/obo/UBERON_0000468"), "multicellular organism", ResourceType.CLASS)
				.build(), null);
		this.engine.createStatementUnit(new URI("HasPartStatementKGBBInstance"), new URI("KGBBEngineUser"), Input.builder()
				.addResourceInput(new URI("HasPartStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/UBERON_0000970"), "eye", ResourceType.CLASS)
				.build(), itemResult.compound().getUri());
		this.engine.createCompoundUnit(new URI("WeightMeasurementCompoundKGBBInstance"), new URI("KGBBEngineUser"), Input.builder()
				.addResourceInput(new URI("WeightMeasurementCompoundKGBBInstance"), itemResult.compound().getSubject(), null, ResourceType.INSTANCE)
				.addResourceInput(new URI("QualityStatementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/PATO_0000128"), "weight", ResourceType.CLASS)
				.addResourceInput(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("http://purl.obolibrary.org/obo/NCIT_C28252"), null, ResourceType.CLASS)
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition1"), String.valueOf(5.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition2"), String.valueOf(4.0))
				.addLiteralInput(new URI("WeightMeasurementUnitLiteralObjectPosition3"), String.valueOf(6.0))
				.build(), itemResult.compound().getUri());
	}
	
	@SuppressWarnings("unused")
	private static UriSupplier randomUriSupplier()
	{
		return () -> new URI(UUID.randomUUID().toString());
	}
	
	@SuppressWarnings("unused")
	private static UriSupplier incrementingUriSupplier()
	{
		return new UriSupplier()
		{
			private int index = 0;
			
			@Override
			public URI newURI()
			{
				return new URI(String.valueOf(this.index++));
			}
		};
	}
	
	@SuppressWarnings("unused")
	private static BiFunction<LinearStorageModel, LinearStorageModel, SemanticUnitRepository> fakeInMemoryRepository()
	{
		return (genericSemanticUnitModel, genericResourceObjectModel) -> new FakeInMemorySemanticUnitRepository();
	}
	
	@SuppressWarnings("unused")
	private static BiFunction<LinearStorageModel, LinearStorageModel, SemanticUnitRepository> neo4jRepository()
	{
		return (genericSemanticUnitModel, genericResourceObjectModel) ->
		{
			Neo4jRepository repo = new Neo4jRepository("bolt://localhost:7687", "neo4j", "123456", genericSemanticUnitModel, genericResourceObjectModel);
			Node node = Cypher.anyNode();
			Statement query = Cypher.match(node).detachDelete(node).build();
			ExecutableStatement.of(query).executeWith(repo.getNeo4jClient().getQueryRunner());
			return repo;
		};
	}
	
	@SuppressWarnings("unused")
	private static BiFunction<LinearStorageModel, LinearStorageModel, SemanticUnitRepository> rdf4jRepository()
	{
		return Rdf4jRepositiory::new;
	}
	
	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException
	{
		this.applicationContext = applicationContext;
	}
	
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry)
	{
		WebMvcConfigurer.super.addResourceHandlers(registry);
		registry.addResourceHandler("/images/**").addResourceLocations("/images/");
		registry.addResourceHandler("/css/**").addResourceLocations("/css/");
		registry.addResourceHandler("/js/**").addResourceLocations("/js/");
	}
	
	@Bean
	public KGBBEngine kgbbEngine()
	{
		return this.engine;
	}
	
	@Bean
	public ResourceBundleMessageSource messageSource()
	{
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("Messages");
		return messageSource;
	}
	
	@Override
	public void addFormatters(final FormatterRegistry registry)
	{
		WebMvcConfigurer.super.addFormatters(registry);
//		registry.addFormatter(this.dateFormatter());
	}
	
//	@Bean
//	public DateFormatter dateFormatter()
//	{
//		return new DateFormatter();
//	}
	
	@Bean
	public SpringResourceTemplateResolver templateResolver()
	{
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setApplicationContext(this.applicationContext);
		templateResolver.setPrefix("templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCacheable(false); //XXX Template caching
		return templateResolver;
	}
	
	@Bean
	public SpringTemplateEngine templateEngine()
	{
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(this.templateResolver());
		templateEngine.setEnableSpringELCompiler(true);
		return templateEngine;
	}
	
	@Bean
	public ThymeleafViewResolver viewResolver()
	{
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(this.templateEngine());
		return viewResolver;
	}
}