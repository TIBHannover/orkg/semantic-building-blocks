package eu.tib.kgbbengine.adapter.input.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import eu.tib.kgbbengine.adapter.input.webapp.business.SpringBusinessConfig;
import eu.tib.kgbbengine.adapter.input.webapp.web.SpringWebConfig;

@SpringBootApplication
@EnableWebMvc
public class Main extends AbstractAnnotationConfigDispatcherServletInitializer
{
	public static void main(String[] args)
	{
		SpringApplication.run(Main.class, args);
	}
	
	@Override
	protected Class<?>[] getServletConfigClasses()
	{
		return new Class<?>[]{SpringWebConfig.class};
	}
	
	@Override
	protected Class<?>[] getRootConfigClasses()
	{
		return new Class<?>[]{SpringBusinessConfig.class};
	}
	
	@Override
	protected String[] getServletMappings()
	{
		return new String[]{"/"};
	}
}