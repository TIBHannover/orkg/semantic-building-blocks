package eu.tib.kgbbengine.adapter.output;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.spi.OntologyLookupService;
import eu.tib.kgbbengine.spi.OntologyServiceException;

public class FakeOntologyLookup implements OntologyLookupService
{
	private static final String API_URL = "https://www.ebi.ac.uk/";
	private static final Pattern RESOURCE_URL_PATTERN = Pattern.compile("http:\\/\\/purl\\.obolibrary\\.org\\/obo\\/([A-Za-z0-9]+)_[A-Za-z0-9]+");
	private static final ObjectMapper MAPPER = new ObjectMapper();
	
	private final Map<String, Set<String>> cache = new HashMap<String, Set<String>>();
	
	@Override
	public boolean hasAncestors(URI uri, Collection<URI> ancestors) throws OntologyServiceException
	{
		Set<String> remaining = ancestors.stream().filter(Predicate.not(uri::equals)).map(URI::toString).collect(Collectors.toSet());
		
		if(remaining.isEmpty() || this.cache.containsKey(uri.toString()) && this.cache.get(uri.toString()).containsAll(remaining))
		{
			return true;
		}
		
		String url = API_URL + "ols/api/ontologies/" + this.ontologyId(uri) + "/terms/" + encode(encode(uri.toString())) + "/hierarchicalAncestors?page=";
		
		for(int page = 0;; page++)
		{
			HttpGet httpGet = new HttpGet(url + page);
			
			try(CloseableHttpClient client = HttpClients.createDefault(); CloseableHttpResponse response = client.execute(httpGet))
			{
				if(response.getStatusLine().getStatusCode() != 200)
				{
					throw new Exception();
				}
				
				JsonNode root = MAPPER.readTree(response.getEntity().getContent());
				
				for(JsonNode term : root.path("_embedded").path("terms"))
				{
					String iri = term.path("iri").asText();
					
					if(remaining.remove(iri) && remaining.isEmpty())
					{
						return true;
					}
					
					this.cache.computeIfAbsent(uri.toString(), key -> new HashSet<String>()).add(iri);
				}
				
				if(root.path("page").path("totalPages").asInt() - 1  <= page)
				{
					return false;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	private String ontologyId(URI uri) throws OntologyServiceException
	{
		Matcher matcher = RESOURCE_URL_PATTERN.matcher(uri.toString());
		
		if(!matcher.find())
		{
			throw new OntologyServiceException("Uri " + uri + " does not match pattern " + RESOURCE_URL_PATTERN.pattern());
		}
		
		return matcher.group(1).toLowerCase();
	}
	
	@Override
	public String resourceLabel(URI uri) throws OntologyServiceException
	{
		HttpGet httpGet = new HttpGet(API_URL + "ols/api/ontologies/" + this.ontologyId(uri) + "/terms/" + encode(encode(uri.toString())));
		
		try(CloseableHttpClient client = HttpClients.createDefault(); CloseableHttpResponse response = client.execute(httpGet))
		{
			if(response.getStatusLine().getStatusCode() == 200)
			{
				return MAPPER.readTree(response.getEntity().getContent()).get("label").asText();
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		throw new OntologyServiceException("Could not retrieve resource label for " + uri);
	}
	
	private static String encode(String string)
	{
		return URLEncoder.encode(string, StandardCharsets.UTF_8);
	}
}
