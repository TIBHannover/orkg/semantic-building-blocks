package eu.tib.kgbbengine.adapter.output.rdf4j;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelException;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Queries;
import org.eclipse.rdf4j.sparqlbuilder.core.query.SelectQuery;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Iri;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf;
import org.jetbrains.annotations.TestOnly;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.LinearStorageModel;
import eu.tib.kgbbengine.domain.storage.SemanticUnitStorageModel;
import eu.tib.kgbbengine.domain.storage.StorageModelManager;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.domain.util.Either;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;
import eu.tib.kgbbengine.spi.ValueProvider;

public class Rdf4jRepositiory implements SemanticUnitRepository
{
	private static final String PREFIX = "http://orkg.org/";
	private static final SemanticUnitMapper SEMANTIC_UNIT_MAPPER = new SemanticUnitMapper();
	private static final ResourceNodeMapper RESOURCE_NODE_MAPPER = new ResourceNodeMapper();
	private static final LiteralNodeMapper LITERAL_NODE_MAPPER = new LiteralNodeMapper();
	private static final IRI SEMANTIC_UNIT_LABEL = Values.iri(PREFIX + "SemanticUnit");
	private static final IRI RESOURCE_OBJECT_LABEL = Values.iri(PREFIX + "ResourceObject");
	private static final IRI LITERAL_OBJECT_LABEL = Values.iri(PREFIX + "LiteralObject");
	private static final IRI SUBJECT_LABEL = Values.iri(PREFIX + "Subject");
	
	private final Repository repo = new SailRepository(new MemoryStore());
	private final LinearStorageModel genericSemanticUnitModel;
	private final LinearStorageModel genericResourceObjectModel;
	
	public Rdf4jRepositiory(LinearStorageModel genericSemanticUnitModel, LinearStorageModel genericResourceObjectModel)
	{
		this.genericSemanticUnitModel = genericSemanticUnitModel;
		this.genericResourceObjectModel = genericResourceObjectModel;
	}
	
	@Override
	public void save(SemanticUnit unit)
	{
		SemanticUnitStorageModel model = unit.getStorageModel();
		LinearStorageModel subjectModel = model.getSubjectStorageModel();
		LinearStorageModel suModel = model.getSemanticUnitStorageModel();
		Map<URI, Set<URI>> nodes = new HashMap<URI, Set<URI>>();
		IRI dataGraphUri = Values.iri(PREFIX + unit.getUri());
		IRI suGraphUri = Values.iri(PREFIX + unit.getSemanticUnitsGraph());
		CustomModelBuilder suGraph = new CustomModelBuilder().namedGraph(suGraphUri);
		this.appendModelMerge(unit.getUri(), unit, SEMANTIC_UNIT_MAPPER, SEMANTIC_UNIT_LABEL, suModel, suGraph);
		CustomModelBuilder dataGraph = new CustomModelBuilder().namedGraph(dataGraphUri);
		
		for(ResourceObjectNode object : unit.getResourceObjects())
		{
			nodes.computeIfAbsent(object.getObjectPositionClass(), key -> new HashSet<URI>()).add(object.getUri());
			LinearStorageModel positionModel = model.getObjectStorageModels().get(object.getObjectPositionClass());
			this.appendModelMerge(object.getUri(), object, RESOURCE_NODE_MAPPER, RESOURCE_OBJECT_LABEL, positionModel, dataGraph);
		}
		
		for(LiteralObjectNode object : unit.getLiteralObjects())
		{
			nodes.computeIfAbsent(object.getObjectPositionClass(), key -> new HashSet<URI>()).add(object.getUri());
			LinearStorageModel positionModel = model.getObjectStorageModels().get(object.getObjectPositionClass());
			this.appendModelMerge(object.getUri(), object, LITERAL_NODE_MAPPER, LITERAL_OBJECT_LABEL, positionModel, dataGraph);
		}
		
		dataGraph.subject(Values.iri(PREFIX + unit.getSubject()))
			.add(RDF.TYPE, SUBJECT_LABEL);
		
		if(!subjectModel.isEmpty())
		{
			for(Entry<String, Slot<?>> slot : subjectModel.getSlots().entrySet())
			{
				Set<URI> targets = nodes.get(new URI(slot.getKey()));
				
				if(targets != null)
				{
					String key = subjectModel.resolveSlotURI(slot.getValue(), true, PREFIX).orElse(PREFIX + slot.getKey());
					
					for(URI target : targets)
					{
						dataGraph.add(key, Values.iri(PREFIX + target));
					}
				}
			}
		}
		
		try(RepositoryConnection connection = this.repo.getConnection())
		{
			connection.begin();
			connection.remove(connection.getStatements(null, null, null, suGraphUri));
			connection.add(suGraph.build());
			connection.remove(connection.getStatements(null, null, null, dataGraphUri));
			connection.add(dataGraph.build());
			connection.commit();
		}
	}
	
	private <T> void appendModelMerge(URI uri, T object, ValueProvider<T> provider, IRI marker, LinearStorageModel model, CustomModelBuilder builder)
	{
		builder.subject(Values.iri(PREFIX + uri));
		builder.add(RDF.TYPE, marker);
		
		for(Entry<String, Slot<?>> slot : model.getSlots().entrySet())
		{
			Either<Object, Map<URI, Boolean>> value = provider.provide(slot.getKey(), slot.getValue(), object);
			
			if(value == null)
			{
				throw new IllegalStateException("No value provider for slot " + slot.getKey());
			}
			else if(value.isLeft())
			{
				if(value.left().isPresent())
				{
					String key = model.resolveSlotURI(slot.getValue(), true, PREFIX).orElse(PREFIX + slot.getKey());
					builder.add(key, value.left().get());
				}
			}
			else if(value.isRight())
			{
				String key = model.resolveSlotURI(slot.getValue(), true, PREFIX).orElse(PREFIX + slot.getKey());
				
				for(URI target : value.right().get().keySet())
				{
					builder.add(key, Values.iri(PREFIX + target));
				}
			}
		}
	}
	
	@Override
	public SemanticUnit findByUpri(URI upri, SemanticUnitStorageModel model)
	{
		LinearStorageModel suModel = model.getSemanticUnitStorageModel();
		String prefixedUri = PREFIX + upri;
		String currentVersionSlotUri = model.getSemanticUnitStorageModel().resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, true, PREFIX).get();
		String subjectSlotUri = model.getSemanticUnitStorageModel().resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT, true, PREFIX).get();
		String resourceUriSlotUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI, true, PREFIX).get();
		String resourceCurrentVersionSlotUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, true, PREFIX).get();
		//Not using rdf4j query builder here because it does not support BIND
		//See https://rdf4j.org/documentation/tutorials/sparqlbuilder/
		String query = """
		SELECT DISTINCT ?s ?p ?o ?c ?t {
			{
				BIND(<$URI> as ?s)
				BIND(<$SemanticUnitIRI> as ?t)
				?s ?p ?o;
					a <$SemanticUnitIRI>;
					<$hasSemanticUnitSubject> ?subject .
				OPTIONAL {
					{
				    	?s <$currentVersion> true;
							<$hasSemanticUnitSubject> ?subject .
						?o <$currentVersion> ?c;
							<$hasSemanticUnitSubject> ?subject .
					} UNION {
						?s <$hasSemanticUnitSubject> ?subject .
						?o <$currentVersion> ?c;
							<$hasSemanticUnitSubject> ?subject2 .
						?subject ?y ?resource .
						?resource a <$ResourceObjectIRI>;
							<$resourceCurrentVersion> true;
							<$resourceURI> ?subject2 .
						FILTER(?subject != ?subject2)
					}
				}
			} UNION {
				GRAPH <$URI> {
					{
						BIND(<$ResourceObjectIRI> as ?t)
						?s ?p ?o;
							a <$ResourceObjectIRI> .
					} UNION {
						BIND(<$LiteralObjectIRI> as ?t)
						?s ?p ?o;
							a <$LiteralObjectIRI> .
					}
				}
			}
		}
		"""	.replace("$URI", prefixedUri)
			.replace("$SemanticUnitIRI", SEMANTIC_UNIT_LABEL.stringValue())
			.replace("$currentVersion", currentVersionSlotUri)
			.replace("$resourceCurrentVersion", resourceCurrentVersionSlotUri)
			.replace("$resourceURI", resourceUriSlotUri)
			.replace("$hasSemanticUnitSubject", subjectSlotUri)
			.replace("$SubjectIRI", SUBJECT_LABEL.stringValue())
			.replace("$ResourceObjectIRI", RESOURCE_OBJECT_LABEL.stringValue())
			.replace("$LiteralObjectIRI", LITERAL_OBJECT_LABEL.stringValue());
		List<BindingSet> bindingSets = this.tupleQueryNoTransaction(query);
		
		if(bindingSets.isEmpty())
		{
			throw new NoSuchElementException("Tried to find missing semantic unit " + upri);
		}
		
		Map<IRI, Result> results = new HashMap<IRI, Result>();
		
		for(BindingSet bindingSet : bindingSets)
		{
			Value s = bindingSet.getBinding("s").getValue();
			Value p = bindingSet.getBinding("p").getValue();
			Value o = bindingSet.getBinding("o").getValue();
			Value t = bindingSet.getBinding("t").getValue();
			Binding c = bindingSet.getBinding("c");
			
			if(o.equals(t))
			{
				continue;
			}
			
			Result result = results.computeIfAbsent((IRI) s, key -> new Result(new URI(stripPrefix(key.stringValue())), (IRI) t));
			result.addStatement(p.stringValue(), o, c != null ? Boolean.valueOf(c.getValue().stringValue()) : false);
		}
		
		Result su = results.remove(Values.iri(prefixedUri));
		SemanticUnit unit = new SemanticUnit(model);
		unit.setUri(upri);
		
		for(Entry<String, Slot<?>> entry : suModel.getSlots().entrySet())
		{
			String slotUri = suModel.resolveSlotURI(entry.getValue(), true, PREFIX).orElse(entry.getKey());
			Set<Pair<Value, Boolean>> values = su.getValues(slotUri);
			
			if(values != null)
			{
				for(Pair<Value, Boolean> value : values)
				{
					SEMANTIC_UNIT_MAPPER.parse(entry.getKey(), value.getKey(), value.getValue(), unit, entry.getValue());
				}
			}
		}
		
		for(Entry<IRI, Result> resultEntry : results.entrySet())
		{
			Result result = resultEntry.getValue();
			String typeSlotUri = suModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, true, PREFIX).get();
			Pair<Value, Boolean> type = result.getValues(typeSlotUri).iterator().next();
			LinearStorageModel objectModel = model.getObjectStorageModels().get(new URI(stripPrefix(type.getLeft().stringValue())));
			
			if(result.getType().equals(RESOURCE_OBJECT_LABEL))
			{
				ResourceObjectNode node = new ResourceObjectNode(result.getUri());
				
				for(Entry<String, Slot<?>> slotEntry : objectModel.getSlots().entrySet())
				{
					String slotUri = suModel.resolveSlotURI(slotEntry.getValue(), true, PREFIX).orElse(slotEntry.getKey());
					Set<Pair<Value, Boolean>> values = result.getValues(slotUri);
					
					if(values != null)
					{
						for(Pair<Value, Boolean> value : values)
						{
							RESOURCE_NODE_MAPPER.parse(slotEntry.getKey(), value.getKey(), value.getValue(), node, slotEntry.getValue());
						}
					}
				}
				
				unit.addResourceObject(node);
			}
			else if(result.getType().equals(LITERAL_OBJECT_LABEL))
			{
				LiteralObjectNode node = new LiteralObjectNode(result.getUri());
				
				for(Entry<String, Slot<?>> slotEntry : objectModel.getSlots().entrySet())
				{
					String slotUri = suModel.resolveSlotURI(slotEntry.getValue(), true, PREFIX).orElse(slotEntry.getKey());
					Set<Pair<Value, Boolean>> values = result.getValues(slotUri);
					
					if(values != null)
					{
						for(Pair<Value, Boolean> value : values)
						{
							LITERAL_NODE_MAPPER.parse(slotEntry.getKey(), value.getKey(), value.getValue(), node, slotEntry.getValue());
						}
					}
				}
				
				unit.addLiteralObject(node);
			}
		}
		
		return unit;
	}
	
	@Override
	public SemanticUnit findBySubject(URI subject, URI kgbb, SemanticUnitStorageModel model)
	{
		LinearStorageModel suModel = model.getSemanticUnitStorageModel();
		String subjectSlotUri = suModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT, true, PREFIX).get();
		String kgbbSlotUri = suModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, true, PREFIX).get();
		
		Variable uri = SparqlBuilder.var("uri");
		SelectQuery query = Queries.SELECT(uri).where(
			uri.isA(SEMANTIC_UNIT_LABEL)
				.andHas(Values.iri(subjectSlotUri), Rdf.iri(PREFIX + subject))
				.andHas(Values.iri(kgbbSlotUri), Rdf.iri(PREFIX + kgbb)
		));
		
		List<BindingSet> bindingSets = this.tupleQueryNoTransaction(query.getQueryString());
		
		if(bindingSets.isEmpty())
		{
			throw new NoSuchElementException("Tried to find missing semantic unit with subject " + subject);
		}
		
		if(bindingSets.size() > 1)
		{
			throw new IllegalStateException("Storage contains multiple semantic units with type " + suModel.getName() + " for subject " + subject);
		}
		
		return this.findByUpri(new URI(stripPrefix(bindingSets.get(0).getBinding("uri").getValue().stringValue())), model);
	}
	
	@Override
	public URI findKggbi(URI upri)
	{
		String slotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_KGBB_URI, true, PREFIX).get();
		Variable kgbbiVar = SparqlBuilder.var("kgbbi");
		SelectQuery query = Queries.SELECT(kgbbiVar)
			.distinct()
			.where(
				Rdf.iri(PREFIX + upri)
					.has(Values.iri(slotUri), kgbbiVar)
			);
		List<BindingSet> results = this.tupleQueryNoTransaction(query.getQueryString());
		
		if(results.isEmpty())
		{
			throw new NoSuchElementException("Tried to find missing semantic unit " + upri);
		}
		
		return new URI(stripPrefix(results.get(0).getBinding("kgbbi").getValue().stringValue()));
	}
	
	@Override
	public boolean isSubjectUsedAsObject(URI subject)
	{
		String resourceUriSlotUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI, true, PREFIX).get();
		//Not using rdf4j query builder here because it does not support ASK
		//See https://rdf4j.org/documentation/tutorials/sparqlbuilder/
		String query = """
		ASK {
			?x <$resourceURI> <$SubjectIRI>;
				a <$ResourceObjectIRI> .
		}
		""" .replace("$SubjectIRI", PREFIX + subject)
			.replace("$ResourceObjectIRI", RESOURCE_OBJECT_LABEL.stringValue())
			.replace("$resourceURI", resourceUriSlotUri);
		
		try(RepositoryConnection connection = this.repo.getConnection())
		{
			return connection.prepareBooleanQuery(query).evaluate();
		}
	}
	
	@Override
	public boolean isSubjectUsedByOtherUnit(URI subject, URI uri)
	{
		String subjectSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT, true, PREFIX).get();
		String currentVersionSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, true, PREFIX).get();
		String typeSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, true, PREFIX).get();
		//Not using rdf4j query builder here because it does not support ASK
		//See https://rdf4j.org/documentation/tutorials/sparqlbuilder/
		String query = """
		ASK {
			?x a <$SemanticUnitIRI>;
				<$type> ?type;
				<$currentVersion> true;
				<$hasSemanticUnitSubject> <$subject> .
			FILTER(?x != <$URI> && ?type != <$NiiuKGBB> && ?type != <$SemanticUnitIRI>)
		}
		""" .replace("$URI", PREFIX + uri)
			.replace("$subject", PREFIX + subject)
			.replace("$SemanticUnitIRI", SEMANTIC_UNIT_LABEL.stringValue())
			.replace("$currentVersion", currentVersionSlotUri)
			.replace("$hasSemanticUnitSubject", subjectSlotUri)
			.replace("$type", typeSlotUri)
			.replace("$NiiuKGBB", PREFIX + Constants.NIIU_KGBB_URI);
		
		try(RepositoryConnection connection = this.repo.getConnection())
		{
			return connection.prepareBooleanQuery(query).evaluate();
		}
	}
	
	@Override
	public Set<URI> findAllConstraints(URI uri, StorageModelManager storageModelManager)
	{
		String subjectSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT, true, PREFIX).get();
		String kgbbUriSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, true, PREFIX).get();
		String currentVersionSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, true, PREFIX).get();
		String resourceCurrentVersionSlotUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, true, PREFIX).get();
		String constraintUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_CONSTRAINT, true, PREFIX).get();
		String resourceUriUri = this.genericResourceObjectModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_RESOURCE_URI, true, PREFIX).get();
		Variable x = SparqlBuilder.var("x");
		Variable y = SparqlBuilder.var("y");
		Variable kgbbConstraintsVar = SparqlBuilder.var("kgbbConstraints");
		Variable resourceContraintsVar = SparqlBuilder.var("resourceContraints");
		Iri iri = Rdf.iri(PREFIX + uri);
		SelectQuery query = Queries.SELECT(kgbbConstraintsVar, resourceContraintsVar)
			.distinct()
			.where(
				GraphPatterns.union(
					x.isA(SEMANTIC_UNIT_LABEL)
						.andHas(Rdf.iri(subjectSlotUri), iri)
						.andHas(Rdf.iri(kgbbUriSlotUri), kgbbConstraintsVar)
						.andHas(Rdf.iri(currentVersionSlotUri), Rdf.literalOf(true))
						.filter(Expressions.notEquals(kgbbConstraintsVar, Rdf.iri(SEMANTIC_UNIT_LABEL))),
					y.isA(RESOURCE_OBJECT_LABEL)
						.andHas(Rdf.iri(resourceUriUri), iri)
						.andHas(Rdf.iri(constraintUri), resourceContraintsVar)
						.andHas(Rdf.iri(resourceCurrentVersionSlotUri), Rdf.literalOf(true))
				)
			);
		List<BindingSet> results = this.tupleQueryNoTransaction(query.getQueryString());
		Set<URI> constraints = new HashSet<URI>();
		
		for(BindingSet result : results)
		{
			if(result.hasBinding(kgbbConstraintsVar.getVarName()))
			{
				LinearStorageModel subjectStorageModel = storageModelManager.getStorageModel(new URI(stripPrefix(result.getBinding(kgbbConstraintsVar.getVarName()).getValue().stringValue()))).getSubjectStorageModel();
				
				if(subjectStorageModel.getSubclassOf() != null)
				{
					constraints.add(subjectStorageModel.getSubclassOf());
				}
			}
			else if(result.hasBinding(resourceContraintsVar.getVarName()))
			{
				constraints.add(new URI(stripPrefix(result.getBinding(resourceContraintsVar.getVarName()).getValue().stringValue())));
			}
		}
		
		return constraints;
	}
	
	@Override
	public boolean isUnique(URI uri)
	{
		//Not using rdf4j query builder here because it does not support ASK
		//See https://rdf4j.org/documentation/tutorials/sparqlbuilder/
		String query = """
		ASK {
			{
				<$URI> ?a ?b .
			} UNION {
				?s ?p <$URI> .
			}
		}
		""".replace("$URI", PREFIX + uri);
		
		try(RepositoryConnection connection = this.repo.getConnection())
		{
			return !connection.prepareBooleanQuery(query).evaluate();
		}
	}
	
	@Override
	public void delete(SemanticUnit unit)
	{
		unit.setCurrentVersion(false);
		this.save(unit);
	}
	
	@Override
	public Set<URI> findAll(URI kgbb)
	{
		String typeSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_TYPE, true, PREFIX).get();
		String currentVersionSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, true, PREFIX).get();
		Variable kgbbiVar = SparqlBuilder.var("kgbb");
		SelectQuery query = Queries.SELECT(kgbbiVar)
			.distinct()
			.where(
				kgbbiVar.has(Values.iri(typeSlotUri), Values.iri(PREFIX + kgbb.toString()))
					.andHas(Values.iri(currentVersionSlotUri), true)
			);
		List<BindingSet> results = this.tupleQueryNoTransaction(query.getQueryString());
		return results.stream().map(result -> new URI(stripPrefix(result.getBinding("kgbb").getValue().stringValue()))).collect(Collectors.toSet());
	}
	
	@Override
	public Set<URI> findAssociatingUnits(URI uri)
	{
		String hasAssociatedSemanticUnitSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_HAS_ASSOCIATED_SEMANTIC_UNIT, true, PREFIX).get();
		String currentVersionSlotUri = this.genericSemanticUnitModel.resolveSlotURI(Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION, true, PREFIX).get();
		Variable uriVar = SparqlBuilder.var("uri");
		SelectQuery query = Queries.SELECT(uriVar)
			.distinct()
			.where(
				uriVar.has(Values.iri(hasAssociatedSemanticUnitSlotUri), Values.iri(PREFIX + uri.toString()))
					.andHas(Values.iri(currentVersionSlotUri), true)
			);
		List<BindingSet> results = this.tupleQueryNoTransaction(query.getQueryString());
		return results.stream().map(result -> new URI(stripPrefix(result.getBinding("uri").getValue().stringValue()))).collect(Collectors.toSet());
	}
	
	@TestOnly
	public Repository getRepository()
	{
		return this.repo;
	}
	
	protected static String stripPrefix(String string)
	{
		return string.replace(PREFIX, "");
	}
	
	private static class Result
	{
		private final URI uri;
		private IRI type;
		private Map<String, Either<Pair<Value, Boolean>, Set<Pair<Value, Boolean>>>> statements = new HashMap<String, Either<Pair<Value, Boolean>, Set<Pair<Value, Boolean>>>>();
		
		public Result(URI uri, IRI type)
		{
			this.uri = uri;
			this.type = type;
		}
		
		public Set<Pair<Value, Boolean>> getValues(String predicate)
		{
			Either<Pair<Value, Boolean>, Set<Pair<Value, Boolean>>> either = this.statements.get(predicate);
			
			if(either == null)
			{
				return null;
			}
			
			if(either.isLeft())
			{
				return Collections.singleton(either.left().get());
			}
			
			return either.right().get();
		}
		
		public void addStatement(String predicate, Value object, boolean currentVersion)
		{
			this.statements.compute(predicate, (key, value) ->
			{
				Pair<Value, Boolean> pair = Pair.of(object, currentVersion);
				
				if(value == null)
				{
					return Either.left(pair);
				}
				
				if(value.isLeft())
				{
					Set<Pair<Value, Boolean>> set = new HashSet<Pair<Value, Boolean>>();
					set.add(value.left().get());
					set.add(pair);
					return Either.right(set);
				}
				else if(value.isRight())
				{
					value.right().get().add(pair);
				}
				
				return value;
			});
		}
		
		public URI getUri()
		{
			return this.uri;
		}
		
		public IRI getType()
		{
			return this.type;
		}
	}
	
	/**
	 * Super simple model builder that does support using a property more that once
	 */
	private static class CustomModelBuilder
	{
		private Set<Statement> statements = new HashSet<Statement>();
		private Resource currentSubject;
		private Resource currentNamedGraph;
		
		public CustomModelBuilder add(IRI predicate, Object object)
		{
			if(this.currentSubject == null)
			{
				throw new ModelException("subject not set");
			}
			
			return this.add(this.currentSubject, predicate, object);
		}
		
		public CustomModelBuilder add(String predicate, Object object)
		{
			return this.add(SimpleValueFactory.getInstance().createIRI(predicate), object);
		}
		
		public CustomModelBuilder add(Resource subject, IRI predicate, Object object)
		{
			Value objectValue = null;
			
			if(object instanceof Value)
			{
				objectValue = (Value) object;
			}
			
			if(objectValue == null)
			{
				objectValue = Values.literal(object);
			}
			
			this.statements.add(SimpleValueFactory.getInstance().createStatement(subject, predicate, objectValue, this.currentNamedGraph));
			return this;
		}
		
		public CustomModelBuilder subject(Resource subject)
		{
			this.currentSubject = subject;
			return this;
		}
		
		public CustomModelBuilder namedGraph(Resource namedGraph)
		{
			this.currentNamedGraph = namedGraph;
			return this;
		}
		
		public Set<Statement> build()
		{
			return this.statements;
		}
	}
	
	/**
	 * Basically an analogous implementation of {@link Repositories.tupleQueryNoTransaction} but this function does not create compile errors in gradle
	 */
	private List<BindingSet> tupleQueryNoTransaction(String query)
	{
		List<BindingSet> bindings = new ArrayList<BindingSet>();
		
		try(RepositoryConnection connection = this.repo.getConnection())
		{
			TupleQuery t = connection.prepareTupleQuery(query);
			
			try(TupleQueryResult result = t.evaluate())
			{
				result.forEach(bindings::add);
			}
		}
		
		return bindings;
	}
	
	@TestOnly
	public void printRepository() throws UnsupportedRDFormatException, URISyntaxException
	{
		RDFWriter writer = Rio.createWriter(RDFFormat.TRIG, System.out, PREFIX);
		
		try(var conn = this.repo.getConnection())
		{
			writer.startRDF();
			RepositoryResult<Statement> results = conn.getStatements(null, null, null);
			
			for(var result : results)
			{
				writer.handleStatement(result);
			}
			
			writer.endRDF();
		}
		catch(RDFHandlerException e)
		{
			e.printStackTrace();
		}
	}
}
