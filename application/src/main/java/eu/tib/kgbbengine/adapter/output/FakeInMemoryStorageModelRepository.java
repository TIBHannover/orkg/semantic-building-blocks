package eu.tib.kgbbengine.adapter.output;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.storage.PartialStorageModel;
import eu.tib.kgbbengine.spi.StorageModelRepository;

public class FakeInMemoryStorageModelRepository implements StorageModelRepository
{
	private final Map<URI, PartialStorageModel> storageModels = new HashMap<URI, PartialStorageModel>();
	
	public FakeInMemoryStorageModelRepository() throws StreamReadException, DatabindException, IOException
	{
		//Object Positions
		this.storageModels.put(new URI("ObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/ObjectPosition.yaml")));
		this.storageModels.put(new URI("StringLiteralObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/StringLiteralObjectPosition.yaml")));
		this.storageModels.put(new URI("DecimalLiteralObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/DecimalLiteralObjectPosition.yaml")));
		this.storageModels.put(new URI("ResourceObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/ResourceObjectPosition.yaml")));
		this.storageModels.put(new URI("NamedIndividualIdentificationUnitResourceObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/NamedIndividualIdentificationUnitResourceObjectPosition.yaml")));
		this.storageModels.put(new URI("NamedIndividualIdentificationUnitLiteralObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/NamedIndividualIdentificationUnitLiteralObjectPosition.yaml")));
		this.storageModels.put(new URI("HasPartStatementUnitResourceObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/HasPartStatementUnitResourceObjectPosition.yaml")));
		this.storageModels.put(new URI("QualityStatementUnitResourceObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/QualityStatementUnitResourceObjectPosition.yaml")));
		this.storageModels.put(new URI("WeightMeasurementUnitLiteralObjectPosition1"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/WeightMeasurementUnitLiteralObjectPosition1.yaml")));
		this.storageModels.put(new URI("WeightMeasurementUnitLiteralObjectPosition2"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/WeightMeasurementUnitLiteralObjectPosition2.yaml")));
		this.storageModels.put(new URI("WeightMeasurementUnitLiteralObjectPosition3"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/WeightMeasurementUnitLiteralObjectPosition3.yaml")));
		this.storageModels.put(new URI("WeightMeasurementUnitResourceObjectPosition"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/objectnode/WeightMeasurementUnitResourceObjectPosition.yaml")));
		
		//Semantic Unit storage models
		this.storageModels.put(new URI("SemanticUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/SemanticUnit.yaml")));
		this.storageModels.put(new URI("NamedIndividualIdentificationUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/NamedIndividualIdentificationUnit.yaml")));
		this.storageModels.put(new URI("HasPartStatementUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/HasPartStatementUnit.yaml")));
		this.storageModels.put(new URI("ItemUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/ItemUnit.yaml")));
		this.storageModels.put(new URI("MaterialEntityItemUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/MaterialEntityItemUnit.yaml")));
		this.storageModels.put(new URI("QualityStatementUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/QualityStatementUnit.yaml")));
		this.storageModels.put(new URI("WeightMeasurementStatementUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/WeightMeasurementStatementUnit.yaml")));
		this.storageModels.put(new URI("WeightMeasurementCompoundUnit"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/semanticunit/WeightMeasurementCompoundUnit.yaml")));
		
		//Subject storage models
		this.storageModels.put(new URI("MaterialEntityItemUnitSubject"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/subject/MaterialEntityItemUnitSubject.yaml")));
		this.storageModels.put(new URI("NamedIndividualIdentificationUnitSubject"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/subject/NamedIndividualIdentificationUnitSubject.yaml")));
		this.storageModels.put(new URI("HasPartStatementUnitSubject"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/subject/HasPartStatementUnitSubject.yaml")));
		this.storageModels.put(new URI("QualityStatementUnitSubject"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/subject/QualityStatementUnitSubject.yaml")));
		this.storageModels.put(new URI("WeightMeasurementStatementUnitSubject"), PartialStorageModel.fromFile(new File("src/main/resources/kgbbs/storage/subject/WeightMeasurementStatementUnitSubject.yaml")));
	}
	
	@Override
	public PartialStorageModel findById(URI uri)
	{
		if(!this.storageModels.containsKey(uri))
		{
			throw new IllegalArgumentException("Tried to find missing storage model " + uri);
		}
		
		return this.storageModels.get(uri);
	}
}
