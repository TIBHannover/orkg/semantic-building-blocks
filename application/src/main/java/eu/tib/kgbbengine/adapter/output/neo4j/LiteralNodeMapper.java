package eu.tib.kgbbengine.adapter.output.neo4j;

import java.util.Map;

import org.neo4j.driver.Value;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.domain.util.Either;
import eu.tib.kgbbengine.spi.ValueParser;
import eu.tib.kgbbengine.spi.ValueProvider;

public class LiteralNodeMapper implements ValueProvider<LiteralObjectNode>, ValueParser<LiteralObjectNode, Value>
{
	@Override
	public Either<Object, Map<URI, Boolean>> provide(String name, Slot<?> slot, LiteralObjectNode node)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				return ValueProvider.literal(node.getCreator());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				return ValueProvider.literal(node.getCreatedWithApplication());
			case Constants.STORAGE_TEMPLATE_SLOT_INPUT_TYPE_LABEL:
				return ValueProvider.literal(node.getInputTypeLabel());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				return ValueProvider.literal(node.getCreationDate());
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				return ValueProvider.literal(node.getObjectPositionClass());
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				return ValueProvider.literal(node.isCurrentVersion());
			case Constants.STORAGE_TEMPLATE_SLOT_LITERAL:
				return ValueProvider.literal(node.getLiteral());
			default:
				return null;
		}
	}
	
	@Override
	public void parse(String name, Value value, boolean currentVersion, LiteralObjectNode node, Slot<?> slot)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				node.setCreator(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				node.setCreatedWithApplication(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_INPUT_TYPE_LABEL:
				node.setInputTypeLabel(value.asString());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				node.setCreationDate(value.asLocalDateTime());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				node.setObjectPositionClass(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				node.setCurrentVersion(value.asBoolean());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_LITERAL:
				node.setLiteral(value.asObject());
				break;
			default:
				break;
		}
	}
}
