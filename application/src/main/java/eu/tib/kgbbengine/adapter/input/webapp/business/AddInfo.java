package eu.tib.kgbbengine.adapter.input.webapp.business;

import java.util.ArrayList;
import java.util.List;

import eu.tib.kgbbengine.domain.input.ResourceInput.ResourceType;

public class AddInfo
{
	private String resource;
	private String label;
	private ResourceType type;
	private List<NavigationItem> items = new ArrayList<NavigationItem>();
	
	public AddInfo()
	{
		super();
	}
	
	public String getResource()
	{
		return this.resource;
	}
	
	public void setResource(String resource)
	{
		this.resource = resource;
	}
	
	public String getLabel()
	{
		return this.label;
	}
	
	public void setLabel(String label)
	{
		this.label = label;
	}
	
	public ResourceType getType()
	{
		return this.type;
	}
	
	public void setType(ResourceType type)
	{
		this.type = type;
	}
	
	public List<NavigationItem> getItems()
	{
		return this.items;
	}
	
	public void setItems(List<NavigationItem> items)
	{
		this.items = items;
	}
}
