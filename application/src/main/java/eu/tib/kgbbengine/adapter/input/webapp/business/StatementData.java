package eu.tib.kgbbengine.adapter.input.webapp.business;

public class StatementData
{
	private String uri;
	private String resourceURI;
	private String resourceLabel;
	private String literal;
	private String describedBy;
	private String formattedResourceLabel;
	
	public String getResourceURI()
	{
		return this.resourceURI;
	}
	
	public void setResourceURI(String resourceURI)
	{
		this.resourceURI = resourceURI;
	}
	
	public String getResourceLabel()
	{
		return this.resourceLabel;
	}
	
	public void setResourceLabel(String resourceLabel)
	{
		this.resourceLabel = resourceLabel;
	}
	
	public String getLiteral()
	{
		return this.literal;
	}
	
	public void setLiteral(String literal)
	{
		this.literal = literal;
	}
	
	public String getDescribedBy()
	{
		return this.describedBy;
	}
	
	public void setDescribedBy(String describedBy)
	{
		this.describedBy = describedBy;
	}
	
	public String getUri()
	{
		return this.uri;
	}
	
	public void setUri(String uri)
	{
		this.uri = uri;
	}
	
	public String getFormattedResourceLabel()
	{
		return this.formattedResourceLabel;
	}
	
	public void setFormattedResourceLabel(String formattedResourceLabel)
	{
		this.formattedResourceLabel = formattedResourceLabel;
	}
}
