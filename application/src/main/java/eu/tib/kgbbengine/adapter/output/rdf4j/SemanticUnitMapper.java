package eu.tib.kgbbengine.adapter.output.rdf4j;

import java.time.LocalDateTime;
import java.util.Map;

import org.eclipse.rdf4j.model.Value;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.domain.util.Either;
import eu.tib.kgbbengine.spi.ValueParser;
import eu.tib.kgbbengine.spi.ValueProvider;

public class SemanticUnitMapper implements ValueProvider<SemanticUnit>, ValueParser<SemanticUnit, Value>
{
	@Override
	public Either<Object, Map<URI, Boolean>> provide(String name, Slot<?> slot, SemanticUnit unit)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_DESCRIPTION:
				return ValueProvider.literal(unit.getDescription());
			case Constants.STORAGE_TEMPLATE_SLOT_LABEL:
				return ValueProvider.literal(unit.getLabel());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT:
				return ValueProvider.resource(unit.getSubject(), unit.isCurrentVersion());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_ASSOCIATED_SEMANTIC_UNIT:
				return ValueProvider.resource(unit.getAssociatedSemanticUnits());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNITS_GRAPH:
				return ValueProvider.resource(unit.getSemanticUnitsGraph());
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				return ValueProvider.resource(unit.getKgbb());
			case Constants.STORAGE_TEMPLATE_SLOT_OBJECT_DESCRIBED_BY_SEMANTIC_UNIT:
				return ValueProvider.resource(unit.getObjectDescribedBySemanticUnits());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_LINKED_SEMANTIC_UNIT:
				return ValueProvider.resource(unit.getLinkedSemanticUnits());
			case Constants.STORAGE_TEMPLATE_SLOT_KGBB_URI:
				return ValueProvider.resource(unit.getKgbbi());
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				return ValueProvider.literal(unit.isCurrentVersion());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				return ValueProvider.resource(unit.getCreator());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				return ValueProvider.literal(unit.getCreationDate());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				return ValueProvider.resource(unit.getCreatedWithApplication());
			case Constants.STORAGE_TEMPLATE_SLOT_DELETED_BY:
				return ValueProvider.resource(unit.getDeletedBy());
			case Constants.STORAGE_TEMPLATE_SLOT_DELETION_DATE:
				return ValueProvider.literal(unit.getDeletionDate());
			default:
				return null;
		}
	}
	
	@Override
	public void parse(String name, Value value, boolean currentVersion, SemanticUnit unit, Slot<?> slot)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_DESCRIPTION:
				unit.setDescription(value.stringValue());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_LABEL:
				unit.setLabel(value.stringValue());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT:
				unit.setSubject(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_ASSOCIATED_SEMANTIC_UNIT:
				unit.addAssociatedSemanticUnit(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())), currentVersion);
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNITS_GRAPH:
				unit.setSemanticUnitsGraph(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				unit.setKgbb(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_OBJECT_DESCRIBED_BY_SEMANTIC_UNIT:
				unit.addObjectDescribedBySemanticUnit(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())), currentVersion);
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_LINKED_SEMANTIC_UNIT:
				unit.addLinkedSemanticUnit(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())), currentVersion);
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_KGBB_URI:
				unit.setKgbbi(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				unit.setCurrentVersion(Boolean.valueOf(value.stringValue()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				unit.setCreator(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				unit.setCreationDate(LocalDateTime.parse(value.stringValue()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				unit.setCreatedWithApplication(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_DELETED_BY:
				unit.setDeletedBy(new URI(Rdf4jRepositiory.stripPrefix(value.stringValue())));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_DELETION_DATE:
				unit.setDeletionDate(LocalDateTime.parse(value.stringValue()));
				break;
			default:
				break;
		}
	}
}
