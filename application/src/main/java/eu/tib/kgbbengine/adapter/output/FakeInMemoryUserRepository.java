package eu.tib.kgbbengine.adapter.output;

import java.util.HashSet;
import java.util.Set;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.spi.UserRepository;

public class FakeInMemoryUserRepository implements UserRepository
{
	private final Set<URI> users = new HashSet<URI>();
	
	public FakeInMemoryUserRepository()
	{
		this.users.add(new URI("KGBBEngineUser"));
	}
	
	@Override
	public Object findById(URI id)
	{
		if(!this.users.contains(id))
		{
			throw new IllegalArgumentException("Invalid user " + id);
		}
		
		return null;
	}
}
