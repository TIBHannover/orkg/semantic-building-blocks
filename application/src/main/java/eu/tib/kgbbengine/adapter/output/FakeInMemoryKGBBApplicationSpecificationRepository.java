package eu.tib.kgbbengine.adapter.output;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.Association;
import eu.tib.kgbbengine.domain.kgbb.KGBBInstance;
import eu.tib.kgbbengine.domain.kgbb.Link;
import eu.tib.kgbbengine.spi.KGBBApplicationSpecificationRepository;

public class FakeInMemoryKGBBApplicationSpecificationRepository implements KGBBApplicationSpecificationRepository
{
	private final Map<URI, KGBBInstance> kgbbs = new HashMap<URI, KGBBInstance>();
	
	public FakeInMemoryKGBBApplicationSpecificationRepository()
	{
		URI niiuUri = new URI("NamedIndividualIdentificationKGBBInstance");
		URI meiUri = new URI("MaterialEntityItemKGBBInstance");
		URI hpsUri = new URI("HasPartStatementKGBBInstance");
		URI wmcUri = new URI("WeightMeasurementCompoundKGBBInstance");
		URI qsUri = new URI("QualityStatementKGBBInstance");
		URI wmsUri = new URI("WeightMeasurementStatementKGBBInstance");
		
		KGBBInstance niiu = new KGBBInstance(niiuUri, new URI("NamedIndividualIdentificationKGBB"), Collections.emptySet(), Collections.emptySet());
		
		Set<Association> siduAssociations = new HashSet<Association>();
		siduAssociations.add(new Association(hpsUri, false, 0, new URI("HasPartStatementUnitResourceObjectPosition")));
		siduAssociations.add(new Association(wmcUri, false, 1, null));
		KGBBInstance sidu = new KGBBInstance(meiUri, new URI("MaterialEntityItemKGBB"), siduAssociations, Collections.emptySet());
		
		Set<Link> hpbuLinks = new HashSet<Link>();
		hpbuLinks.add(new Link(meiUri, true, 1, new URI("HasPartStatementUnitResourceObjectPosition")));
		KGBBInstance hbpu = new KGBBInstance(hpsUri, new URI("HasPartStatementKGBB"), Collections.emptySet(), hpbuLinks);
		
		Set<Association> wmcAssociations = new HashSet<Association>();
		wmcAssociations.add(new Association(qsUri, true, 1, new URI("QualityStatementUnitResourceObjectPosition")));
		KGBBInstance wmcu = new KGBBInstance(wmcUri, new URI("WeightMeasurementCompoundKGBB"), wmcAssociations, Collections.emptySet());
		
		Set<Link> qsuLinks = new HashSet<Link>();
		qsuLinks.add(new Link(wmsUri, true, 0, new URI("QualityStatementUnitResourceObjectPosition")));
		KGBBInstance qsu = new KGBBInstance(qsUri, new URI("QualityStatementKGBB"), Collections.emptySet(), qsuLinks);
		
		KGBBInstance wmsu = new KGBBInstance(wmsUri, new URI("WeightMeasurementStatementKGBB"), Collections.emptySet(), Collections.emptySet());
		
		this.add(niiu);
		this.add(sidu);
		this.add(hbpu);
		this.add(wmcu);
		this.add(qsu);
		this.add(wmsu);
	}
	
	private void add(KGBBInstance kgbbInstance)
	{
		this.kgbbs.put(kgbbInstance.getUri(), kgbbInstance);
	}
	
	@Override
	public KGBBInstance findById(URI id)
	{
		if(!this.kgbbs.containsKey(id))
		{
			throw new IllegalArgumentException("Tried to find missing kggbi " + id);
		}
		
		return this.kgbbs.get(id);
	}
}
