package eu.tib.kgbbengine.adapter.output.neo4j;

import java.util.Map;

import org.neo4j.driver.Value;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.slot.Slot;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.domain.util.Either;
import eu.tib.kgbbengine.spi.ValueParser;
import eu.tib.kgbbengine.spi.ValueProvider;

public class SemanticUnitMapper implements ValueProvider<SemanticUnit>, ValueParser<SemanticUnit, Value>
{
	@Override
	public Either<Object, Map<URI, Boolean>> provide(String name, Slot<?> slot, SemanticUnit unit)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_DESCRIPTION:
				return ValueProvider.literal(unit.getDescription());
			case Constants.STORAGE_TEMPLATE_SLOT_LABEL:
				return ValueProvider.literal(unit.getLabel());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT:
				return ValueProvider.resource(unit.getSubject(), unit.isCurrentVersion());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_ASSOCIATED_SEMANTIC_UNIT:
				return ValueProvider.resource(unit.getAssociatedSemanticUnits());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNITS_GRAPH:
				return ValueProvider.literal(unit.getSemanticUnitsGraph());
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				return ValueProvider.literal(unit.getKgbb());
			case Constants.STORAGE_TEMPLATE_SLOT_OBJECT_DESCRIBED_BY_SEMANTIC_UNIT:
				return ValueProvider.resource(unit.getObjectDescribedBySemanticUnits());
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_LINKED_SEMANTIC_UNIT:
				return ValueProvider.resource(unit.getLinkedSemanticUnits());
			case Constants.STORAGE_TEMPLATE_SLOT_KGBB_URI:
				return ValueProvider.literal(unit.getKgbbi());
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				return ValueProvider.literal(unit.isCurrentVersion());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				return ValueProvider.literal(unit.getCreator());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				return ValueProvider.literal(unit.getCreationDate());
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				return ValueProvider.literal(unit.getCreatedWithApplication());
			case Constants.STORAGE_TEMPLATE_SLOT_DELETED_BY:
				return ValueProvider.literal(unit.getDeletedBy());
			case Constants.STORAGE_TEMPLATE_SLOT_DELETION_DATE:
				return ValueProvider.literal(unit.getDeletionDate());
			default:
				return null;
		}
	}
	
	@Override
	public void parse(String name, Value value, boolean currentVersion, SemanticUnit unit, Slot<?> slot)
	{
		switch(name)
		{
			case Constants.STORAGE_TEMPLATE_SLOT_DESCRIPTION:
				unit.setDescription(value.asString());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_LABEL:
				unit.setLabel(value.asString());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNIT_SUBJECT:
				unit.setSubject(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_ASSOCIATED_SEMANTIC_UNIT:
				unit.addAssociatedSemanticUnit(new URI(value.asString()), currentVersion);
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_SEMANTIC_UNITS_GRAPH:
				unit.setSemanticUnitsGraph(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_TYPE:
				unit.setKgbb(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_OBJECT_DESCRIBED_BY_SEMANTIC_UNIT:
				unit.addObjectDescribedBySemanticUnit(new URI(value.asString()), currentVersion);
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_HAS_LINKED_SEMANTIC_UNIT:
				unit.addLinkedSemanticUnit(new URI(value.asString()), currentVersion);
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_KGBB_URI:
				unit.setKgbbi(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CURRENT_VERSION:
				unit.setCurrentVersion(value.asBoolean());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATOR:
				unit.setCreator(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATION_DATE:
				unit.setCreationDate(value.asLocalDateTime());
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_CREATED_WITH_APPLICATION:
				unit.setCreatedWithApplication(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_DELETED_BY:
				unit.setDeletedBy(new URI(value.asString()));
				break;
			case Constants.STORAGE_TEMPLATE_SLOT_DELETION_DATE:
				unit.setDeletionDate(value.asLocalDateTime());
				break;
			default:
				break;
		}
	}
}
