package eu.tib.kgbbengine.adapter.output;

import java.util.HashMap;
import java.util.Map;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.ObjectPosition;
import eu.tib.kgbbengine.spi.ObjectPositionRepository;

public class FakeInMemoryObjectPositionRepository implements ObjectPositionRepository
{
	private final Map<URI, ObjectPosition> positions = new HashMap<URI, ObjectPosition>();
	
	public FakeInMemoryObjectPositionRepository() throws Exception
	{
		ObjectPosition rootObject = new ObjectPosition(new URI("ObjectPosition"));
		ObjectPosition resourceObject = new ObjectPosition(rootObject, new URI("ResourceObjectPosition"));
		ObjectPosition stringLiteralObject = new ObjectPosition(rootObject, new URI("StringLiteralObjectPosition"));
		ObjectPosition decimalObject = new ObjectPosition(rootObject, new URI("DecimalLiteralObjectPosition"));
		ObjectPosition niiuResourceObjectPosition = new ObjectPosition(resourceObject, new URI("NamedIndividualIdentificationUnitResourceObjectPosition"));
		ObjectPosition niiuLiteralObjectPosition = new ObjectPosition(stringLiteralObject, new URI("NamedIndividualIdentificationUnitLiteralObjectPosition"));
		ObjectPosition hpsResourceObjectPosition = new ObjectPosition(resourceObject, new URI("HasPartStatementUnitResourceObjectPosition"));
		ObjectPosition qsResourceObject = new ObjectPosition(resourceObject, new URI("QualityStatementUnitResourceObjectPosition"));
		ObjectPosition wmsDecimalLiteralObject1 = new ObjectPosition(decimalObject, new URI("WeightMeasurementUnitLiteralObjectPosition1"));
		ObjectPosition wmsDecimalLiteralObject2 = new ObjectPosition(decimalObject, new URI("WeightMeasurementUnitLiteralObjectPosition2"));
		ObjectPosition wmsDecimalLiteralObject3 = new ObjectPosition(decimalObject, new URI("WeightMeasurementUnitLiteralObjectPosition3"));
		ObjectPosition wmsResourceObject = new ObjectPosition(resourceObject, new URI("WeightMeasurementUnitResourceObjectPosition"));
		this.add(rootObject, resourceObject, stringLiteralObject, decimalObject, niiuResourceObjectPosition, niiuLiteralObjectPosition, hpsResourceObjectPosition,
				qsResourceObject, wmsDecimalLiteralObject1, wmsDecimalLiteralObject2, wmsDecimalLiteralObject3, wmsResourceObject);
	}
	
	private void add(ObjectPosition... positions)
	{
		for(ObjectPosition position : positions)
		{
			this.positions.put(position.getUri(), position);
		}
	}
	
	@Override
	public ObjectPosition findById(URI id)
	{
		if(!this.positions.containsKey(id))
		{
			throw new IllegalArgumentException("Tried to find missing object position " + id);
		}
		
		return this.positions.get(id);
	}
	
	@Override
	public boolean isResourceObjectPosition(ObjectPosition position)
	{
		return position.getUri().equals(new URI("ResourceObjectPosition")) || position.getParent() != null && this.isResourceObjectPosition(this.findById(position.getParent()));
	}
	
//	public void exportToNeo4j(Driver driver)
//	{
//		try(Session session = driver.session())
//		{
//			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
//			SimpleModule module = new SimpleModule();
//			module.addSerializer(URI.class, new JsonSerializer<URI>()
//			{
//				@Override
//				public void serialize(URI value, JsonGenerator gen, SerializerProvider serializers) throws IOException
//				{
//					gen.writeString(value.toString());
//				}
//			});
//			mapper.registerModule(module);
//			mapper.registerModule(new JavaTimeModule());
//			
//			this.export(session, mapper, this.positions.get(new URI("ObjectPosition")));
//			this.export(session, mapper, this.positions.get(new URI("ResourceObjectPosition")));
//			this.export(session, mapper, this.positions.get(new URI("StringLiteralObjectPosition")));
//			this.export(session, mapper, this.positions.get(new URI("NamedIndividualIdentificationUnitResourceObjectPosition")));
//			this.export(session, mapper, this.positions.get(new URI("NamedIndividualIdentificationUnitLiteralObjectPosition")));
//			this.export(session, mapper, this.positions.get(new URI("HasPartStatementUnitResourceObjectPosition")));
//		}
//	}
//	
//	private void export(Session session, ObjectMapper mapper, ObjectPosition position)
//	{
//		session.writeTransaction(tx ->
//		{
//			try
//			{
//				tx.run("CREATE (:" + position.getUri() + " {name:\"" + position.getUri() + "\", hasStorageModel:\"" + StringEscapeUtils.escapeJson(mapper.writeValueAsString(position.getStorageModel())) + "\"})");
//			}
//			catch(JsonProcessingException e)
//			{
//				e.printStackTrace();
//			}
//			
//			if(position.getParent() != null)
//			{
//				tx.run("MATCH (parent:" + position.getParent() + ")\nMATCH (child:" + position.getUri() + ")\nCREATE (child)-[:SUBCLASS_OF]->(parent)");
//			}
//			
//			return null;
//		});
//	}
}
