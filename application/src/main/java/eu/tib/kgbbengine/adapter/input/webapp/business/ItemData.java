package eu.tib.kgbbengine.adapter.input.webapp.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemData
{
	private String uri;
	private String label;
	private String niiu;
	private String resourceURI;
	private String resourceLabel;
	private String formattedResourceLabel;
	private Map<String, List<ItemData>> children = new HashMap<String, List<ItemData>>();
	private Map<String, List<Map<String, StatementData>>> statements = new HashMap<String, List<Map<String, StatementData>>>(); // kgbb -> List(position -> data)
	
	public String getResourceURI()
	{
		return this.resourceURI;
	}
	
	public String getLabel()
	{
		return this.label;
	}
	
	public void setLabel(String label)
	{
		this.label = label;
	}
	
	public void setResourceURI(String resourceURI)
	{
		this.resourceURI = resourceURI;
	}
	
	public String getResourceLabel()
	{
		return this.resourceLabel;
	}
	
	public void setResourceLabel(String resourceLabel)
	{
		this.resourceLabel = resourceLabel;
	}
	
	public Map<String, List<ItemData>> getChildren()
	{
		return this.children;
	}
	
	public void setChildren(Map<String, List<ItemData>> children)
	{
		this.children = children;
	}
	
	public Map<String, List<Map<String, StatementData>>> getStatements()
	{
		return this.statements;
	}
	
	public void setStatements(Map<String, List<Map<String, StatementData>>> statements)
	{
		this.statements = statements;
	}
	
	public String getUri()
	{
		return this.uri;
	}
	
	public void setUri(String uri)
	{
		this.uri = uri;
	}
	
	public String getFormattedResourceLabel()
	{
		return this.formattedResourceLabel;
	}
	
	public void setFormattedResourceLabel(String formattedResourceLabel)
	{
		this.formattedResourceLabel = formattedResourceLabel;
	}
	
	public String getNiiu()
	{
		return this.niiu;
	}
	
	public void setNiiu(String niiu)
	{
		this.niiu = niiu;
	}
}
