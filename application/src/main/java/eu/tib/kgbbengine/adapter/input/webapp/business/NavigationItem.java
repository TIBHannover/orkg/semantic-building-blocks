package eu.tib.kgbbengine.adapter.input.webapp.business;

import java.util.ArrayList;
import java.util.List;

public class NavigationItem
{
	private String name;
	private String itemUri;
	private NavigationItem parent;
	private List<NavigationItem> children = new ArrayList<NavigationItem>();
	
	public NavigationItem()
	{
		super();
	}
	
	public NavigationItem(String name, String itemUri)
	{
		this.name = name;
		this.itemUri = itemUri;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getItemUri()
	{
		return this.itemUri;
	}
	
	public void setItemUri(String itemUri)
	{
		this.itemUri = itemUri;
	}
	
	public List<NavigationItem> getChildren()
	{
		return this.children;
	}
	
	public void setChildren(List<NavigationItem> children)
	{
		this.children = children;
	}
	
	public boolean hasParent(String uri)
	{
		return this.itemUri.equals(uri) || this.parent != null && this.parent.hasParent(uri);
	}
	
	public boolean hasChild(String uri)
	{
		return this.itemUri.equals(uri) || this.children.stream().anyMatch(child -> child.hasChild(uri));
	}
	
	public NavigationItem getParent()
	{
		return this.parent;
	}
	
	public void setParent(NavigationItem parent)
	{
		this.parent = parent;
	}
}
