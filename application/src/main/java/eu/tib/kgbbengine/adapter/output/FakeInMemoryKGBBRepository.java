package eu.tib.kgbbengine.adapter.output;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.kgbb.KGBB;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnitType;
import eu.tib.kgbbengine.spi.KGBBRepository;

public class FakeInMemoryKGBBRepository implements KGBBRepository
{
	private final Map<URI, KGBB> kgbbs = new HashMap<URI, KGBB>();
	
	public FakeInMemoryKGBBRepository() throws Exception
	{
		KGBB kgbb = new KGBB(new URI("KGBB"), new URI("SemanticUnit"), null, Collections.emptyList());
		KGBB statement = new KGBB(kgbb, new URI("StatementKGBB"), null, null, Collections.emptyList());
		KGBB compound = new KGBB(kgbb, new URI("CompoundKGBB"), null, null, Collections.emptyList());
		KGBB item = new KGBB(compound, new URI("ItemKGBB"), new URI("ItemUnit"), null, Collections.emptyList());
		List<URI> niiuPositions = List.of(new URI("NamedIndividualIdentificationUnitResourceObjectPosition"), new URI("NamedIndividualIdentificationUnitLiteralObjectPosition"));
		KGBB niiu = new KGBB(statement, new URI("NamedIndividualIdentificationKGBB"), new URI("NamedIndividualIdentificationUnit"), new URI("NamedIndividualIdentificationUnitSubject"), niiuPositions);
		KGBB hps = new KGBB(statement, new URI("HasPartStatementKGBB"), new URI("HasPartStatementUnit"), new URI("HasPartStatementUnitSubject"), List.of(new URI("HasPartStatementUnitResourceObjectPosition")));
		KGBB mei = new KGBB(item, new URI("MaterialEntityItemKGBB"), new URI("MaterialEntityItemUnit"), new URI("MaterialEntityItemUnitSubject"), Collections.emptyList());
		KGBB wmc = new KGBB(compound, new URI("WeightMeasurementCompoundKGBB"), new URI("WeightMeasurementCompoundUnit"), null, Collections.emptyList());
		KGBB qs = new KGBB(statement, new URI("QualityStatementKGBB"), new URI("QualityStatementUnit"), new URI("QualityStatementUnitSubject"), List.of(new URI("QualityStatementUnitResourceObjectPosition")));
		List<URI> wmsPositions = List.of(new URI("WeightMeasurementUnitResourceObjectPosition"), new URI("WeightMeasurementUnitLiteralObjectPosition1"), new URI("WeightMeasurementUnitLiteralObjectPosition2"), new URI("WeightMeasurementUnitLiteralObjectPosition3"));
		KGBB wms = new KGBB(statement, new URI("WeightMeasurementStatementKGBB"), new URI("WeightMeasurementStatementUnit"), new URI("WeightMeasurementStatementUnitSubject"), wmsPositions);
		this.add(kgbb, statement, compound, item, niiu, hps, mei, wmc, qs, wms);
	}
	
	private void add(KGBB... kgbbs)
	{
		for(KGBB kgbb : kgbbs)
		{
			this.kgbbs.put(kgbb.getUri(), kgbb);
		}
	}
	
	@Override
	public KGBB findById(URI id)
	{
		if(!this.kgbbs.containsKey(id))
		{
			throw new IllegalArgumentException("Tried to find missing kgbb " + id);
		}
		
		return this.kgbbs.get(id);
	}
	
	@Override
	public SemanticUnitType getType(URI kgbb)
	{
		if(kgbb.toString().equals("MaterialEntityItemKGBB") || kgbb.toString().equals("WeightMeasurementCompoundKGBB"))
		{
			return SemanticUnitType.COMPOUND;
		}
		else if(kgbb.toString().equals("NamedIndividualIdentificationKGBB") || kgbb.toString().equals("HasPartStatementKGBB") || kgbb.toString().equals("QualityStatementKGBB") || kgbb.toString().equals("WeightMeasurementStatementKGBB"))
		{
			return SemanticUnitType.STATEMENT;
		}
		
		throw new RuntimeException("Tried to get type for unknown kgbb " + kgbb);
	}
}
