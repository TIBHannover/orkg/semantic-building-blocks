package eu.tib.kgbbengine.adapter.output;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.tib.kgbbengine.domain.URI;
import eu.tib.kgbbengine.domain.semanticunit.LiteralObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.ResourceObjectNode;
import eu.tib.kgbbengine.domain.semanticunit.SemanticUnit;
import eu.tib.kgbbengine.domain.storage.SemanticUnitStorageModel;
import eu.tib.kgbbengine.domain.storage.StorageModelManager;
import eu.tib.kgbbengine.domain.util.Constants;
import eu.tib.kgbbengine.spi.SemanticUnitRepository;

public class FakeInMemorySemanticUnitRepository implements SemanticUnitRepository
{
	@JsonProperty("units")
	private final Map<URI, SemanticUnit> units = new HashMap<URI, SemanticUnit>();
	
	@Override
	public void save(SemanticUnit unit)
	{
		this.units.put(unit.getUri(), unit);
	}
	
	@Override
	public SemanticUnit findByUpri(URI uri, SemanticUnitStorageModel model)
	{
		if(!this.units.containsKey(uri))
		{
			throw new NoSuchElementException("Tried to find missing semantic unit " + uri);
		}
		
		return this.units.get(uri).shallowCopy();
	}
	
	@Override
	public boolean isSubjectUsedAsObject(URI subject)
	{
		return this.units.values().stream().anyMatch(unit -> unit.getResourceObjects().stream().anyMatch(node -> node.getResourceURI().equals(subject)));
	}
	
	@Override
	public boolean isSubjectUsedByOtherUnit(URI subject, URI uri)
	{
		return this.units.values().stream().anyMatch(unit -> unit.isCurrentVersion() && unit.getSubject().equals(subject) && !unit.getUri().equals(uri) && !Constants.NIIU_KGBB_URI.equals(unit.getKgbb()));
	}
	
	@Override
	public Set<URI> findAllConstraints(URI subject, StorageModelManager storageModelManager)
	{
		return this.units.values().stream().filter(SemanticUnit::isCurrentVersion).map(unit ->
		{
			Stream<URI> constraints = unit.getResourceObjects().stream()
					.filter(resource -> resource.isCurrentVersion() && resource.getConstraint() != null && subject.equals(resource.getResourceURI()))
					.map(ResourceObjectNode::getConstraint);
			
			if(subject.equals(unit.getSubject()))
			{
				return Stream.concat(Stream.ofNullable(storageModelManager.getStorageModel(unit.getKgbb()).getSubjectStorageModel().getSubclassOf()), constraints);
			}
			
			return constraints;
		}).flatMap(Function.identity()).collect(Collectors.toSet());
	}
	
	@Override
	public SemanticUnit findBySubject(URI subject, URI kgbb, SemanticUnitStorageModel model)
	{
		List<SemanticUnit> units = this.units.values().stream().filter(unit ->
		{
			return unit.isCurrentVersion() && unit.getStorageModel().getSemanticUnitStorageModel().getName().equals(model.getSemanticUnitStorageModel().getName()) && unit.getSubject().equals(subject);
		}).toList();
		
		if(units.isEmpty())
		{
			throw new NoSuchElementException("Tried to find missing semantic unit with subject " + subject);
		}
		
		if(units.size() > 1)
		{
			throw new IllegalStateException("Storage contains multiple semantic units with type " + model.getSemanticUnitStorageModel().getName() + " for subject " + subject);
		}
		
		return units.get(0).shallowCopy();
	}
	
	@Override
	public URI findKggbi(URI uri)
	{
		return this.findByUpri(uri, null).getKgbbi();
	}
	
	@Override
	public boolean isUnique(URI uri)
	{
		return !this.units.values().stream().anyMatch(unit ->
		{
			if(unit.getSemanticUnitsGraph().equals(uri) || unit.getUri().equals(uri) || unit.getSubject().equals(uri))
			{
				return true;
			}
			
			for(ResourceObjectNode node : unit.getResourceObjects())
			{
				if(node.getUri().equals(uri))
				{
					return true;
				}
			}
			
			for(LiteralObjectNode node : unit.getLiteralObjects())
			{
				if(node.getUri().equals(uri))
				{
					return true;
				}
			}
			
			return false;
		});
	}
	
	@Override
	public void delete(SemanticUnit unit)
	{
		unit.setCurrentVersion(false);
		this.save(unit);
		
		for(SemanticUnit u : this.units.values())
		{
			if(u.getCurrentAssociatedSemanticUnits().contains(unit.getUri()))
			{
				u.removeAssociatedSemanticUnit(unit.getUri());
			}
			
			if(u.getCurrentLinkedSemanticUnits().contains(unit.getUri()))
			{
				u.removeLinkedSemanticUnit(unit.getUri());
			}
			
			if(u.getCurrentObjectDescribedBySemanticUnits().contains(unit.getUri()))
			{
				u.removeObjectDescribedBySemanticUnit(unit.getUri());
			}
		}
	}
	
	@Override
	public Set<URI> findAll(URI kgbb)
	{
		return this.units.values().stream().filter(unit -> unit.isCurrentVersion() && unit.getKgbb().equals(kgbb)).map(SemanticUnit::getUri).collect(Collectors.toSet());
	}
	
	@Override
	public Set<URI> findAssociatingUnits(URI uri)
	{
		return this.units.values().stream().filter(unit -> unit.isCurrentVersion() && unit.getCurrentAssociatedSemanticUnits().contains(uri)).map(SemanticUnit::getUri).collect(Collectors.toSet());
	}
}
