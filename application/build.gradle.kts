plugins {
	//kotlin("jvm") version "1.6.10"
	java
	eclipse
	application
}

group = "eu.tib"
version = "0.0.0-SNAPSHOT"

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

repositories {
	mavenCentral()
}

dependencies {
	//implementation(kotlin("stdlib"))
	
	//Domain
	implementation("com.fasterxml.jackson.core:jackson-databind:2.13.2")
	implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.13.2")
	implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.2")
	implementation("org.apache.httpcomponents:httpclient:4.5.+")
	implementation("org.apache.commons:commons-text:1.9")
	implementation("org.springframework.data:spring-data-neo4j:6.2.+")
	implementation("org.jetbrains:annotations:23.0.+")
	//implementation("org.apache.logging.log4j:log4j-slf4j-impl:2.17.+")
	implementation("org.neo4j:neo4j-cypher-dsl:2022.2.0")
	
	//Rdf4j
	//implementation("org.eclipse.rdf4j:rdf4j-storage:4.0.0-M2")
	//implementation("org.eclipse.rdf4j:rdf4j-client:4.0.0-M2")
	//Simple local storage and querying of RDF
	implementation("org.eclipse.rdf4j:rdf4j-repository-sail:4.0.0-M2")
	implementation("org.eclipse.rdf4j:rdf4j-sail-memory:4.0.0-M2")
	//Parsing / writing RDF files
	implementation("org.eclipse.rdf4j:rdf4j-rio-rdfxml:4.0.0-M2")
	implementation("org.eclipse.rdf4j:rdf4j-rio-turtle:4.0.0-M2")
	//SparQL
	implementation("org.eclipse.rdf4j:rdf4j-sparqlbuilder:4.0.0-M2")
	
	//Input
	implementation("org.springframework:spring-webmvc:5.3.+")
	implementation("org.thymeleaf:thymeleaf-spring5:3.1.0.M1")
	implementation("org.springframework.boot:spring-boot-starter-web:2.6.+")
	implementation("javax.inject:javax.inject:1")
	
	//Test
	testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.getByName<Test>("test") {
	useJUnitPlatform()
}

application {
    mainClass.set("eu.tib.kgbbengine.adapter.input.webapp.Main")
}
