# KGBB Engine #

The KGGB-Engine is based on the concepts of Semantic Units and Knowledge Graph Building Blocks (KGBBs) and provides a modular Framework for KGBB-driven Knowledge Graph applications.
It currently supports five different generic types of semantic units, including Named Individual Identification Units, Assertional Statement Units, Typed Statement Units, Quality Measurement Units and Item Units.

## Prerequisites ##

* Java 17

## Build and Test ##

To build and test the application run `gradlew build`

## Demo ##

To start the webbased demo application with an in-memory database and some dummy data run `gradlew run` and navigate to http://localhost:8080
